//
//  CustomProgressCell.swift
//  Sample
//
//  Created by Thejus Jose on 08/10/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Eureka

// Custom Cell with value type: Bool
// The cell is defined using a .xib, so we can set outlets :)
public class CustomProgressCell: Cell<Float>, CellType {
    
//    private var progVal:Float=0.1
    
    
    @IBOutlet weak var progressBarParentView: UIView!
    @IBOutlet weak var progress: UIProgressView!
    
    public override func setup() {
        super.setup()
        progress.transform = progress.transform.scaledBy(x: 1, y: 8)
        progress.layer.cornerRadius = 10
        progress.layer.masksToBounds = false
        progress.clipsToBounds = true
//        self.progressBarParentView.layer.borderColor = UIColor.init(red: 62/255, green: 57/255, blue: 171/255, alpha: 1).cgColor
//        self.progressBarParentView.layer.borderWidth = 2
//        progress.layer.sublayers![1].cornerRadius = 8
//        progress.subviews[1].clipsToBounds = true
//        progress.setProgress(0, animated: true)
        
//        switchControl.addTarget(self, action: #selector(CustomProgressCell.switchValueChanged), for: .valueChanged)
    }
    
    func updateValueChanged(val:Float){
//        row.value = switchControl.on
//        progVal=val
        progress.setProgress(val, animated: true)
        row.updateCell() // Re-draws the cell which calls 'update' bellow
    }
    
    public override func update() {
        super.update()    }
}

