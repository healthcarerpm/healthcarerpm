//
//  PopoverView.swift
//  HealthcareRPM
//
//  Created by Admin on 1/16/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import UIKit

protocol PopoverProtocol: class {
    func onOKClick()
}

class PopoverView: UIView {
    
    @IBOutlet weak var popoverBgView: UIView!
    
    @IBOutlet weak var textView: UITextView!
    
    weak var delegate: PopoverProtocol?
    
    override func didMoveToSuperview() {
        
        textView.adjustsFontForContentSizeCategory = true
        textView.updateTextFont()
        fadeIn()
    }
    
    
    @IBAction func okPressed(_ sender: Any) {
        if (delegate != nil)
        {
            self.delegate!.onOKClick()
            
        }
        fadeOut()
    }
    
    func fadeIn() {
        transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        alpha = 0
        UIView.animate(withDuration: 0.35, animations: {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
        
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.35, animations: {
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 0.0
        }) { finished in
            if finished {
                self.removeFromSuperview()
            }
        }
    }
    
}


extension UITextView {
func updateTextFont() {
    if (self.text.isEmpty || self.bounds.size.equalTo(CGSize.zero)) {
        return;
    }

    let textViewSize = self.frame.size;
    let fixedWidth = textViewSize.width;
    let expectSize = self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))


    var expectFont = self.font
    if (expectSize.height > textViewSize.height) {

        while (self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height > textViewSize.height) {
            expectFont = self.font!.withSize(self.font!.pointSize - 1)
            self.font = expectFont
        }
    }
    else {
        while (self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height < textViewSize.height) {
            expectFont = self.font
            self.font = self.font!.withSize(self.font!.pointSize + 1)
        }
        self.font = expectFont
    }
}
}
