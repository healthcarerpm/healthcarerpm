//
//  LogoutModalViewController.swift
//  HealthcareRPM
//
//  Created by Admin on 2/29/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

protocol LogoutModalDelegate:class {
    func handleTouch()
    func moveToTC()
}


class LogoutModalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tableView: UITableView!
    weak var delegate: LogoutModalDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "logoutPopover")
        tableView.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "logoutPopover", for: indexPath)
    cell.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9450980392, blue: 0.9607843137, alpha: 1)
    cell.selectionStyle = .none
    
    switch indexPath.row {
    case 0:
            cell.textLabel?.text = "LOGOUT"
            cell.textLabel?.font = UIFont(name: "Bogle-Bold", size: 18)
            
    case 1:
            cell.textLabel?.text = "Terms"
            cell.textLabel?.font = UIFont(name: "Bogle-Bold", size: 18)
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.lineBreakMode = .byWordWrapping
    default:
        print("")
    }
    
    return cell
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            logout()
//            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            navigateToTC()
//            self.dismiss(animated: true, completion: nil)

        }
    }
    
    func logout()
    {
        self.delegate?.handleTouch()
    }
    
    func navigateToTC()
    {
        self.delegate?.moveToTC()
        
    }
}
