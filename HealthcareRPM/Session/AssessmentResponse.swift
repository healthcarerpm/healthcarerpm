//
//  AssessmentResponse.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 29/04/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct AssessmentResponse:Codable{
    let PatientId:String
    let AssessmentId:String
    let Name:String
    let AnsweredDateTime:String
    let Questions:[Question]
    let Id:String
    /// ID and session ID are one and the same
    enum CodingKeys: String, CodingKey {
        case PatientId
        case AssessmentId
        case Name
        case AnsweredDateTime
        case Questions
        case Id
    }
    
}

