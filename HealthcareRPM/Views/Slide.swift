import UIKit

class Slide: UICollectionViewCell {
    
    @IBOutlet weak var linechart: LineChart!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelRecs: UILabel!
    
    @IBOutlet weak var buttonConnect: UIButton!
    @IBOutlet weak var buttonDisconnect: UIButton!
    
    @IBOutlet weak var surveyContainerView: UIView!
    
    @IBOutlet weak var buttonStartSurvey: UIButton!
    @IBOutlet weak var buttonAllSession: UIButton!
//    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    //    override func awakeFromNib() {
    //        super.awakeFromNib()
    //        contentView.translatesAutoresizingMaskIntoConstraints = false
    //        contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
    //    }
    
    override func layoutSubviews() {
        let data2: [CGFloat] = [1, 3, 5, 13, 17, 20]
        
        // simple line with custom x axis labels
        let xLabels: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
        
        
//        linechart.animation.enabled = true
//        linechart.area = true
//        linechart.x.labels.visible = false
//        linechart.x.grid.visible = false
//        linechart.y.grid.visible = false
//        linechart.x.labels.values = xLabels
//        linechart.y.labels.visible = false
//        //        linechart.addLine(data)
//        
//        linechart.x.axis.visible = false
//        linechart.y.axis.visible = false
//        //        linechart.addLine(data2)
//        
//        linechart.translatesAutoresizingMaskIntoConstraints = false
 
        
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath

        self.buttonConnect.layer.borderWidth = 0.5
        self.buttonConnect.layer.borderColor = UIColor.white.cgColor
        
        self.buttonDisconnect.layer.borderWidth = 0.5
        self.buttonDisconnect.layer.borderColor = UIColor.white.cgColor
        
        self.buttonDisconnect.layer.cornerRadius = 18
        self.buttonConnect.layer.cornerRadius = 18
        self.buttonConnect.titleLabel?.adjustsFontForContentSizeCategory = true
        self.buttonConnect.titleLabel?.adjustsFontSizeToFitWidth = true
        self.buttonConnect.titleLabel?.minimumScaleFactor = 0.2
        self.buttonConnect.titleLabel?.lineBreakMode = .byWordWrapping
        self.buttonConnect.titleLabel?.numberOfLines = 1
      
        self.buttonDisconnect.titleLabel?.adjustsFontForContentSizeCategory = true
        self.buttonDisconnect.titleLabel?.adjustsFontSizeToFitWidth = true
        self.buttonDisconnect.titleLabel?.minimumScaleFactor = 0.2
        self.buttonDisconnect.titleLabel?.lineBreakMode = .byWordWrapping
        self.buttonDisconnect.titleLabel?.numberOfLines = 1
        
      
        
        self.contentView.isUserInteractionEnabled = false
    }
}

