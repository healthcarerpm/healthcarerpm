//
//  HPSSignaturePopup.swift
//  healthpointe
//
//  Created by Veeresh on 11/09/19.
//  Copyright © 2019 . All rights reserved.
//

import UIKit

protocol RPMSignaturePopoverDelegate {
    func didSendSignature(signatureImage : UIImage, popup:RPMSignaturePopup)
    func showSigAlert (title: String, message: String)
}

class RPMSignaturePopup: UIView, YPSignatureDelegate {
 
    func didStart(_ view: YPDrawSignatureView) {
      //  self.doneBtn.setBackgroundImage(UIImage.init(named: "Done"), for: .normal)
        // self.doneBtn.setImage(UIImage().setSpecificImageBasedOnKey(key: HPSProjectSettings.sharedProjectSettings.signDoneKey), for: .normal)
        self.doneBtn.isEnabled = true
        self.clearSigBtn.isHidden = false
    }
    
    func didFinish(_ view: YPDrawSignatureView) {
    }

    @IBOutlet weak var clearSigBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var bgSignatureView: YPDrawSignatureView!
    var delegate: RPMSignaturePopoverDelegate?
    
    override func didMoveToSuperview() {
        fadeIn()
       // self.clearSigBtn.setTitleColor(Constants().LINK_COLOR, for: .normal)
        self.signatureView.layer.borderWidth = 1
       // self.signatureView.layer.borderColor = Constants().ALMOST_BLACK.cgColor
        self.signatureView.delegate = self
        self.doneBtn.setImage(UIImage.init(named: "grayDoneBtn"), for: .normal)
        self.doneBtn.isEnabled = false
        self.clearSigBtn.isHidden = true
    }
    
    func fadeIn() {
        transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        alpha = 0
        UIView.animate(withDuration: 0.35, animations: {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
        
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.35, animations: {
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 0.0
        }) { finished in
            if finished {
                self.removeFromSuperview()
            }
            
        }
    }
    
    @IBAction func clearSignature(_ sender: UIButton) {
        // This is how the signature gets cleared
        self.signatureView.clear()
    }
    
    
    @IBAction func saveSignature(_ sender: UIButton) {
        // Getting the Signature Image from self.drawSignatureView using the method getSignature().
        if let signatureImage = self.signatureView.getSignature(scale: 10) {
            
            if let sendSignature = delegate?.didSendSignature{
                sendSignature(signatureImage,self)
            }
        }
        else {
            delegate?.showSigAlert(title: "Alert", message: "Please complete your signature.")
        }
    }
}
