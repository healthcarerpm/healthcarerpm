//
//  BluetoothReadingViewController.h
//
//  Created by Igor Vasilev on 5/26/16.
//  Copyright © 2016 OdysseyInc, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BluetoothResultsViewController.h"

@class BluetoothReadingViewController;

@protocol BluetoothReadingViewControllerDelegate

-(void)BluetoothDevicedidcapturemeasurment:(NSString *)measurementType valuedict:(NSDictionary * _Nonnull)valuesdictionary;

@end


@interface BluetoothReadingViewController : UIViewController <BluetoothResultsViewControllerDelegate>

@property(nonatomic, weak, nullable) id<BluetoothReadingViewControllerDelegate> delegate;
@property(nonatomic, assign) UIViewController* _Nullable callingController;
@property (nonatomic, strong, nullable) NSNumber *isfromHealthSession;

@end
