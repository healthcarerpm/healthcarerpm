//
//  BluetoothReadingViewController.m
//
//  Created by Igor Vasilev on 5/26/16.
//  Copyright © 2016 OdysseyInc, LLC. All rights reserved.
//

#import "BluetoothReadingViewController.h"
#import "BluetoothResultsViewController.h"


#import "ANDDevice.h"
#import "ADBloodPressure.h"



/*
 The error system must be redone
 */
#define BLUETOOTH_COMMUNICATION_DOMAIN		@"BluetoothReadingErrorDomain"
#define INVALID_BUFFER_CODE					172

#define READING_TIMEOUT						(5.0 * 60.0)//5 minutes

@interface BluetoothReadingViewController () <ANDDeviceDelegate>

@property (nonatomic, strong) ANDDevice *device;
@property (nonatomic, strong) NSMutableDictionary *devices;
@property (nonatomic,strong) NSMutableDictionary *measurementData;
@property NSString *type;

@end
@implementation BluetoothReadingViewController
-(void)viewDidLoad
{
    self.device = [ANDDevice new];
    [self.device controlSetup];
    self.device.delegate = self;
    
    if (self.device.activePeripheral.state != CBPeripheralStateConnected) {
        if (self.device.activePeripheral) self.device.peripherials = nil;
        [self.device findBLEPeripherals];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"Entered into reading view controller");
    [self.device findBLEPeripherals];
}

- (void)gotDevice:(CBPeripheral *)peripheral
{
    NSLog(@"peripheral name is %@", peripheral.name);
    if ([peripheral.name rangeOfString:@"A&D"].location != NSNotFound) {
        if (![self.devices objectForKey:peripheral.name]) {
            if (peripheral.name != nil) {
                self.devices = [[NSMutableDictionary alloc]init];
                [self.devices setValue:peripheral forKey:peripheral.name];
            }
        }
    }
    
    if (self.devices.count > 0) {
        CBPeripheral *device = [self.devices allValues].firstObject;
        [self.device connectPeripheral:device];
    }
    
}

- (void) showMeasurement:(NSDictionary*)measurement
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BluetoothReading" bundle:nil];
    BluetoothResultsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BluetoothResultsViewController"];
    vc.callingController = self.callingController;
    vc.isfromHSorAdHoc = self.isfromHealthSession;
    vc.delegate = (BluetoothReadingViewController *)self;
    [vc applyMeasurement:measurement];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) showError:(NSError*)error
{
    NSString* message;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:NULL];
}

- (void) deviceReady
{
    self.device.connectionStats = @"Connected";
    ADBloodPressure *bp = [[ADBloodPressure alloc] initWithDevice:self.device];
    [bp setTime];
    [bp readMeasurementForSetup];
    [self.device readDeviceInformation];
}

- (void) gotActivity:(NSData *)data
{
    [self.device.CM cancelPeripheralConnection:self.device.activePeripheral];
    [self.device findBLEPeripherals];
}

- (void)gotBloodPressure:(NSDictionary *)data
{
    _measurementData = [[NSMutableDictionary alloc]initWithDictionary:data];
    
    //  HHM-651 - map error values to back button
    if ([[_measurementData valueForKey:@"systolic"] isEqual:@2047] & [[_measurementData valueForKey:@"diastolic"] isEqual:@2047]) {
        [self returnToCallingController];
    } else {
        NSLog(@"***** data %@", data);
        [self.device.CM cancelPeripheralConnection:self.device.activePeripheral];
    //    [self.device findBLEPeripherals];
        [self showMeasurement:_measurementData];
    }
}

- (IBAction)backButtonPressed:(id)sender {
    [self returnToCallingController];
}

- (void) returnToCallingController {
        if (self.callingController) {
            [self.navigationController popToViewController:self.callingController animated:YES];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }

}

- (void)bluetoothResultsViewControllerDidFinishWithMeasurement:(NSString *)type valuedict:(NSDictionary * _Nullable)valuesdictionary {
    
    [self.delegate BluetoothDevicedidcapturemeasurment:@"BP" valuedict:valuesdictionary];
    
}

- (void)bluetoothResultsViewControllerDidFinishWithRetakeReading:(BluetoothResultsViewController * _Nonnull)controller {
    
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    
}



@end



