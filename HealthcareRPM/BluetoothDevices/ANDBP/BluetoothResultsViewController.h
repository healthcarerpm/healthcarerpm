//
//  BluetoothResultsViewController.h
//
//  Created by Joshua Ryan on 5/26/16.
//  Copyright © 2016 OdysseyInc, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>


@class BluetoothResultsViewController;


@protocol BluetoothResultsViewControllerDelegate <NSObject>

- (void) bluetoothResultsViewControllerDidFinishWithRetakeReading:(BluetoothResultsViewController* _Nonnull)controller;

-(void)bluetoothResultsViewControllerDidFinishWithMeasurement:(NSString *)type valuedict:(NSDictionary *_Nullable)valuesdictionary;


@end


@interface BluetoothResultsViewController : UIViewController

@property(nonatomic, nullable, weak) id<BluetoothResultsViewControllerDelegate> delegate;
@property(nonatomic, assign, nullable) UIViewController* callingController;
@property (nonatomic, assign , nullable) NSNumber *isfromHSorAdHoc;

- (void) applyMeasurement:(NSDictionary* _Nonnull)measurement;

@end


