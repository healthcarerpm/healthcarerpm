//
//  ADWeightScale.m
//  BLE2
//
//  Created by Chenchen Zheng on 1/6/14.
//  Copyright (c) 2014 Chenchen Zheng. All rights reserved.
//

#import "ADWeightScale.h"
//#import "WCSQLite.h"
#import "ANDBLEDefines.h"

@implementation ADWeightScale

@synthesize measurementID = _measurementID;
@synthesize measurementTime = _measurementTime;
@synthesize measurementReceivedTime = _measurementReceivedTime;
@synthesize WSWeight = _WSWeight;
@synthesize units = _units;
@synthesize bmi = _bmi;

- (id)initWithMT:(NSString *)measurementTime MRT:(NSString *)measurementReceivedTime Weight:(NSString *)weight Unit:(NSString *)unit Bmi:(NSString *)bmi UID:(NSString *)UserID isM:(NSString *)isManualInput
{
  _measurementTime = measurementTime;
  _measurementReceivedTime = measurementReceivedTime;
  _WSWeight = [NSNumber numberWithFloat: [weight floatValue]];
  _units =  unit;
  _bmi = [NSNumber numberWithFloat:[bmi floatValue]];
  _userID = [NSNumber numberWithInt:[UserID intValue]];
  _isManualInput = [NSNumber numberWithInt:[isManualInput intValue]];
  
  return self;
}



#pragma mark - private methods

- (NSNumber *)weightInKG
{
  NSNumber *result = self.WSWeight;
  if ([self.units isEqualToString: @"lb"]) {
    double tmp = [self.WSWeight doubleValue] / 2.20462262185;
    NSLog(@"weightInKG tmp is %f", tmp);
    result = [NSNumber numberWithDouble:tmp];
  }
  NSString *tmp2 = [NSString stringWithFormat:@"%.2f", [result doubleValue]];
  result =[NSNumber numberWithDouble:[tmp2 doubleValue]];
  
  NSLog(@"KG result is %f", [result doubleValue]);
  return result;
}

- (NSNumber *)weightInLB
{
  NSNumber *result = self.WSWeight;
  if ([self.units isEqualToString: @"kg"]) {
    double tmp = [self.WSWeight doubleValue] * 2.20462262185;
    NSLog(@"weightInLB tmp is %f", tmp);
    result =[NSNumber numberWithDouble:tmp];
  }
  NSString *tmp2 = [NSString stringWithFormat:@"%.2f", [result doubleValue]];
  result =[NSNumber numberWithDouble:[tmp2 doubleValue]];
  
  NSLog(@"LB result is %f", [result doubleValue]);
  return result;
}

- (NSDictionary *) printProperWeight
{
  NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:2];
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  NSString *unitDefault = [defaults objectForKey:@"Units"];
  if ([unitDefault isEqualToString:@"Metrics (Kilograms, Meters)"])
    unitDefault = @"kg";
  else unitDefault = @"lb";
  if (![unitDefault isEqualToString:self.units])
    {
    if ([self.units isEqualToString:@"kg"]) {
      //convert kg to pound
      double tmp = [self.WSWeight doubleValue] * 2.20462262185;
      NSLog(@"tmp is %.5f", [self.WSWeight doubleValue]);
      [result setObject:[NSString stringWithFormat:@"%0.1f", tmp] forKey:@"weight"];
      [result setObject:@"lb" forKey:@"unit"];
    } else {
      //convert pound to kg
      double tmp = [self.WSWeight doubleValue] * 1/2.20462262185;
      [result setObject:[NSString stringWithFormat:@"%0.1f", tmp] forKey:@"weight"];
      [result setObject:@"kg" forKey:@"unit"];
    }
    } else {
      double tmp = [self.WSWeight doubleValue];
      [result setObject:[NSString stringWithFormat:@"%0.1f", tmp] forKey:@"weight"];
      [result setObject:self.units forKey:@"unit"];
    }
  return result;
}


-(id)initWithDevice:(ANDDevice *)device
{
  self = [super init];
  if (self) {
    self.activePeripheral = device.activePeripheral;
    self.CM = device.CM;
    self.peripherials = device.peripherials;
    self.delegate = device.delegate;
      // WCJ
      //self.andDevice = device;
  }
  return self;
}


#pragma mark - Write Time Stamp For Weight Scale
- (void) setTime
{
    NSLog(@"ws set time");
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    
    NSInteger year = components.year;
    NSMutableData *yearData = [[NSMutableData alloc] initWithBytes:&year length:sizeof(year)];
    int year1 = *(int *)[[yearData subdataWithRange:NSMakeRange(0, 1)] bytes];
    int year2 = *(int *)[[yearData subdataWithRange:NSMakeRange(1, 1)] bytes];
    
    int month = components.month;
    
    int day = components.day;
    
    int hour = components.hour;
    
    int min = components.minute;
    
    int second = components.second;
    
    char bytes[7];
    
    bytes[0] = year1;
    
    bytes[1] = year2;
    
    bytes[2] = month;
    
    bytes[3] = day;
    
    bytes[4] = hour;
    
    bytes[5] = min;
    
    bytes[6] = second;
    
    
    NSData *data = [[NSData alloc] initWithBytes:&bytes length:sizeof(bytes)];
    NSLog(@"data is %@", data);
    

    NSString* uuidStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"weightScaleServiceUUID"];
    [self writeValue:[CBUUID UUIDWithString: uuidStr]
  characteristicUUID:[CBUUID UUIDWithString: DateTime_Char]
                   p:self.activePeripheral
                data:data];
    
}

- (void)readMeasurement
{
    NSLog(@"ws readMeasurement");
    
    NSString* uuidStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"weightScaleServiceUUID"];
    
    if ([uuidStr isEqualToString:WeightScale_Service_New]) {
        [self readValue:[CBUUID UUIDWithString:WeightScale_Service_New]
     characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                      p:self.activePeripheral];
        
        [self notification:[CBUUID UUIDWithString:WeightScale_Service_New]
        characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                         p:self.activePeripheral on:YES];
        
        
        [self readValue:[CBUUID UUIDWithString:WeightScale_Service_New]
     characteristicUUID:[CBUUID UUIDWithString:WeightScaleMeasurement_Char_New]
                      p:self.activePeripheral];
        
        [self notification:[CBUUID UUIDWithString:WeightScale_Service_New]
        characteristicUUID:[CBUUID UUIDWithString:WeightScaleMeasurement_Char_New]
                         p:self.activePeripheral on:YES];
        
    }
    if ([uuidStr isEqualToString:WeightScale_Service_Original]) {
        [self readValue:[CBUUID UUIDWithString:WeightScale_Service_Original]
     characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                      p:self.activePeripheral];
        
        [self notification:[CBUUID UUIDWithString:WeightScale_Service_Original]
        characteristicUUID:[CBUUID UUIDWithString:DateTime_Char]
                         p:self.activePeripheral on:YES];
        
        
        [self readValue:[CBUUID UUIDWithString:WeightScale_Service_Original]
     characteristicUUID:[CBUUID UUIDWithString:WeightScaleMeasurement_Char_Original]
                      p:self.activePeripheral];
        
        [self notification:[CBUUID UUIDWithString:WeightScale_Service_Original]
        characteristicUUID:[CBUUID UUIDWithString:WeightScaleMeasurement_Char_Original]
                         p:self.activePeripheral on:YES];
        
    }
    

}

-(void)readMeasurementForSetup
{
    NSLog(@"Enter readMeasurementForSetup within weight scale ");
    [self notification:[CBUUID UUIDWithString:WeightScale_Service_New]
    characteristicUUID:[CBUUID UUIDWithString:WeightScaleMeasurement_Char_New]
                     p:self.activePeripheral on:YES];
}




@end
