//
//  JSONTraverserHelper.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation
import Alamofire
import CoreData

class JSONTraverserHelper{
    
    
    static func doJSONTraversalSessionRequest(data:Data){
        
        let parentElementArray = JSONTraverser(data: data)
        for parentElement in parentElementArray {
            
            let assessmentArray = parentElement.get("Assessments")
            
            let patientId = parentElement.get("PatientId").string!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy H:mm"
            
            for assessment in assessmentArray {
                let serverDateTimeString = "\(assessment.get("Date").string ?? "") 00:00 "
                
                let utcDateTime = dateFormatter.date(from: serverDateTimeString)
                
                let serverStartDateTimeString = "\(assessment.get("Date").string ?? "") \(assessment.get("StartTime").string ?? "") "
                let serverEndDateTimeString = "\(assessment.get("Date").string ?? "") \(assessment.get("EndTime").string ?? "") "

                let utcStartDateTime = dateFormatter.date(from: serverStartDateTimeString)
                let utcEndDateTime = dateFormatter.date(from: serverEndDateTimeString)
                let utcAssessmentLasUpdateDateTime = dateFormatter.date(from: assessment.get("AssessmentLastUpdated").string!)
                print("sessionId-\(assessment.get("Id").string!)")
                CoreDataUtils().saveSessionFromServer(sessionId:assessment.get("Id").string!,assessmentId: assessment.get("AssessmentId").string!,date:utcDateTime, name:assessment.get("Name").string, patientId: patientId, startTime: utcStartDateTime, endTime: utcEndDateTime, assessmentLastUpdated: utcAssessmentLasUpdateDateTime)
                
            }
        }
    }
    
   
    
    static func doJSONTraversalAssessmentNodeRequest(data:Data,sessionId: String){

        let assessmentId:String = JSONTraverser(data: data).get("AssessmentId").string!
        let parentElement = JSONTraverser(data: data).get("Questions")

        let val = dfs(assessmentId:assessmentId,sessionId:sessionId,node: parentElement,parentId:assessmentId)
        
    }
    
    let nodeList = [Node]()
    
    
    static func dfs(assessmentId:String,sessionId:String, node:JSONTraverser?,parentId:String?) ->Bool{
        
        if(node != nil){
            var idx = 0
            for parent in node! where parent != nil {
                let nodeId:String = parent.get("Id").string!
                var nodeText:String? = parent.get("Text").string ?? nil
                let nodeValue:String? = parent.get("Value").string ?? nil
                let measurementTypeId:String? = parent.get("MeasurementTypeId").string ?? nil
                let nodeType:Node.NodeType = Node.NodeType(rawValue: parent.get("Type").string!.lowercased()) ?? Node.NodeType.option
                //hack for media
                if(nodeType == Node.NodeType.media){
                //get url attribute and add it as value
                    nodeText = parent.get("url").string ?? nil
                 }
                let alertType:Node.AlertType? = Node.AlertType(rawValue: (parent.get("Alert").string ?? Node.AlertType.colorless.rawValue).lowercased())
                idx += 1
                _ = idx
                var nodeOptions = [String]()
                var nodeChildren = [String]()

                let childElement:JSONTraverser = parent.get("Childern")
                for child in childElement  where child != nil
                {
                    let childId:String = child.get("Id").string!
                    
                    if(child.get("Type").string! == "Option"){
                        //add option id to option array
                        nodeOptions.append(childId)
                    }
                    else{
                        //else add to childrenarray
                        nodeChildren.append(childId)
                    }
                    
                }

                CoreDataUtils().saveNodeFromServer(id: nodeId, assessmentId: assessmentId, sessionId:sessionId, text: nodeText, value: nodeValue, nodeType: nodeType, alertType: alertType!, parentId: parentId,measurementTypeId:measurementTypeId, options: nodeOptions, children: nodeChildren)
                if(childElement != nil){
                    dfs(assessmentId:assessmentId,sessionId: sessionId, node:childElement, parentId: nodeId)
                }
                //endof parent loop
            }
            //endof if
        }
        return true
    }
    
}

