//
//  JSONTraverser.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 07/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

// TODO: implment Sequence

// TODO: can the values be limited
// to just these supported JSON ones?
//public protocol JSONValue {}
//extension Bool: JSONValue {}
//extension Int: JSONValue {}
//extension Float: JSONValue {}
//extension Double: JSONValue {}
//extension CGFloat: JSONValue {}
//extension String: JSONValue {}
//extension Array: JSONValue {}
//extension Dictionary: JSONValue {}

public typealias JSONArray = [Any]
public typealias JSONDictionary = [String: Any]

// MARK: -
public struct JSONTraverser {
    
    /// The value of the item
    public let value: Any?
    
    /// Create an instance with any object
    public init(_ value: Any? = nil) {
        self.value = value
    }
    
    /// Create an instance with json data
    public init(data: Data?) {
        if data != nil {
            self.init(try? JSONSerialization.jsonObject(with: data!, options: []))
        } else {
            self.init()
        }
    }
    
}

// MARK: - Get
extension JSONTraverser {
    
    /// Get the item with a given index, if one exists.
    /// Negative numbers work back from the length of the array,
    /// -1 for example will return the last objexct, -2 the second to last
    public func get(_ index: Int) -> JSONTraverser {
        guard let array = self.array, index < array.count else {
            return JSONTraverser()
        }
        return JSONTraverser(array[index < 0 ? array.count+index : index])
    }
    
    /// Get the item with a given key, if one exists.
    public func get(_ key: String) -> JSONTraverser {
        return JSONTraverser(self.dictionary?[key])
    }
    
}

// MARK: - Keypath
extension JSONTraverser {
    
    /// Paw.app style keypaths:
    /// "company.employees[0].name" <- get the name of the first employee in the company
    /// "company.employees[-1].name" <- get the name of the last employee in the company
    public func keypath(_ keypath: String) -> JSONTraverser {
        var json = self
        for dotPart in keypath.components(separatedBy: ".") {
            for bracketPart in dotPart.components(separatedBy: "[") {
                if let index = Int(bracketPart.replacingOccurrences(of: "]", with: "")) {
                    json = json.get(index)
                } else if bracketPart.isEmpty == false {
                    json = json.get(bracketPart)
                }
            }
        }
        return json
    }
    
}

// MARK: - Value helpers
extension JSONTraverser {
    
    public var bool: Bool? {
        return self.value as? Bool
    }
    
    public var int: Int? {
        return self.value as? Int
    }
    
    public var integer: Int? {
        return self.int
    }
    
    public var float: Float? {
        return self.value as? Float
    }
    
    public var double: Double? {
        return self.value as? Double
    }
    
    public var string: String? {
        return self.value as? String
    }
    
    public var array: JSONArray? {
        return self.value as? JSONArray
    }
    
    public var dictionary: JSONDictionary? {
        return self.value as? JSONDictionary
    }
    
}

// MARK: - Subscript
extension JSONTraverser {
    
    /// Subscript for indices
    public subscript(index: Int) -> JSONTraverser {
        return self.get(index)
    }
    
    /// Subscript for keys
    public subscript(key: String) -> JSONTraverser {
        return self.get(key)
    }
    
}

// MARK: - First/Last
extension JSONTraverser {
    
    /// Get the first item of the array
    public var first: JSONTraverser {
        return self.get(0)
    }
    
    /// Get the last item of the array
    public var last: JSONTraverser {
        return self.get(-1)
    }
    
    
}

public struct JSONTraverserGenerator : IteratorProtocol{
    
    public typealias Element = JSONTraverser
    let value:JSONTraverser
    var currentIdx = 0
    
    init(value:JSONTraverser){
        self.value = value
    }
    
    public mutating func next() -> Element? {
        let c = self.value.array?.count != nil ? (self.value.array?.count)! : 0
        guard currentIdx < c else {
            return nil
        }
        defer{
            currentIdx+=1
        }
        return self.value.get(currentIdx)
    }
    
}

extension JSONTraverser : Sequence {
    public func makeIterator() -> JSONTraverserGenerator {
        return JSONTraverserGenerator(value: self)
    }
}

// MARK: - Description
extension JSONTraverser: CustomStringConvertible {
    public var description: String {
        if self.value == nil {
            // TODO: What's the best thing to print?
            return "[]"
        }
        
        return "\(self.value!)"
    }
}

// MARK: - Equatable
extension JSONTraverser: Equatable {}
public func ==(lhs: JSONTraverser, rhs: JSONTraverser) -> Bool {
    let lhv = lhs.value, rhv = rhs.value
    if lhv == nil && rhv == nil {
        return true
    } else if lhv is Bool && rhv is Bool && lhv as? Bool == rhv as? Bool {
        return true
    } else if lhv is Int && rhv is Int && lhv as? Int == rhv as? Int {
        return true
    } else if lhv is Double && rhv is Double && lhv as? Double == rhv as? Double {
        return true
    } else if lhv is String && rhv is String && lhv as? String == rhv as? String {
        return true
    } else if let lha = lhv as? JSONArray, let rha = rhv as? JSONArray {
        return NSArray(array: lha).isEqual(to: rha)
    } else if let lhd = lhv as? JSONDictionary, let rhd = rhv as? JSONDictionary {
        return NSDictionary(dictionary: lhd).isEqual(to: rhd)
    }
    return false
}
