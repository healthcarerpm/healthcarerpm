//
//  MessageModel.swift
//  HealthcareRPM
//
//  Created by Barath Vijayaraghavan on 9/30/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct Message {
    let fromId:String?
    let toID:String?
    let value:String?
    let creationDate:String?
    let frompatient:Bool?
    let messageId:String?
    let patientId:String?
    let fromUserName:String?
    
    init(fromId:String? = nil, toID:String? = nil, value:String? = nil, creationDate:String? = nil, frompatient:Bool = false,messageId:String? = nil, patientId:String? = nil, fromuserName:String? = nil) {
        self.fromId = fromId
        self.toID = toID
        self.value = value
        self.creationDate = creationDate
        self.frompatient = frompatient
        self.messageId = messageId
        self.patientId = patientId
        self.fromUserName = fromuserName
    }
}
//
//struct SelfMessage {
//    let message:String?
//    let date:String?
//    let from:String?
//}
