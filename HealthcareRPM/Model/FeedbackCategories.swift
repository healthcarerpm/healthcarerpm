//
//  FeedbackCategories.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 07/04/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import UIKit

class FeedbackCategories {
    
    var feedbackCategoryID:Int?
    var categoryDescription:String?
    
    init(_ feedbackCategories:[String:Any]) {
        self.feedbackCategoryID = feedbackCategories["FeedbackCategoryId"] as? Int
        self.categoryDescription = feedbackCategories["CategoryDescription"] as? String
    }
}

