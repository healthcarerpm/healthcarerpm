//
//  RPMPatientCondition.swift
//  HealthcareRPM
//
//  Created by veeresh rm on 28/01/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class RPMPatientCondition: NSObject {

    var iD = ""
    var name = ""
    var sequence = 0
    var category = ""
    var customerId = ""
    var customer = ""
    
    init(patientCondition : [String:Any]) {
        iD = patientCondition["Id"] as! String
        name = patientCondition["Name"] as! String
        sequence = patientCondition["Sequence"] as! Int
        category = patientCondition["Category"] as! String
        customerId = patientCondition["CustomerId"] as! String
    }
}
