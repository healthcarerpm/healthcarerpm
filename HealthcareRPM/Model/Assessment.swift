//
//  Assessment.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation
import CoreData


//
//  Assessment.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation
import CoreData


public class Assessment: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Assessment> {
        return NSFetchRequest<Assessment>(entityName: "Assessment")
    }
    
    @NSManaged public var sessionId: String
    @NSManaged public var assessmentId: String
    @NSManaged public var name: String?
    @NSManaged public var patientId: String?
    @NSManaged public var date: Date?
    @NSManaged public var startTime: Date?
    @NSManaged public var endTime: Date?
    @NSManaged public var isCompleted : Bool
    @NSManaged public var isSubmitted : Bool
    @NSManaged public var isStarted : Bool
    @NSManaged public var assessmentLastUpdated: Date?
    @NSManaged public var answeredDateTime: Date?
    
    //    @NSManaged public var node: NSSet?
    
    
    convenience init(sessionId: String,assessmentId:String,name: String? = nil ,patientId: String? = nil,date:Date?,startTime: Date?,endTime: Date?,assessmentLastUpdated: Date?,answeredDateTime:Date?,insertIntoManagedObjectContext context: NSManagedObjectContext!){
        let entity = NSEntityDescription.entity(forEntityName: "Assessment", in: context)!
        self.init(entity: entity, insertInto: context)
        self.sessionId = sessionId
        self.assessmentId = assessmentId
        self.name = name
        self.patientId = patientId
        self.date = date
        self.startTime = startTime
        self.endTime = endTime
        self.assessmentLastUpdated = assessmentLastUpdated
        self.answeredDateTime = answeredDateTime
    }
    
}

// MARK: Generated accessors for node
extension Assessment {
    
    @objc(addNodeObject:)
    @NSManaged public func addToNode(_ value: Node)
    
    @objc(removeNodeObject:)
    @NSManaged public func removeFromNode(_ value: Node)
    
    @objc(addNode:)
    @NSManaged public func addToNode(_ values: NSSet)
    
    @objc(removeNode:)
    @NSManaged public func removeFromNode(_ values: NSSet)
    
}
