//
//  RPMConsentDataBase.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 09/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMConsentDataBase: NSObject {

    class func configuredConsentDetails()->Array<RPMConsentModel>{
        
        return[RPMConsentModel(descriptionText:"I understand that:",isOn: false,type: "text"),
               RPMConsentModel(descriptionText:"I am the only person who should be using the remote monitoring equipment as instructed.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"I will not use the device for reasons other than my own personal health monitoring.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"I understand that I can only participate in this program with one Medical Provider at a time.",isOn:false,type:"switch"),
               
               RPMConsentModel(descriptionText:"I understand that I am responsible for any fees associated with misuse of the equipment.",isOn:false,type:"switch"),
              
               RPMConsentModel(descriptionText:"The devices is meant to collect vital Readings and transfer those readings to an online website.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"It is NOT AN EMERGENCY RESPONSE UNIT AND IS NOT MONITORED 24/7. Call 911 for immediate medical emergencies.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"I am aware my daily readings will be transmitted from the monitor to a website in a safe and secure manner.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"I can withdraw my consent to participate in this program, and revoke service at any time.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"The website will securely and confidentially store my collected data.",isOn:false,type:"switch"),
              
               RPMConsentModel(descriptionText:"I am aware that a Remote Patient Monitoring Qualified Health Professional will only view my readings, and that this program is NOT a 24/7 Monitoring Service.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"I will be contacted by phone, to review and discuss my results and progress.",isOn:false,type:"switch"),
               RPMConsentModel(descriptionText:"I have read and understood the information and consent to participate in the Remote Patient Monitoring program as stated above.",isOn:false,type:"switch"),
               
               RPMConsentModel(descriptionText:"",isOn:false,type:"signature")
        ]
        //RPMConsentModel(descriptionText:"I am aware that this consent is valid as long as I’m in possession of the RPM equipment/device.",isOn:false,type:"text"),
        // RPMConsentModel(descriptionText:"I will do my best to take my sessions every day.",isOn:false,type:"switch"),
         //RPMConsentModel(descriptionText:"I understand the devices are only designed for the RPM program.",isOn:false,type:"switch"),
        //RPMConsentModel(descriptionText:"I will not tamper with the equipment",isOn:false,type:"switch"),
    }
}
