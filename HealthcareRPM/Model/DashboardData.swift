//
//  DashboardData.swift
//  HealthcareRPM
//
//  Created by Isham M on 09/05/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct DashboardData {
    var measurementArray: [CGFloat]
    var secondaryMeasurementArray: [CGFloat]
}
