//
//  UserDetails.swift
//  HealthcareRPM
//
//  Created by Isham on 17/01/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct UserDetails {
    var patientId: String?
    var firstName: String?
    var lastName: String?
    var lastLogin: String?
}
