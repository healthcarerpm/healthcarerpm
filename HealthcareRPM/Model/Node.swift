//
//  Node.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 12/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//
import Foundation
import CoreData


public class Node: NSManagedObject {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Node> {
        return NSFetchRequest<Node>(entityName: "Node")
    }
    @NSManaged public var id: String
    @NSManaged public var assessmentId: String
    @NSManaged public var sessionId: String
    @NSManaged public var text: String?
    @NSManaged public var answerId: String?
    @NSManaged public var value: String?
    @NSManaged fileprivate var nodeType_raw:String
    @NSManaged fileprivate var alertType_raw:String?
    @NSManaged public var parentId: String?
    @NSManaged public var measurementTypeId:String?
    @NSManaged public var measurementDateTime: Date?
    @NSManaged public var options: [String]?
    @NSManaged public var children: [String]?
    @NSManaged public var orderNumber: String?
    @NSManaged public var isTaken: Bool
    @NSManaged public var isCompleted: Bool
    @NSManaged public var measurementValueTypeId:String?
    @NSManaged public var measurementValue:String?
    
    convenience init(id:String,assessmentId: String, sessionId:String,text:String? = nil,answerId: String? = nil, value:String? = nil,nodeType:String = NodeType.option.rawValue, alertType:String? = AlertType.colorless.rawValue, parentId:String? = nil,measurementTypeId:String? = nil, measurementDateTime:Date? = nil,options:[String]? = nil,children:[String]? = nil,isTaken:Bool = false,isCompleted:Bool = false,orderNumber:String? = nil , insertIntoManagedObjectContext context: NSManagedObjectContext!,measurementValueTypeId:String? = nil,measurementValue:String? = nil){
        let entity = NSEntityDescription.entity(forEntityName: "Node", in: context)!
        self.init(entity: entity, insertInto: context)
        self.id = id
        self.assessmentId = assessmentId
        self.sessionId = sessionId
        self.text = text
        self.answerId = answerId
        self.value = value
        self.nodeType_raw = nodeType
        self.alertType_raw = alertType
        self.parentId = parentId
        self.measurementTypeId = measurementTypeId
        self.measurementDateTime = measurementDateTime
        self.options = options
        self.children = children
        self.isTaken = isTaken
        self.isCompleted = isCompleted
        self.orderNumber = orderNumber
        self.measurementValueTypeId = measurementValueTypeId
        self.measurementValue = measurementValue
    }
    
    static func ==(_ lhs: Node, _ rhs: Node) -> Bool {
        return (lhs.id == rhs.id)
            && (lhs.assessmentId == rhs.assessmentId)
            && (lhs.sessionId == rhs.sessionId)
            && (lhs.text == rhs.text)
            && (lhs.answerId == rhs.answerId)
            && (lhs.value == rhs.value)
            && (lhs.measurementValueTypeId == rhs.measurementValueTypeId)
            && (lhs.measurementValue == rhs.measurementValue)
            && (lhs.nodeType_raw == rhs.nodeType_raw)
            && (lhs.alertType_raw == rhs.alertType_raw)
            && (lhs.measurementTypeId == rhs.measurementTypeId)
            && (lhs.measurementDateTime == rhs.measurementDateTime)
            && (lhs.options == rhs.options)
            && (lhs.children == rhs.children)
            && (lhs.isTaken == rhs.isTaken)
            && (lhs.isCompleted == rhs.isCompleted)
            && (lhs.orderNumber == rhs.orderNumber)
    }
}

extension Node{
    
    public enum NodeType: String {
        case option = "option"
        case measurement = "measurement"
        case radio = "radio"
        case alert = "alert"
        case check = "check"
        case date = "date"
        case number = "number"
        case media = "media"
        case video = "video"
        case freetext = "freetext"
    }
    
    public enum AlertType : String {
        case red = "red"
        case green = "green"
        case colorless = ""
    }
}
extension Node{
    var nodeType:NodeType{
        get{
            return NodeType(rawValue: self.nodeType_raw)!
        }
        set{
            self.nodeType_raw = newValue.rawValue
        }
    }
    
    var alertType:AlertType?{
        get{
            if let value = self.alertType_raw {
                return AlertType(rawValue: value)!
            }
            return nil
        }
        set{
            self.alertType_raw = newValue!.rawValue
        }
    }
    
}

extension Date {
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> TimeInterval {
        let duration =  Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
        return Double(abs(duration))
    }
}
