//
//  MeasurementIdentifier.swift
//  HealthcareRPM
//
//  Created by Himanshu on 4/7/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

class MeasurementIdentifier:NSObject{
    
    static let sharedInteractor: MeasurementIdentifier = {
        let sharedInteractor = MeasurementIdentifier()
        return sharedInteractor
    }()
    
    fileprivate override init() {
        super.init()
    }
    var customerId:String?
    var customerName:String?
    var isfromdashboardforTerms = true

    var systolicID:String?
    var diastolicID:String?
    var spo2ID:String?
    var pulseID:String?
    var bloodsugar:String?
    var temperatureID:String?
    var weightID:String?
    
    /* Start and end for validations*/
    var weightstart:Double?
    var weightEnd:Double?
    var diastolicStart:Int?
    var diastolicEnd:Int?
    var systolicStart:Int?
    var systolicEnd:Int?
    var Spo2start:Int?
    var temperatueEnd:Double?
    var temperatureStart:Double?
    var heartRateStart:Int?
    var heartrateEnd:Int?
    var bgStart:Double?
    var bgEnd:Double?
    
    /* Patient ID
     */
    var patientId:String?
    var messages:[Message]? = []
    var initialmessagecounter: Int = 0
    var datetimelastRead: String?
   // var selfmessages:[SelfMessage] = []
    var isEmployer = 0
    var IsMessagingAllowed = true
    //repeating timer
    let repeatingTimer = RepeatingTimer(timeInterval: 30)
    var InitialInstructionsHeading : String?
    var InitialInstructions : String?
}
