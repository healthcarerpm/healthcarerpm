//
//  MeasurementType.swift
//  HealthcareRPM
//
//  Created by Isham on 06/02/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct MeasurementModel: Decodable {
    let data: [Measurement]
}

struct Measurement: Decodable {
    let id: String?
    let Name: String?
    let Unit: String?
    
    
    private enum CodingKeys: String, CodingKey {
        case id = "Id"
        case Name = "Name"
        case Unit = "Unit"
    }
}

struct MeasurementValue:Decodable{
    let id:String?
    let Name:String?
    
    private enum CodingKeys: String,CodingKey{
        case id = "id"
        case Name = "Name"
    }
}

struct CustomerDetails:Decodable{
    
    let id:String?
    let CustomerName:String?
    let Group:String?
    let Country:String?
    let Address:String?
    let Active:Bool?

    let HeartrateId:String?
    let BloodGlucoseId:String?
    let BloodPressureId:String?
    let SPO2Id:String?
    let TemperatureId:String?
    let WeightId:String?
    
    let HeartRateStart:Int?
    let HeartRateEnd:Int?
    let BloodGlucoseStart:Int?
    let BloodGlucoseEnd:Int?
    let BP_SystolicStart:Int?
    let BP_SystolicEnd:Int?
    let weightStart:Int?
    let weightEnd:Int?
    let BP_DistolicStart:Int?
    let BP_DistolicEnd:Int?
    let SPO2Start:Int?
    let SPO2End:Int?
    let DiabetesTemplate:Bool?
    let CreatedDate:Date?
    let CreatedBy:String?
    let LastAction:Date?
    let LastActionBy:String?

    
    private enum CodingKeys : String, CodingKey{

        case id = "id"
        case CustomerName = "CustomerName"
        case Group = "Group"
        case HeartrateId = "HeartrateId"
        case BloodGlucoseId = "BloodGlucoseId"
        case BloodPressureId = "BloodPressureId"
        case SPO2Id = "SPO2Id"
        case TemperatureId = "TemperatureId"
        case WeightId = "WeightId"
        
        case HeartRateStart = "HeartRateStart"
        case HeartRateEnd = "HeartRateEnd"
        case BloodGlucoseStart = "BloodGlucoseStart"
        case BloodGlucoseEnd = "BloodGlucoseEnd"
        case BP_SystolicStart = "BP_SystolicStart"
        case BP_SystolicEnd = "BP_SystolicEnd"
        case weightStart = "weightStart"
        case weightEnd = "weightEnd"
        case BP_DistolicStart = "BP_DistolicStart"
        case BP_DistolicEnd = "BP_DistolicEnd"
        case SPO2Start = "SPO2Start"
        case SPO2End = "SPO2End"
        case DiabetesTemplate = "DiabetesTemplate"
        case CreatedDate = "CreateDate"
        case CreatedBy = "CreatedBy"
        case LastAction = "LastAction"
        case LastActionBy = "LastActionBy"
        case Active = "Active"
        case Address = "Address"
        case Country = "Country"
        
    }
}
