//
//  RPMDataManager.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 09/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMDataManager: NSObject {

    
    static var sharedInstance = RPMDataManager()
    
    @objc public class func settings() -> RPMDataManager {
        return sharedInstance
    }
    
    func getConsentsArray() -> Array<RPMConsentModel>? {
        return RPMConsentDataBase.configuredConsentDetails()
    }
    
    
}
