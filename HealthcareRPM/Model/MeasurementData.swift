//
//  MeasurementData.swift
//  HealthcareRPM
//
//  Created by Isham on 06/02/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct MeasurementData:Codable {
    
    var patientId: String?
    var measurementTypeId : String?
    var measurementName : String?
    var measurementUnit : String?
    var value: String?
    var measurementDateTime : String?
    var isManual: Bool?
    var isUploadedToServer : Bool?
    var measurementValueType : String?
    var linkedId : String?
    var details: String! = ""
}
