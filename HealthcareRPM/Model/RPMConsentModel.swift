//
//  RPMConsentModel.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 09/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMConsentModel: NSObject {
    
    var consentDescription : String?
    var isSwichedOn : Bool?
    var consentType : String?

    
    init(descriptionText :String, isOn :Bool, type :String){
        consentDescription = descriptionText
        isSwichedOn = isOn
        consentType = type

    }
    

}
