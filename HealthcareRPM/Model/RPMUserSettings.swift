//
//  RPMUserSettings.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 21/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMUserSettings: NSObject {

    var patientId : String?
    var patientIdNumber = ""
    var username = ""
    var password = ""
    var firstname = ""
    var lastname = ""
    var patientCondition : [RPMPatientCondition] = []
    static var sharedInstance = RPMUserSettings()
    
}
