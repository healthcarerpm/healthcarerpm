//
//  RPMConsentSelectionTableViewCell.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 10/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMConsentSelectionTableViewCell: UITableViewCell {

    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var checkBox : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateConsentCellWith(consentDescription :String, switchState: Bool){
        lblMessage.text = consentDescription
    }
    
    

}
