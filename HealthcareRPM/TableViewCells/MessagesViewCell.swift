//
//  MessagesViewCell.swift
//  HealthcareRPM
//
//  Created by Barath Vijayaraghavan on 9/30/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

//
//  ReferencesCell.swift
//  healthpointe
//
//  Created by Barath Vijayaraghavan on 6/26/19.
//  Copyright © 2019 RBEI. All rights reserved.
//

import UIKit

class MessagesCell: UITableViewCell {
    
    var label1:UILabel?
    var label2:UILabel?

    let brighterBlue = UIColor.init(red: 0.000, green: 0.467, blue: 0.694, alpha: 1.0)
    let ozarkNoir = UIColor.init(red: 0.016, green: 0.118, blue: 0.259, alpha: 1.0)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        self.initialize()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize()
    {
        label1 = UILabel.init(frame: CGRect.init(x: 20, y: 40, width:  UIScreen.main.bounds.size.width - 300, height: 60))
        label1?.font = UIFont.init(name: "Helvetica", size: 24)
        label1?.backgroundColor = UIColor.white
        label1?.layer.cornerRadius = 20
        label1?.textColor = brighterBlue
        label1?.lineBreakMode = .byWordWrapping
        self.addSubview(label1!)
        
        label2 = UILabel.init(frame: CGRect.init(x:  UIScreen.main.bounds.size.width - 300, y: 50, width: UIScreen.main.bounds.size.width - 300, height: 40))
        label2?.font =  UIFont.init(name: "Helvetica", size: 20)
      //  label2?.backgroundColor = UIColor.white
        label2?.textColor = brighterBlue//Constants().BRIGHTER_BLUE
        label2?.layer.cornerRadius = 20
        self.addSubview(label2!)
        
      
    
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
