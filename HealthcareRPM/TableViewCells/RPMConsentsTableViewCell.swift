//
//  RPMConsentsTableViewCell.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 09/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMConsentsTableViewCell: UITableViewCell {

    @IBOutlet var consentSwitch : UISwitch?
    
    @IBOutlet weak var lblDescription: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateConsentCellWith(consentDescription :String, switchState: Bool){
        lblDescription!.text = consentDescription
        consentSwitch?.isOn = switchState
        consentSwitch?.onTintColor = UIColor.init(red: 65/225, green: 105/225, blue: 225/225, alpha: 1.0)
        
    }

    
}
