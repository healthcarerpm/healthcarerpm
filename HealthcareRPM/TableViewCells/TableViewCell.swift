import UIKit

//protocol InsuranceCellViewDelegate {
//    func didTapExplanationBtn()
//}

class Measurementcell: UITableViewCell {
    
    var label: UILabel?
    var label1:UILabel?
    var label2:UILabel?
    var label3:UILabel?
    var label4:UILabel?
    var explanationBtn : UIButton?
    
    let brighterBlue = UIColor.init(red: 0.000, green: 0.467, blue: 0.694, alpha: 1.0)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style:style, reuseIdentifier:reuseIdentifier)
        self.initialize()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize()
    {
        label = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: 150, height: 50))
        label?.font = UIFont.init(name: "Bogle-Bold", size: 15)!
        label?.numberOfLines = 0
        label?.lineBreakMode = .byWordWrapping
        self.addSubview(label!)
        
        label1 = UILabel.init(frame: CGRect.init(x: 100, y: 0, width: 100, height: 50))
        label1?.font = UIFont.init(name: "Bogle-Bold", size: 15)!

        label1?.numberOfLines = 0
      label1?.lineBreakMode = .byWordWrapping
        label1?.backgroundColor = UIColor.white
        label1?.layer.cornerRadius = 20
        self.addSubview(label1!)
        
        label2 = UILabel.init(frame: CGRect.init(x: 170, y: 0, width: 150, height: 50))
        label2?.backgroundColor = UIColor.white
        label2?.layer.cornerRadius = 20
        label2?.numberOfLines = 0
        label2?.lineBreakMode = .byWordWrapping
        label2?.adjustsFontSizeToFitWidth = true
        label2?.minimumScaleFactor = 0.5
        label2?.font = UIFont.init(name: "Bogle-Bold", size: 15)!

        self.addSubview(label2!)
        
        label3 = UILabel.init(frame: CGRect.init(x: 247, y: 0, width: 150, height: 50))
//        label3?.font = UIFont.init(name: "Helvetica", size: 18)
        label3?.numberOfLines = 0
               label3?.lineBreakMode = .byWordWrapping
               label3?.adjustsFontSizeToFitWidth = true
               label3?.minimumScaleFactor = 0.5
        label3?.backgroundColor = UIColor.white
        label3?.font = UIFont.init(name: "Bogle-Bold", size: 15)!

        label3?.layer.cornerRadius = 20
        self.addSubview(label3!)
        
        label4 = UILabel.init(frame: CGRect.init(x: 325, y: 0, width: 150, height: 50))
        label4?.font = UIFont.init(name: "Bogle-Bold", size: 15)!

        label4?.backgroundColor = UIColor.white
        label4?.numberOfLines = 0
               label4?.lineBreakMode = .byWordWrapping
               label4?.adjustsFontSizeToFitWidth = true
               label4?.minimumScaleFactor = 0.5
        label4?.layer.cornerRadius = 20
        self.addSubview(label4!)
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
