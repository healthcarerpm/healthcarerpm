//
//  TermsConditionsViewController.swift
//  HealthcareRPM
//
//  Created by Adithya jp on 25/08/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit
import WebKit

class TermsConditionsViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var webviewrpm: WKWebView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var backButton : UIButton!
    
    @IBOutlet weak var backButon: UIBarButtonItem!
    
    @IBOutlet weak var nextBtn : UIBarButtonItem!
    
    var btn : UIButton?
    
    @IBOutlet weak var labelContentView: UIView!
    //   var label1:UILabel?
    
    @IBOutlet weak var acceptBtn: UIButton!
    
      let PDFURLLINK = URL(string:"https://hrpmportalbackupstore.blob.core.windows.net/documents/Healthcare%20RPM%20terms%20and%20conditions.pdf?sp=r&st=2020-04-19T05:42:43Z&se=2022-08-30T13:42:43Z&spr=https&sv=2019-02-02&sr=b&sig=sKLK55DOd%2BgA%2FjzihrdUIYgyixgriLX1B8tIxxzIAIs%3D")!
    
    var fromDashboard = false
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.acceptBtn.setImage(UIImage(named: "BlueCheckedBox"), for: UIControl.State.normal)
//                self.doneBtn.isHidden = false
            }
            else {
                self.acceptBtn.setImage(UIImage(named: "unchecked"), for: UIControl.State.normal)
            }
        }
    }
    

    @IBAction func dismiss(_ sender: UIButton) {
        NetworkManager().postconsentforpatient(patientId: RPMUserSettings.sharedInstance.patientId ?? "", completion: {
            _ in
            //directDashboard2
            if MeasurementIdentifier.sharedInteractor.customerId == "4e11d2bc-e5ea-499e-836e-cfa403e88ce7"{
                self.performSegue(withIdentifier: "splashscreen", sender: nil)
            }else{
                self.performSegue(withIdentifier: "directDashboard2", sender: nil)
            }
        })
    }
    
    /**
        OnClick of Back button  => Logout of the app.
     */
    
    @IBAction func showLogin(_sender : UIBarButtonItem){
        
        if MeasurementIdentifier.sharedInteractor.isfromdashboardforTerms == true{
            self.dismiss(animated: true, completion: nil)
        }
        else{
        
                let alert = UIAlertController(title: "Terms & Conditions", message: "Are you sure you dont't want to accept the Terms & Conditions?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes",
                                      style:.default,
                                      
                                      handler: {(alert: UIAlertAction!) in
                                        UserDefaults.standard.set("no", forKey: "LoggedIn")
                                        
                                        UserDefaults.standard.synchronize()
                                        
                                        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                        
                                        let loginVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                                        
                                        self.present(loginVC, animated: true, completion: nil)
                                        
        }))
        
        alert.addAction(UIAlertAction(title: "No",
                                      
                                      style:.cancel,
                                      
                                      handler: nil))

        
        self.present(alert, animated: true, completion: nil)
        
        }
    }
    
    func buttonClicked(sender: UIButton) {
           // isChecked = !isChecked
        let pId = RPMUserSettings.sharedInstance.patientId ?? ""
        UserDefaults.standard.set(pId, forKey: pId)
        UserDefaults.standard.synchronize()
        self.doneBtn.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.4117647059, blue: 0.8823529412, alpha: 1)
        self.doneBtn.isEnabled = true
            print("click")
        
    }
    
   
    @IBAction func aceeptAction(_ sender: Any) {
        self.isChecked = !self.isChecked
        if self.isChecked{
            self.buttonClicked(sender: sender as! UIButton)
        }
        else{
            self.doneBtn.backgroundColor = #colorLiteral(red: 0.7255717487, green: 0.793196157, blue: 1, alpha: 1)
            self.doneBtn.isEnabled = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
      //  self.navigationController?.navigationBar.barTintColor = UIColor(red: 44.0/255.0, green: 171.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
    }
    @IBAction func LaunchDashboard(_ sender: Any) {
        NetworkManager().postconsentforpatient(patientId: RPMUserSettings.sharedInstance.patientId ?? "", completion: {
            _ in
            //directDashboard2
//            if MeasurementIdentifier.sharedInteractor.customerId == "4e11d2bc-e5ea-499e-836e-cfa403e88ce7"{
                self.performSegue(withIdentifier: "splashscreen", sender: nil)
//            }
//            else{
//                self.performSegue(withIdentifier: "directDashboard2", sender: nil)
//            }
        })
        
    }
    
    @IBAction func NavigateBacktoLogin(_ sender: Any) {
        print("Thaats")
    }
    
    
    @IBOutlet weak var acceptlabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        textView.isEditable = false
        let str = NSMutableAttributedString()
        if MeasurementIdentifier.sharedInteractor.isfromdashboardforTerms == true{
            self.backButon.title = "Done"
            self.nextBtn.title = ""
        }
        //change 1 march 2020 : to enable by default
  //      self.doneBtn.isEnabled = true
  //      self.doneBtn.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.4117647059, blue: 0.8823529412, alpha: 1)
        //self.doneBtn.isEnabled = false
        //self.doneBtn.backgroundColor = #colorLiteral(red: 0.7255717487, green: 0.793196157, blue: 1, alpha: 1)
        
        str.append(changeFontSize(myAttrString: "PLEASE READ THESE TERMS OF SERVICE CAREFULLY"))
        str.append(changeFont(myAttrString: terms))
       // str.append(Terms)
        str.append(changeFontSize(myAttrString: ServiceProvided))
        str.append(changeFont(myAttrString: services))
        //str.append(Services)
        str.append(changeFontSize(myAttrString: RightToUse))
        str.append(changeFont(myAttrString: rights))
       // str.append(Rights)
        str.append(changeFontSize(myAttrString: RightToMaterial))
        str.append(changeFont(myAttrString: rightsToMaterial))
        //str.append(RightsToMaterial)
        str.append(changeFontSize(myAttrString: limitation))
        str.append(changeFont(myAttrString: LimitationsToUse))
      //  str.append(Limit)
        str.append(changeFontSize(myAttrString: site))
        str.append(changeFont(myAttrString: SiteRegProcess))
     //   str.append(SiteForreg)
        str.append(changeFontSize(myAttrString: user))
        str.append(changeFont(myAttrString: UserName))
      //  str.append(User)
        str.append(changeFontSize(myAttrString: disclaimers))
        str.append(changeFont(myAttrString: Disclaimers))
       // str.append(DIsclaimers)
        str.append(changeFontSize(myAttrString: limitationLiability))
        str.append(changeFont(myAttrString: liability))
        
//        textView.attributedText = str
        let acceptanceString = NSMutableAttributedString()
        acceptanceString.append(changeFontSize(myAttrString: "I accept the terms and conditions"))
//        self.acceptlabel.attributedText = acceptanceString

//        if let pdf = Bundle.main.url(forResource: "RPM terms and conditions", withExtension: "pdf", subdirectory: nil, localization: nil)
//        {
            let req = NSURLRequest(url: PDFURLLINK)
            webviewrpm.load(req as URLRequest)
 //       }
        
//        if   MeasurementIdentifier.sharedInteractor.isfromdashboardforTerms {
//            labelContentView.isHidden = true
//            acceptBtn.isHidden  = true
//
//        }
//        else{
//
//            acceptBtn.isHidden = false
//        }
    }
    
    func changeFontSize(myAttrString:String) -> NSAttributedString {
        var myStr : String
        myStr = myAttrString
        let myAttribute = [NSAttributedString.Key.font: UIFont(name:"Bogle-Bold", size: 22)!]
        let AttrString = NSAttributedString(string: myStr, attributes: myAttribute)
        return AttrString
    }
    
    
    func changeFont(myAttrString:String) -> NSAttributedString {
        var myStr : String
        myStr = myAttrString
        let myAttribute = [NSAttributedString.Key.font: UIFont(name:"Bogle-Regular", size: 18)!]
        let AttrString = NSAttributedString(string: myStr, attributes: myAttribute)
        return AttrString
    }
    

    let terms = "\n\n" + "These terms of service (“Terms of Service”) apply to and govern your access to and use of any website (“Site”) owned or operated by Integrum Care, Inc. (“Integrum Care”, “we” or “us”). By using the “Site”, you agree to be bound by these Terms of Service and to use the Site in accordance with these Terms of Service, our Privacy Policy, and any additional terms and conditions that are referenced herein or that otherwise may apply to specific sections of the Site, or to products and services that we make available to you through the Site (all of which are deemed part of these Terms of Service). Accessing the Site in any manner, whether automated or otherwise, constitutes use of the Site and your agreement to be bound by these Terms of Service." + "\n\n" + "We reserve the right to change these Terms of Service or to impose new conditions on use of the Site, from time to time, in which case we will post the revised Terms of Service on the Site and update the “Last Updated” date to reflect the date of the changes. By continuing to use the Site after we post any such changes, you accept the Terms of Service, as modified. We also reserve the right to deny access to the Site or any features of the Site to anyone who violates these Terms of Service or who, in our sole judgment, interferes with the ability of others to enjoy our Site or infringes on the rights of others."
    
    let ServiceProvided = "\n\n" + "1. Services Provided"
    
    let services = "\n\n" + "Integrum Care provides internet healthcare resources to connect individuals with participating physicians, licensed therapists and other licensed health care practitioners (the “Providers”) in real time, via live video conferencing, telephone and/or secure messaging for the diagnosis and treatment of patients over the Internet, as well as providing other types of administrative services and information (“Services”). All of the participating Providers are independent contractors. Providers may record video conference consultations and Integrum Care may record telephone calls for quality purposes. Integrum Care itself does not provide any medical or Provider services."
    
    let RightToUse = "\n\n" + "2. Your Limited Right to Use Site Materials"
    
    let rights = "\n\n" + "The Site and all the materials available on the Site are the property of Integrum Care and/or our affiliates or licensors, and are protected by copyright, trademark, and other intellectual property laws. The Site is provided solely for your personal noncommercial use. You may not use the Site or the materials available on the Site in a manner that constitutes an infringement of our rights or that has not been authorized by us. More specifically, unless explicitly authorized in these Terms of Service or by the owner of the materials, you may not modify, copy, reproduce, republish, upload, post, transmit, translate, sell, create derivative works, exploit, or distribute in any manner or medium (including by email or other electronic means) any material from the Site. You may, however, from time to time, download and/or print one copy of individual pages of the Site for your personal, non-commercial use, provided that you keep intact all copyright and other proprietary notices. Information about requesting permission to reproduce or distribute materials from the Site can be obtained by contacting us as follows:"
    
    let RightToMaterial = "\n\n" + "3. Our Right to Use Materials You Submit or Post"
    
    let rightsToMaterial = "\n\n" + "When you submit or post any material (including any photos or videos) via the Site, you grant us, and anyone authorized by us, a royalty-free, perpetual, irrevocable, non-exclusive, unrestricted, worldwide license to use, copy, modify, transmit, sell, exploit, create derivative works from, distribute, and/or publicly perform or display such material, in whole or in part, in any manner or medium (whether now known or hereafter developed), for any purpose that we choose. The foregoing grant includes the right to exploit any proprietary rights in such posting or submission, including, but not limited to, rights under copyright, trademark or patent laws that exist in any relevant jurisdiction. Also, in connection with the exercise of these rights, you grant us, and anyone authorized by us, the right to identify you as the author of any of your postings or submissions by name, e-mail address or screen name, as we deem appropriate. You understand that the technical processing and transmission of the Site, including content submitted by you, may involve transmissions over various networks, and may involve changes to the content to conform and adapt it to technical requirements of connecting networks or devices. You will not receive any compensation of any kind for the use of any materials submitted by you."
    
    let limitation = "\n\n" + "4. Limitations on Linking and Framing"
    
    let LimitationsToUse = "\n\n" + "You are free to establish a hypertext link to our Site so long as the link does not state or imply any sponsorship of your website or service by Integrum Care, or Integrum Care. However, you may not, without our prior written permission, frame or inline link any of the content of our Site, or incorporate into another website or other service any of our material, content or intellectual property."
    
    let site = "\n\n" + "5. Site Registration Process"
    
    let SiteRegProcess = "\n\n" + "In order to access certain features of our Site, we may ask you to provide certain demographic information including your gender, year of birth, address, and payment information. In addition, if you elect to sign up for particular features of the Site, such as discussion forums, blogs, photo- and video-sharing pages or social networking features, you may also be asked to register with us on a form provided for registration purposes, and we may require you to provide personally identifiable information such as your name and e-mail address. You agree to provide true, accurate, current and complete information about yourself as prompted by the Site’s registration form. If we have reasonable grounds to suspect that such information is untrue, inaccurate, or incomplete, we have the right to suspend or terminate your account and refuse any and all current or future use of the Site (or any portion thereof). Our use of any information you provide to us as part of the registration process is governed by the terms of our Privacy Policy."
    
    let user = "\n\n" + "6. Responsibility for Your Username and Password"
    
    let UserName = "\n\n" + "In order to use certain features of our Site, you may need a username and password, which you will receive and/or create through the Site’s registration process. We reserve the right to reject or terminate the use of any username that we deem in our sole judgment offensive or inappropriate. In addition, we also reserve the right to terminate the use of any username or account, or to deny access to the Site or any features of the Site, to anyone who violates these Terms of Service or who, in our sole judgment, interferes with the ability of others to enjoy our website or infringes the rights of others. You are responsible for maintaining the confidentiality of your password and account, and you are responsible for all activities (whether by you or by others) that occur under your password or account. You agree to notify us immediately of any unauthorized use of your password or account or any other breach of security, and to ensure that you exit from your account at the end of each session. We cannot and will not be liable for any loss or damage arising from your failure to protect your password or account information."
    
    let disclaimers = "\n\n" + "7. Disclaimers"
    
    let Disclaimers = "\n\n" + "Throughout our Site, we may have provided links and pointers to Internet sites maintained by third parties. Our linking to any such third-party sites does not imply an endorsement or sponsorship of such sites, or the information, products or services offered on or through the sites. In addition, neither we nor our parent or subsidiary companies nor any of our respective affiliates operate or control in any respect any information, products or services that such third parties may provide on or through the Site or on websites linked to by us on the Site." + "\n\n" + "THE INFORMATION, PRODUCTS AND SERVICES OFFERED ON OR THROUGH THE SITE AND ANY THIRD-PARTY SITES ARE PROVIDED “AS IS” AND WITHOUT WARRANTIES OF ANY KIND EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. WE DO NOT WARRANT THAT THE SITE OR ANY OF ITS FUNCTIONS WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT ANY PART OF THE SITE, INCLUDING BULLETIN BOARDS, OR THE SERVERS THAT MAKE IT AVAILABLE, ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS." + "\n\n" + "WE DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE SITE OR MATERIALS ON THE SITE OR ON THIRD-PARTY SITES IN TERMS OF THEIR CORRECTNESS, ACCURACY, TIMELINESS, RELIABILITY OR OTHERWISE." + "\n\n" + "You must provide and are solely responsible for all hardware and/or software necessary to access the Site. You assume the entire cost of and responsibility for any damage to, and all necessary maintenance, repair or correction of, that hardware and/or software." + "\n\n" + "The Site is provided for informational purposes only, and is not intended for trading or investing purposes or for commercial use. Stock and mutual fund quotes, and related financial news stories may be delayed at least 20 minutes, as may be required by the stock exchanges and/or the financial information services. The Site should not be used in any high risk activities where damage or injury to persons, property, environment, finances or business may result if an error occurs. You expressly assume all risk for such use." + "\n\n" + "Your interactions with companies, organizations and/or individuals found on or through our Site, including any purchases, transactions, or other dealings, and any terms, conditions, warranties or representations associated with such dealings, are solely between you and such companies, organizations and/or individuals. You agree that we will not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings. You also agree that, if there is a dispute between users of the Site, or between a user and any third party, we are under no obligation to become involved, and you agree to release us and our affiliates from any claims, demands and damages of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way related to such dispute and/or our Site."
    
    let limitationLiability = "\n\n" + "8. Limitation of Liability"
    
    let liability = "\n\n" + "UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, WILL WE OR OUR SUBSIDIARIES, PARENT COMPANIES OR AFFILIATES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE USE OF, OR THE INABILITY TO USE, THE SITE, INCLUDING ITS MATERIALS, PRODUCTS, OR SERVICES, OR THIRD-PARTY MATERIALS, PRODUCTS, OR SERVICES MADE AVAILABLE THROUGH THE SITE, EVEN IF WE ARE ADVISED BEFOREHAND OF THE POSSIBILITY OF SUCH DAMAGES. (BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF CERTAIN CATEGORIES OF DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IN SUCH STATES, OUR LIABILITY AND THE LIABILITY OF OUR SUBSIDIARIES, PARENT COMPANIES AND AFFILIATES, IS LIMITED TO THE FULLEST EXTENT PERMITTED BY SUCH STATE LAW.) YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT WE ARE NOT LIABLE FOR ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF ANY USER. IF YOU ARE DISSATISFIED WITH THE SITE, OR ANY MATERIALS, PRODUCTS, OR SERVICES ON THE SITE, OR WITH ANY OF THE SITE’S TERMS AND CONDITIONS, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE SITE."
    
    
    //let accept = "\n\n" +
 
}
