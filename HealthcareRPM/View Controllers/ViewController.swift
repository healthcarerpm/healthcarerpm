//
//  ViewController.swift
//  HealthcareRPM
//
//  Created by Isham on 27/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onLoginButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "login", sender: nil)
    }
    
}

