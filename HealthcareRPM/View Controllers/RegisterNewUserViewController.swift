//
//  RegisterNewUserViewController.swift
//  HealthcareRPM
//
//  Created by Admin on 1/7/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown

class RegisterNewUserViewController: UIViewController, UITextFieldDelegate, PopoverProtocol {
    

    @IBOutlet weak var registerBtn: UIButton!
    
    @IBOutlet weak var firstName: UITextField!{
        didSet {
            firstName.tintColor = UIColor.lightGray
            firstName.setIcon(#imageLiteral(resourceName: "fname"))
        }
    }
    
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var gender: UITextField!{
        didSet{
            self.gender.tintColor = .lightGray
            self.gender.setIcon(#imageLiteral(resourceName: "gender"))
        }
    }
    
    @IBOutlet weak var email: UITextField!{
        didSet{
            self.email.tintColor = .lightGray
            self.email.setIcon(#imageLiteral(resourceName: "email"))
        }
    }

    @IBOutlet weak var mobileNumber: UITextField!
    
    @IBOutlet weak var age: UITextField!{
        didSet{
            self.age.tintColor = .lightGray
            self.age.setIcon(#imageLiteral(resourceName: "age"))
        }
    }
    
    @IBOutlet weak var whoRecommended: UITextField!{
        didSet{
            self.whoRecommended.tintColor = .lightGray
            self.whoRecommended.setIcon(#imageLiteral(resourceName: "recomended"))
        }
    }
    
    @IBOutlet weak var recommendedId: UITextField!{
        didSet{
            self.recommendedId.tintColor = .lightGray
            self.recommendedId.setIcon(#imageLiteral(resourceName: "recomended"))
        }
    }
    
    @IBOutlet weak var country: DropDown!{
        didSet{
            self.country.tintColor = .lightGray
            self.country.setIcon(#imageLiteral(resourceName: "country"))
            self.country.optionArray = ["India","USA", "UK", "Canada", "Mexico"]
        }
    }
    
    var textFields = [UITextField]()
    
    
    
    @IBOutlet weak var setFirstPwd: UITextField!
    
    @IBOutlet weak var setSecondPwd: UITextField!
    
    @IBOutlet weak var setThirdPwd: UITextField!
    
    @IBOutlet weak var setFourthPwd: UITextField!
    
    @IBOutlet weak var confirmFirstPwd: UITextField!
    
    @IBOutlet weak var confirmSecPwd: UITextField!
    
    
    @IBOutlet weak var confirmFourthPwd: UITextField!
    @IBOutlet weak var confirmThirdPwd: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        print("RegisterVC")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        
        backgroundImage.image = #imageLiteral(resourceName: "healthSessionBgImg")
        
        self.view.insertSubview(backgroundImage, at: 0)
        self.registerBtn.layer.cornerRadius = 20
        self.registerBtn.layer.masksToBounds = true
        self.registerBtn.clipsToBounds = true
        
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.email.delegate = self
        self.country.delegate = self
        self.mobileNumber.delegate = self
//        self.recommendedId.delegate = self
//        self.whoRecommended.delegate = self
        self.age.delegate = self
        self.gender.delegate = self
        
        
        self.setFirstPwd.delegate = self
        self.setSecondPwd.delegate = self
        self.setThirdPwd.delegate = self
        self.setFourthPwd.delegate = self
        self.confirmFirstPwd.delegate = self
        self.confirmSecPwd.delegate = self
        self.confirmThirdPwd.delegate = self
        self.confirmFourthPwd.delegate = self
        
        self.setFirstPwd.addBottomBorder()
        self.setSecondPwd.addBottomBorder()
        self.setThirdPwd.addBottomBorder()
        self.setFourthPwd.addBottomBorder()
        self.confirmFirstPwd.addBottomBorder()
        self.confirmSecPwd.addBottomBorder()
        self.confirmThirdPwd.addBottomBorder()
        self.confirmFourthPwd.addBottomBorder()


    }
    
    @IBAction func register(_ sender: UIButton) {
        
        //self.navigateToAilmentVC()
        
       // self.setRPMParameter()
        if self.validateInput() == true{
            self.showSpinner(onView: self.view)
                             NetworkManager().loginForToken(username: "defaultpatient", password:"2345") { (response) -> () in
                                self.removeSpinner(completion:{
                                    if response != nil {
                                        
                                        self.setRPMParameter()
                                        
                                    }
                                })
                                
                                 
                             }
        }
       
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AilmentSelectionSegue"
        {
            let destinationVC = segue.destination as! AilmentSelectionViewController
        }
    }
    
    func navigateToAilmentVC()
    {
        
        self.performSegue(withIdentifier: "AilmentSelectionSegue", sender: self)

    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    
    @IBAction func femaleSelected(_ sender: UIButton) {
        
        self.gender.text = "Female"
        
    }
    
    @IBAction func maleSelected(_ sender: UIButton) {
        
        self.gender.text = "Male"

    }
    
    
    let BASE_URL = "https://brainmeshintegrumapi.azurewebsites.net/"
    
    @IBAction func attemptLogin(_ sender: Any) {
          
          
    }
    

    func validateInput()->Bool{
        if self.firstName.text == "" || self.lastName.text == "" || self.email.text == "" || self.gender.text == "" || self.country.text == "" || self.age.text == "" || self.setFirstPwd.text == "" || self.setSecondPwd.text == "" || self.setThirdPwd.text == "" || self.setFourthPwd.text == "" || self.confirmFirstPwd.text == "" || self.confirmSecPwd.text == "" || self.confirmThirdPwd.text == "" || self.confirmFourthPwd.text == ""{
              self.showSimpleAlert(titleText:"warning",details: "please enter all requied fields")
            return false
        }
       if let email = self.email.text
        {
            if(self.validateEmail(enteredEmail: email)) != true {
                self.showSimpleAlert(titleText:"warning",details: "Invalid Email Id")
                return false
            }
           
        }
        if (self.setFirstPwd.text != self.confirmFirstPwd.text) || (self.setSecondPwd.text != self.confirmSecPwd.text) || (self.setThirdPwd.text != self.confirmThirdPwd.text) || (self.setFourthPwd.text != self.confirmFourthPwd.text)
        {
            self.showSimpleAlert(titleText:"Warning",details: "Passwords are not matching")
            return false
        }
         return true
    }
    
    
    func setRPMParameter(){

        var fullName = ""
        var emailId = ""
        
        if let firstName = self.firstName.text
        {
            fullName = firstName + " " + self.lastName.text!
        }
        if let email = self.email.text
        {
          
                emailId = email
           
        }
 
        var mobileNo = ""

        if let contact = self.mobileNumber.text
        {
            if contact.contains("+")
                
            {
                let splitContact = contact.split(separator: "+")
                mobileNo = String(splitContact[1])
                
            }else{
                mobileNo = contact
            }

        }


        let pwd = setFirstPwd.text! + setSecondPwd.text! + setThirdPwd.text! + setFourthPwd.text!
        
        let rpmParameters: [String: Any] = [
            
            "CustomerId": "09349ed1-a358-456b-a6fb-3257df3d7ffb", "CreatedBy": "9bcf0057-1d5a-4242-abde-e00e5815606e", "IsActivated": true, "PatientStatusId": "Active", "LastAction": "2020-01-15T03:17:28.906Z", "EDD": "2020-10-20", "TimeZoneId": "47de0848-07a9-4e90-a3bd-7a30de168c35", "PatientExternalId": "668", "Telephone": mobileNumber.text!, "LastName": lastName.text!, "CreateDateString": "2020-01-15T13:24:16.055Z", "Address": ["City": "Prospect", "Address1": "13011 West HWY US 42", "Address2": "13011 West HWY US 42", "State": "Suite L05", "ZipCode": "KY 40059", "Country": "India"], "CreateDate": "2020-01-14T13:24:16.055Z", "FullName": fullName, "FirstName": firstName.text! , "LastActionBy": "9bcf0057-1d5a-4242-abde-e00e5815606e", "LastActionString": "RPMB2C", "DateOfBirth": "2001-01-01", "CountryCode": "1", "Email": emailId, "ActivationPassword":pwd, "patientLogin":emailId
            
        ]
        
        
        RPMUserSettings.sharedInstance.firstname =  firstName.text!
        RPMUserSettings.sharedInstance.lastname = lastName.text!

        NetworkManager().register(requestData: rpmParameters){ (response) -> () in
        
            if let value = response as? [String: Any]{
                    
                    let details : [String:Any] = value["Result"] as! [String : Any]
                    guard let login : String = details["Login"] as? String else {
                        return
                    }
                    guard let  pass : String = details["Password"] as? String else{
                        return
                    }
                    RPMUserSettings.sharedInstance.patientIdNumber = details["PatientId"] as! String
                    RPMUserSettings.sharedInstance.username =  details["Login"] as! String
                    RPMUserSettings.sharedInstance.password =  details["Password"]  as! String
                    let message : String = "\n" + "User Name:" + login + "\n\n" + "Password:" + pass
                    UserDefaults.standard.setValue( RPMUserSettings.sharedInstance.username , forKey: "UserID")
                    self.showPopover(details: message)
                    
                }
            
        }
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == age{
            if range.location > 1{
                return false
            }
        }
        
        let allowedCharacters = CharacterSet(charactersIn:"+0123456789")
        let characterSet = CharacterSet(charactersIn: string)

        if textField == mobileNumber{
            if range.location > 9{
                return false
            }
            
            if string.rangeOfCharacter(from: allowedCharacters) != nil{

                return true
                
            }else{
                return allowedCharacters.isSuperset(of: characterSet)
            }
            
        }
        
//        let text = textField.text
        if  string.count == 1 {
            switch textField{
            case setFirstPwd:
                textField.text = string
                setFirstPwd.resignFirstResponder()
                setSecondPwd.becomeFirstResponder()
            case setSecondPwd:
                textField.text = string

                setSecondPwd.resignFirstResponder()

                setThirdPwd.becomeFirstResponder()
            case setThirdPwd:
                textField.text = string

                setThirdPwd.resignFirstResponder()

                setFourthPwd.becomeFirstResponder()
            case setFourthPwd:
                textField.text = string

                setFourthPwd.resignFirstResponder()

                setFourthPwd.resignFirstResponder()
            default:
                break
            }
            
            switch textField{
            case confirmFirstPwd:
                textField.text = string

                confirmSecPwd.becomeFirstResponder()
            case confirmSecPwd:
                textField.text = string

                confirmThirdPwd.becomeFirstResponder()
            case confirmThirdPwd:
                textField.text = string

                confirmFourthPwd.becomeFirstResponder()
            case confirmFourthPwd:
                textField.text = string

                confirmFourthPwd.resignFirstResponder()
            default:
                break
            }
            
            
        }
        if  string.count == 0 {
            switch textField{
            case setFirstPwd:
                setFirstPwd.becomeFirstResponder()
            case setSecondPwd:
                setFirstPwd.becomeFirstResponder()
            case setThirdPwd:
                setSecondPwd.becomeFirstResponder()
            case setFourthPwd:
                setThirdPwd.becomeFirstResponder()
            default:
                break
            }
            
            switch textField{
            case confirmFirstPwd:
                confirmFirstPwd.becomeFirstResponder()
            case confirmSecPwd:
                confirmFirstPwd.becomeFirstResponder()
            case confirmThirdPwd:
                confirmSecPwd.becomeFirstResponder()
            case confirmFourthPwd:
                confirmThirdPwd.becomeFirstResponder()
            default:
                break
            }
        }
        
        return true
    }
    
    
    
    @IBAction func goBackToLogin(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func showPopover(details:String)
    {
        let window: UIWindow? = UIApplication.shared.keyWindow
        let presentPopver = Bundle.main.loadNibNamed("PopoverView", owner: self, options: nil)?.first as? PopoverView
        presentPopver!.frame = (window?.frame)!
        presentPopver?.delegate = self
        presentPopver?.textView.text = details
        window?.addSubview(presentPopver!)
    }
    
    func onOKClick() {
        self.navigateToAilmentVC()

    }
    
    func showSimpleAlert(titleText:String,details:String) {
        
        let alert = UIAlertController(title:titleText, message: details,  preferredStyle: UIAlertController.Style.alert)


           alert.addAction(UIAlertAction(title: "Ok",
                                         style: UIAlertAction.Style.default,
                                         handler: {(_: UIAlertAction!) in
                                           //Sign out action
                                            if titleText == "Login Details"{
                                                self.navigateToAilmentVC()
                                            }

           }))
           self.present(alert, animated: true, completion: nil)
       }
    
}


extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 10, y: 5, width: 25, height: 25))
        iconView.image = image
        iconView.contentMode = .scaleAspectFit
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 40, height: 40))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
    
    func setRightIconOne(_ image1: UIImage, _ image2: UIImage, completion: @escaping (UIImageView, UIImageView, UIView) -> ())
    {
        let iconView = UIImageView(frame:
            CGRect(x: -70, y: 5, width: 40, height: 40))
        iconView.image = image1
        iconView.contentMode = .scaleAspectFit
        let iconView2 = UIImageView(frame:
            CGRect(x: -30, y: 5, width: 40, height: 40))
        iconView2.image = image2
        iconView2.contentMode = .scaleAspectFit
        
        
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: -80, y: 0, width: 50, height: 50))
        iconContainerView.addSubview(iconView)
        iconContainerView.addSubview(iconView2)

        rightView = iconContainerView
        rightViewMode = .always
        completion(iconView, iconView2, iconContainerView)
    }
}

