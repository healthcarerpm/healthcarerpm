//
//  GraphViewController.swift
//  HealthcareRPM
//
//  Created by Admin on 2/28/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit
import CoreData

class GraphViewController: UIViewController, LineChartDelegate,WeightScaleMeasurementCaptureProtocol,BluetoothReadingViewControllerDelegate,NoninInputViewControllerDelegate {
 
    func didSelectYDataPoint(_ y: CGFloat, xValues: [Any]) {
        
    }
    

    func noninInputController(_ controller: NoninInputViewController!, didCompleteWithMeasurement measurements: [AnyHashable : Any]!) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Pulse";
        
        let table = "HeartRate"
        request.predicate = NSPredicate(format: "name = %@", table)// measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        
        measurement.measurementDateTime = getCurrentDateTimeasString()
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId

        let pulse = measurements["pulse"] as! Int
        let spo2 = measurements["spo2"] as! Int
        measurement.value =  String(pulse) + "/" + String(spo2)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        
        measurement.value = String(pulse)
        
        NetworkManager().uploadDataPulse(measurementData: measurement ){ (response) -> () in
        }
        
        measurement.value = String(spo2)
        measurement.isManual = false
        measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.spo2ID
        
        NetworkManager().uploadDataSPO2(measurementData: measurement, completion: { (response) -> () in
        })
        
    }


    func weightScaleDidCaptureMeasurement(_ value:Double, dateCaptured date: Date) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Weight";
        request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeasString()
        let linkedId =  UUID().uuidString
        measurement.linkedId = linkedId

        let weight = value
        measurement.value =  String(weight)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
        }
        
    }
    var viewType : Int = 0
    
//        @IBOutlet weak var chartNameLabel: UILabel!

        @IBOutlet weak var chartView: LineChart!
        
        let list = ["Blood pressure","Blood glucose","Pulse","Temperature","Weight"]
        var measurementData = [MeasurementData]()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
            if(viewType == 2){
                self.title = "SPO2"  //Barath changed dashboard name. Not easy to replace since db is bound to it -> Isham
            }else{
                self.title = list[viewType];
            }
//            self.chartNameLabel.text = list[viewType].uppercased()
            self.initChat()
           // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 30), NSAttributedString.Key.foregroundColor: UIColor.white]
            UINavigationBar.appearance().isTranslucent = false
            self.navigationController?.navigationBar.backItem?.backBarButtonItem?.title = "Isham"
            self.navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        }
        
        override func viewDidAppear(_ animated: Bool) {
            measurementData = reloadData()
            self.reloadGraph()
            super.viewDidAppear(animated)
        }

        func SetBackBarButtonCustom()
        {
            //Back buttion
            let btnLeftMenu: UIButton = UIButton()
            btnLeftMenu.setImage(UIImage(named: "back_arrow"), for: UIControl.State())
            btnLeftMenu.titleLabel?.text = "<"
            btnLeftMenu.addTarget(self, action: #selector(DetailsViewController.onClcikBack), for: UIControl.Event.touchUpInside)
            btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 33/2, height: 27/2)
            let barButton = UIBarButtonItem(customView: btnLeftMenu)
            self.navigationItem.leftBarButtonItem = barButton
        }
        
        @objc func onClcikBack()
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        func initChat (){
            let data2: [CGFloat] = [0,0,0,0,0,0,]
            let xLabels: [String] = ["","","","","","","","","",""]
            
            
            chartView.animation.enabled = true
            chartView.area = true
            chartView.x.labels.visible = false
            chartView.y.labels.visible = false
            chartView.x.grid.count = 6
            chartView.y.grid.count = 6
            chartView.x.labels.values = xLabels
            chartView.addLine(data2)
            
            chartView.translatesAutoresizingMaskIntoConstraints = false
            chartView.delegate = self
        }
        
        /**
         * Line chart delegate method.
         */
        func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>) {
        }
        
        func getDateForGraph(date: String) -> String{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let graphDateFormatter = DateFormatter()
            graphDateFormatter.dateFormat = "MM-dd"
            if let date = dateFormatter.date(from: date)
            {
                return graphDateFormatter.string(from: date)
            }
            else{
                return ""
            }
        }

        
        /**
         * Redraw chart on device rotation.
         */
        override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
            if let chart = chartView {
                chart.setNeedsDisplay()
            }
        }
                
        
        func reloadData() ->[MeasurementData]{
            let coreDataUtils = CoreDataUtils()
            var tableName = ""
            if(self.list[viewType] == "Blood pressure"){
                tableName = "BloodPressure"
            }else if (self.list[viewType] == "Blood glucose"){
                tableName = "BloodGlucose"
            }else{
                tableName = self.list[viewType]
            }
            return coreDataUtils.getMeasurement(tablename: tableName)
        }
        
        func reloadGraph() {
            var xLabelData : [String] = []
            let measurementDataArray = measurementData.reversed()
            if viewType == 0 {
                var systolicData : [CGFloat] = []
                var diastolicData : [CGFloat] = []
                var heartrateData : [CGFloat] = []
                
                for measurement in measurementDataArray {
                    let data: [String] =  (measurement.value?.components(separatedBy: "/"))!
                    systolicData.append(CGFloat(NumberFormatter().number(from: data[0])!))
                    diastolicData.append(CGFloat(NumberFormatter().number(from: data[1])!))
                    if data.count > 2 && data[2] != nil && data[2] != ""
                    {
                    heartrateData.append(CGFloat(NumberFormatter().number(from: data[2])!))
                    }
                    xLabelData.append(getDateForGraph(date: measurement.measurementDateTime!))
                }
                
                if systolicData.count > 0 {
                    chartView.clearAll()
                    chartView.x.labels.visible = true
                    chartView.y.labels.visible = true
                    chartView.x.labels.values = xLabelData
                    chartView.addLine(systolicData)
                    chartView.addLine(diastolicData)
                    chartView.addLine(heartrateData)
                }
                
            }else if viewType == 2 {
                var pulseData : [CGFloat] = []
                var spo2Data : [CGFloat] = []
                
                for measurement in measurementDataArray {
                    let data: [String] =  (measurement.value?.components(separatedBy: "/"))!
                    pulseData.append(CGFloat(NumberFormatter().number(from: data[0])!))
                    if data.count > 1 && data[1] != ""{
                        spo2Data.append(CGFloat(NumberFormatter().number(from: data[1])!))
                    }
                    xLabelData.append(getDateForGraph(date: measurement.measurementDateTime!))
                }
                
                if pulseData.count > 0 {
                    chartView.clearAll()
                    chartView.x.labels.visible = true
                    chartView.y.labels.visible = true
                    chartView.x.labels.values = xLabelData
                    chartView.addLine(pulseData)
                    chartView.addLine(spo2Data)
                }
                
            }else{
                var chartData : [CGFloat] = []
                for measurement in measurementDataArray {
                    chartData.append(CGFloat((measurement.value as! NSString).doubleValue))
                    xLabelData.append(getDateForGraph(date: measurement.measurementDateTime!))
                }
                if chartData.count > 0 {
                    chartView.clearAll()
                    chartView.x.labels.visible = true
                    chartView.y.labels.visible = true
                    chartView.x.labels.values = xLabelData
                    chartView.addLine(chartData)
                }
            }
            
        }
        
         func bluetoothDevicedidcapturemeasurment(_ measurementType: String!, valuedict valuesdictionary: [AnyHashable : Any]) {
                var measurement: MeasurementData! = MeasurementData()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
                measurement.measurementName = "BloodPressure";
                request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
                request.returnsObjectsAsFaults = false
        
                measurement.isManual = false
        
                do {
                    let result = try context.fetch(request)
                    for data in result as! [NSManagedObject] {
                        measurement.measurementTypeId = data.value(forKey: "id") as? String
                        measurement.measurementUnit = data.value(forKey: "unit") as? String
                    }
                }catch {
                    print("Failed")
                }
                let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                request.returnsObjectsAsFaults = false
                do {
                    let result = try context.fetch(patientDetailsRequest)
                    for data in result as! [NSManagedObject] {
                        measurement.patientId = data.value(forKey: "id") as? String
                    }
                } catch {
                    print("Failed")
                }
                measurement.measurementDateTime = getCurrentDateTimeasString()
        
                let systolicVal = valuesdictionary["systolic"] as! Int
                let diaSystolicVal = valuesdictionary["diastolic"] as! Int
                let heartRateValue = valuesdictionary["pulse"] as! Int
                measurement.measurementValueType = "2/3"
                measurement.value =  String(systolicVal) + "/" + String(diaSystolicVal) + "/" + String(heartRateValue)
                let linkedId =  UUID().uuidString
                measurement.linkedId = linkedId

                CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
                NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
                }
                print(valuesdictionary)
        
            }
        
        func getCurrentDateTimeasString() -> String{
            let currentDateTime = Date()
            let dateTimeFormatter = DateFormatter()
            dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let str:String  = dateTimeFormatter.string(from: currentDateTime)
            print("DATE-TIME \(str)")
            
            return str
        }
        
        func getDateascontinuesStrings(datefromfunction:String) -> (String)
        {
            let dateFormatter = DateFormatter()
            var date:Date? = nil
            dateFormatter.locale = Locale.current
            // save locale temporarily
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" //Z has been removed since if we download user data again we are not receiving SSSZ and app crashes
            date = dateFormatter.date(from:datefromfunction)
            /*
             Temporaray fix to handle both mobile and server values
             */
            if date == nil{
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                //date = dateFormatter.date(from: datefromfunction)
                date = dateFormatter.date(from:datefromfunction)

            }

            let dateFormatterWithDay = DateFormatter()
            dateFormatterWithDay.dateFormat = "EEEE MMM d, yyyy HH:mm"
            let dateString = dateFormatterWithDay.string(from: date!)
            return dateString
        }
        
        func getDate(getdate:String) -> String
        {
            let dateFormatter = DateFormatter()
            var date:Date? = nil
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            date = dateFormatter.date(from:getdate)
            
            let dateFormaterwithday = DateFormatter()
            dateFormatter.dateStyle = .medium
            if date != nil{
                let dateToReturn = dateFormatter.string(from: date!)
                return dateToReturn
            }
            else
            {
                return ""
            }
        }
        
        func getTime(getTime:String) -> String
        {
            let dateFormatter = DateFormatter()
            var date:Date? = nil
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            date = dateFormatter.date(from:getTime)
            
            let dateFormaterwithday = DateFormatter()
            dateFormatter.timeStyle = .short
            if date != nil{
            let TimeToReturn = dateFormatter.string(from: date!)
            return TimeToReturn
            }else{
                return ""
            }
        }
        

        open var colors: [UIColor] = [
            UIColor(red: 0.121569, green: 0.466667, blue: 0.705882, alpha: 1),
            UIColor(red: 1, green: 0.498039, blue: 0.054902, alpha: 1),
            UIColor(red: 0.172549, green: 0.627451, blue: 0.172549, alpha: 1),
            UIColor(red: 0.839216, green: 0.152941, blue: 0.156863, alpha: 1),
            UIColor(red: 0.580392, green: 0.403922, blue: 0.741176, alpha: 1),
            UIColor(red: 0.54902, green: 0.337255, blue: 0.294118, alpha: 1),
            UIColor(red: 0.890196, green: 0.466667, blue: 0.760784, alpha: 1),
            UIColor(red: 0.498039, green: 0.498039, blue: 0.498039, alpha: 1),
            UIColor(red: 0.737255, green: 0.741176, blue: 0.133333, alpha: 1),
            UIColor(red: 0.0901961, green: 0.745098, blue: 0.811765, alpha: 1)
        ]
        
        func getAttributedStringForChart(mainString: String, strigToBeAttributed:String, color:UIColor) -> NSMutableAttributedString {
            let range = (mainString as NSString).range(of: strigToBeAttributed)
            let attributedString = NSMutableAttributedString(string:mainString)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
            return attributedString
        }

}
