//
//  EmployeeInstructionsViewController.swift
//  HealthcareRPM
//
//  Created by Himanshu on 5/12/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class EmployeeInstructionsViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var instructionsTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.companyLogoImageView.image = LogoResource.sharedInteractor.splashLogo
        
        let str = NSMutableAttributedString()
        
        str.append(changeFontSize(myAttrString: MeasurementIdentifier.sharedInteractor.InitialInstructionsHeading!))
        
        str.append(changeFont(myAttrString: "\n\n" + MeasurementIdentifier.sharedInteractor.InitialInstructions!))
//        // str.append(Terms)
//        str.append(changeFontSize(myAttrString: ServiceProvided))
//        str.append(changeFont(myAttrString: services))
//        //str.append(Services)
//        str.append(changeFont(myAttrString: RightToUse))
        
        instructionsTextView.attributedText = str
        // Do any additional setup after loading the view.
    }
    
    func changeFontSize(myAttrString:String) -> NSAttributedString {
        var myStr : String
        myStr = myAttrString
        let myAttribute = [NSAttributedString.Key.font: UIFont(name:"Bogle-Bold", size: 22)!]
        let AttrString = NSAttributedString(string: myStr, attributes: myAttribute)
        return AttrString
    }
    
    
    func changeFont(myAttrString:String) -> NSAttributedString {
        var myStr : String
        myStr = myAttrString
        let myAttribute = [NSAttributedString.Key.font: UIFont(name:"Bogle-Regular", size: 18)!]
        let AttrString = NSAttributedString(string: myStr, attributes: myAttribute)
        return AttrString
    }
    
    
    let terms = "\n\n" + "Your employer likes you to take 'Health Session' daily to ensure you are healthy and disease free during this pandemic."
    
    let ServiceProvided = "\n\n"
    
    let services = "Please answer it daily and if you have any questions then please contact your employer"
    
    let RightToUse = "\n\n" + "You will be able to see and track your measurements"
    
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
