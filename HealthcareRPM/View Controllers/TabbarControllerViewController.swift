//
//  TabbarControllerViewController.swift
//  HealthcareRPM
//
//  Created by Himanshu on 5/14/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class TabbarControllerViewController: UITabBarController {

    var isemployerEnabled = MeasurementIdentifier.sharedInteractor.isEmployer
    var isMessagingAllowed = MeasurementIdentifier.sharedInteractor.IsMessagingAllowed
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let firstVC = storyboard?.instantiateViewController(withIdentifier: "CovidDashboardViewController")
        firstVC!.tabBarItem = UITabBarItem.init(title: "", image: #imageLiteral(resourceName: "Home-1"), tag: 0)

        let secondVC = storyboard?.instantiateViewController(withIdentifier: "education")
        secondVC!.tabBarItem = UITabBarItem.init(title: "", image: #imageLiteral(resourceName: "education") ,  tag: 1)
        
        let thirdVC = storyboard?.instantiateViewController(withIdentifier: "messageDetail")
        thirdVC!.tabBarItem = UITabBarItem.init(title: "", image: #imageLiteral(resourceName: "message"), tag: 2)

        let fourthVC = storyboard?.instantiateViewController(withIdentifier: "DashboardViewController")
        fourthVC!.tabBarItem = UITabBarItem.init(title: "", image: #imageLiteral(resourceName: "Home-1"), tag: 2)

       var controllerArray = [firstVC, secondVC,thirdVC]
       var controllerArray2 = [fourthVC,secondVC,thirdVC]
       var controllerArray3 = [firstVC,secondVC]
        if isemployerEnabled == 1 && isMessagingAllowed == true{
            self.viewControllers = controllerArray as! [UIViewController]
        }else if isemployerEnabled == 1 && isMessagingAllowed == false
        {
              self.viewControllers = controllerArray3 as! [UIViewController]
        }
        else{
            self.viewControllers = controllerArray2 as! [UIViewController]
        }

    }
    

}
