//
//  PDFReportViewController.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 05/05/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import SWRevealViewController

class PDFReportViewController : UIViewController {
    
    @IBOutlet var generateButton: BasicButton!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var sideMenu: UIBarButtonItem!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var StartDate: UITextField!
    var keyboardHeight : CGFloat?
    
    var pdfReport:Data? = nil
    
    @IBOutlet weak var PdfImage: UIImageView!
    lazy var datePicker: UIDatePicker = {
           let picker = UIDatePicker();
           picker.datePickerMode = .date
          picker.addTarget(self, action: #selector(datePickerChanged(_:)), for: .valueChanged)
           return picker
       }()

       @objc func datePickerChanged(_ sender: UIDatePicker){
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "MM/dd/yyyy"
           StartDate.text = dateFormatter.string(from: sender.date)
       }


    lazy var datePicker2: UIDatePicker = {
           let picker = UIDatePicker();
           picker.datePickerMode = .date
           picker.maximumDate = Date()
           picker.addTarget(self, action: #selector(datePickerChangedend(_:)), for: .valueChanged)
           return picker
       }()

       @objc func datePickerChangedend(_ sender: UIDatePicker){
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "MM/dd/yyyy"
           endDate.text = dateFormatter.string(from: sender.date)
       }

    override func viewDidLoad() {
        super.viewDidLoad()
        StartDate.doneAccessory = true
        endDate.doneAccessory = true
        self.PdfImage.image = LogoResource.sharedInteractor.splashLogo
        StartDate.inputView = datePicker
        self.view.addBackgroundImage()
        endDate.inputView = datePicker2
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"

        StartDate.text = dateFormatter.string(from: Date())
        endDate.text = dateFormatter.string(from: Date())

        if self.revealViewController() != nil {
            sideMenu.target = self.revealViewController()
            sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }

    @IBAction func generateButton_Tapped(_ sender: Any) {
        
        let patientId = UserDefaults.standard.value(forKey: Constants.USER_DEF_PATIENT_ID);
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let toDate = dateFormatter.date(from: endDate.text!)//Date();        //7 dayes before toDate
        let fromDate =  dateFormatter.date(from: StartDate.text!)//toDate.addingTimeInterval(-7*24*60*60)
        NetworkManager().getAssessmentRangeData(patientId: patientId as! String, fromDate: fromDate!, toDate: toDate!, start: 0, count: 0) { (responseData) in
            //                print("responseDate=\(responseDate)")
            if responseData != nil{
                let totalItemCount:Int = responseData!.get("TotalItemCount").int ?? 0
                if totalItemCount == 0 {
                    self.showAlert(title:"Info" , message: "Data is unavailable for the selected Date Range")
                }else{
                    var sections = [PDFSection]()
                    for eachAssessment in responseData!.get("PageItems") ?? JSONTraverser() {
                        var secText = ""
                        for question in eachAssessment.get("Questions") ?? JSONTraverser() {
                            if question.get("Text").string != nil{
                                secText.append(question.get("Text").string!)
                                secText.append("\n")
                            }
                        }
                        var titleText = eachAssessment.get("Name").string!
                        //"AnsweredDateTime":"2020-03-17T19:46:13",
                        if(eachAssessment.get("AnsweredDateTime").value != nil){
                            let dateString = eachAssessment.get("AnsweredDateTime").string!
                            let dateFormatter = DateFormatter()
                            //        var date:Date? = nil
                            dateFormatter.locale = Locale.current
                            // save locale temporarily
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                            var date = dateFormatter.date(from:dateString)
                            
                            if date == nil{
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                date = dateFormatter.date(from:dateString)
                            }
                            
                            let dateString2 = self.getDateascontinuesStrings(datefromfunction: date!)
                            if(date != nil){
                                titleText.append("\n Taken on :")
                            //    titleText.append("\(date!)")
                                 titleText.append("\(dateString2)")
                            }
                        }
                        //                print("assessment:\(eachAssessment.get("Name"))")
                        sections.append(PDFSection(title: titleText, body: secText))
                        
                    }
                    
                    let patientDetails = CoreDataUtils().getUserDetails();
                    
                    let patientNameString = "\(patientDetails.firstName!) \(patientDetails.lastName!)"
                    
                    let pdfData = PDFCreator().createPatientReport(mainTitle: "COVID-19", subTitle: "Containment and mitigation", sections: sections)
                    self.pdfReport = pdfData
                    DispatchQueue.main.async {
                        print("ALAMOFIRE:It's on main queue now")
                        if(self.pdfReport != nil){
                            self.performSegue(withIdentifier: "previewPDFSegue", sender: sender)
                            
                        }
                    }
                }

            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if StartDate.text!.isEmpty || endDate.text!.isEmpty{
            self.showAlert(title: "Info", message: "Please select the date range")
        }
        else if segue.identifier == "previewPDFSegue" {
            if let destinationVC = segue.destination as? PDFPreviewViewController {
                destinationVC.documentData  = self.pdfReport
            }
        }
    }
    
    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 150
    }
    
   func getDateascontinuesStrings(datefromfunction:Date) -> (String)
   {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current
    
    // save locale temporarily
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    var date = datefromfunction//dateFormatter.date(from:datefromfunction)
    if date == nil
    {
        date = datefromfunction
    }
    
    let dateFormatterWithDay = DateFormatter()
    dateFormatterWithDay.dateStyle = .medium
    dateFormatterWithDay.timeStyle = .medium
    
    let dateString = dateFormatterWithDay.string(from: date)
    return dateString
    
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardHeight = keyBoardHeight()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.3//keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
}
