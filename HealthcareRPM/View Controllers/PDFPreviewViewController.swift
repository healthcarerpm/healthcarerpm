//
//  PDFPreviewViewController.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 05/05/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import UIKit
import PDFKit

class PDFPreviewViewController:UIViewController{
    
    public var documentData: Data?
    @IBOutlet var pdfView: PDFView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = self.documentData {
            pdfView.document = nil
            pdfView.document = PDFDocument(data: data)
            pdfView.autoScales = true
            (pdfView.subviews[0] as! UIScrollView).isScrollEnabled = true
            
            for subview in pdfView.subviews {
                 if let scrollView = subview as? UIScrollView {
                        //   scrollView.bounces = true
                            scrollView.isScrollEnabled = true
                           
                       }
           }
        }
        
//        let url = URL(string: "https://arxiv.org/pdf/1803.10760.pdf")
//        let pdfDocument = PDFDocument(url: url!)
//
//        pdfView.document = pdfDocument
//        pdfView.displayMode = PDFDisplayMode.singlePageContinuous
//        pdfView.autoScales = true
        
    }
    
}
