//
//  SessionViewController.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 02/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//


import UIKit

class SessionListViewController : UITableViewController{
    
    var viewModel: ViewModel!
    let myTimer: Timer = Timer(timeInterval: 5.0, target: self, selector: "refreshData", userInfo: nil, repeats: true)
    
    
    override func viewDidLoad() {
        
        UINavigationBar.appearance().isTranslucent = false
        self.tableView.allowsSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        loadTableViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadTableViewModel()

        DispatchQueue.main.async{
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfAssessments ?? 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 228.0;//Your custom row height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let headline = self.viewModel.text(at: indexPath.row)
        let assessmentId = self.viewModel.assessmentId(at: indexPath.row)
        let sessionId = self.viewModel.sessionId(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "SessionItemCell", for: indexPath) as! SessionItemCell

        cell.titleLabel?.text = headline
        cell.titleLabel?.textColor = UIColor.black
        
        cell.dateLabel?.text = self.viewModel.date(at: indexPath.row)
        cell.dateLabel?.textColor = UIColor.black
        
        cell.startTimeLabel?.text = self.viewModel.startTime(at: indexPath.row)
        cell.startTimeLabel?.textColor = UIColor.black
        
        cell.endTimeLabel?.text = self.viewModel.endTime(at: indexPath.row)
        cell.endTimeLabel?.textColor = UIColor.black
        
        // add border and color
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        cell.updateConstraintsIfNeeded()
        cell.startButton.tag = indexPath.row-1
        cell.startButton.accessibilityIdentifier = assessmentId
        cell.startButton.accessibilityLabel = sessionId
        cell.startButton.accessibilityHint = headline
        
        //check first - completed,submiited or no node pending
        let now = Date();
        
        print("\(self.viewModel.startDateTime(at: indexPath.row))---\(now)---\(self.viewModel.endDateTime(at: indexPath.row))")
        
        if(self.viewModel.isSessionCompleted(at: indexPath.row) == true || self.viewModel.isSessionSubmitted(at: indexPath.row) == true ){
                // completed or submitted flag not updated
            cell.startButton.isEnabled = false
            cell.startButton.alpha = 0.25
        } else if (self.viewModel.startDateTime(at: indexPath.row) <= now && now <= self.viewModel.endDateTime(at: indexPath.row) ){
            cell.startButton.isEnabled = true
            cell.startButton.alpha = 1
        }else{
            cell.startButton.isEnabled = false
            cell.startButton.alpha = 0.25
        }
        
        cell.startButton.addTarget(self, action: #selector(onStartSessionClicked(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onStartSessionClicked(sender: UIButton){
        let tag = sender.tag
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "healthSessionViewController") as! HealthSessionViewController
        nextViewController.viewType = tag
        let coreDataUtils = CoreDataUtils()
        let nodes = coreDataUtils.getUntakenChildNodesOfAssessmentParent(assessmentId: sender.accessibilityIdentifier ?? "", sessionId:sender.accessibilityLabel ?? "", parentId: sender.accessibilityIdentifier ?? "")
        if(nodes.count > 0){
            //update  8-Oct-19
            let incompleteRootNodesCount = CoreDataUtils().getUntakenChildNodesOfAssessmentParent(assessmentId:  sender.accessibilityIdentifier ?? "", sessionId:sender.accessibilityLabel ?? "", parentId: sender.accessibilityIdentifier ?? "").count
            
            let finishedRootNodesCount = CoreDataUtils().getTakenChildNodesOfAssessmentParent(assessmentId:  sender.accessibilityIdentifier ?? "", sessionId:sender.accessibilityLabel ?? "", parentId: sender.accessibilityIdentifier ?? "").count
            
            nextViewController.viewModel = HealthSessionViewController.ViewModel(assessmentId: sender.accessibilityIdentifier ?? "",sessionId:sender.accessibilityLabel ?? "", currentParentId: sender.accessibilityIdentifier ?? "", currentNode: nodes[0], questionName: sender.accessibilityHint!,completedQuestionNumber: finishedRootNodesCount, totalQuestions: (finishedRootNodesCount+incompleteRootNodesCount))
            self.navigationController?.show(nextViewController, sender: self)
        }
        
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func loadTableViewModel(){
        let coreDataUtils = CoreDataUtils()
        self.viewModel = ViewModel(assessments: coreDataUtils.getAssessments())
    }
    
    
}

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}



