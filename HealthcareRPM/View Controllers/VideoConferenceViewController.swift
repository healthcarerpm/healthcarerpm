////
////  VideoConferenceViewController.swift
////  HealthcareRPM
////
////  Created by Thejus Jose on 23/01/19.
////  Copyright © 2019 Isham. All rights reserved.
////
//
//import UIKit
//import MobileRTC
//
//
//class VideoConferenceViewController : UIViewController , MobileRTCAuthDelegate, MobileRTCMeetingServiceDelegate{
//
//    var rtc_appKey = "w4EAhwLIYgjFGq0sBFeDrU3EF9CHPEr1JNBT";
//    var rtc_appSecret="vgZE38X7uoxXQ3WwEjWdxTgJwD1BSos7mHlK";
//    var rtc_domain="zoom.us";
//
//    var rtc_userid = CoreDataUtils().getUserDetails().firstName//"thejusjj"
//    var rtc_username = CoreDataUtils().getUserDetails().firstName
//    var rtc_token=""
//    var rtc_meeting=""
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view, typically from a nib.
//        sdkAuth()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//
//    func onMeetingReturn(_ error: MobileRTCMeetError, internalError: Int) {
//        print("onMeetingReturn: \(error) internalError: \(internalError) ");
//    }
//
//    func onMeetingStateChange(_ state: MobileRTCMeetingState) {
//        print("onMeetingStateChange: \(state)");
//    }
//
//    func sdkAuth(){
//        MobileRTC.shared()?.setMobileRTCDomain(rtc_domain);
//        let service = MobileRTC.shared()?.getAuthService();
//        if(service != nil){
//            service?.delegate=self;
//            service?.clientKey = rtc_appKey;
//            service?.clientSecret = rtc_appSecret;
//            service?.sdkAuth();
//        }
//
//    }
//
//    func startVideo(){
//        let mservice = MobileRTC.shared().getMeetingService();
//        if (mservice == nil){
//            print("RETURNED WITHOUT JOINING MEETINGS")
//            return;
//        }
//
//        mservice?.delegate = self;
//
//        let dic = [kMeetingParam_Username: rtc_username,
//                   kMeetingParam_MeetingNumber: rtc_meeting,
//                   ];
//
//        let ret = mservice?.joinMeeting(with: dic);
//        print("join meeting ret: \(ret)");
//    }
//
//    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
//        print("onMobileRTCAuthReturn....  \(returnValue)");
//        startVideo()
//    }
//
//    func onMeetingEndedReason(_ reason: MobileRTCMeetingEndReason) {
//        let board = UIStoryboard.init(name: "Main", bundle: nil)
//        let tab = storyboard?.instantiateViewController(withIdentifier: "dashboardNav")
//        self.present(tab!, animated: true, completion: nil)
//    }
//}
