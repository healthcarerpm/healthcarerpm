//
//  LoginViewController.swift
//  HealthcareRPM
//
//  Created by Isham on 27/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var forgotPasswordLabel: UIButton!
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBOutlet weak var descriptionView: UITextView!
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    var keyboardHeight : CGFloat?
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
 
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = #imageLiteral(resourceName: "bg")
        parentView.insertSubview(backgroundImage, at: 0)
         RPMUserSettings.sharedInstance.patientId = ""
        
        userNameField.rightViewMode = UITextField.ViewMode.always
        passwordField.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: -80, y: 0, width: 5, height: 5))
        let image = UIImage(named: "outline_mail_black")
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        userNameField.rightView = imageView
        if UserDefaults.standard.value(forKey: "username") != nil{
            userNameField.text = UserDefaults.standard.value(forKey: "username") as? String
        }
        let passimageView = UIImageView(frame: CGRect(x: -80, y: 0, width: 5, height: 5))
        passimageView.contentMode = .scaleAspectFit
        let passimage = UIImage(named: "outline_lock_black")
        passimageView.image = passimage
        passwordField.rightView = passimageView

        userNameField.doneAccessory = true
        passwordField.doneAccessory = true

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        CoreDataUtils().deleteAllDatabase()
      
        
    }
    
@objc func keyboardWillShow(notification: NSNotification) {
    keyboardHeight = keyBoardHeight()
    if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.4//keyboardSize.height/2
        }
    }
}

    @IBAction func navigateToRegisterVC(_ sender: UIButton) {
        
        performSegue(withIdentifier: "Consent", sender: sender)
                
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 50
    }
    
    func validateInput()->Bool{
//        if let email = self.userNameField.text
//        {
//            if(self.validateEmail(enteredEmail: email)) != true {
//                self.showAlert(title: "Info", message: "Invalid Email Id")
//                return false
//            }
//            
//        }
        return true
    }
    

    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }

    @IBAction func attemptLogin(_ sender: Any)
    {
        if userNameField.text?.isEmpty ?? true {
            self.showAlert(title: "Login", message: "Please enter valid email address")
           return
        }else if passwordField.text?.isEmpty ?? true {
            self.showAlert(title: "Login", message: "Please enter valid password")
            return
        }
        if self.validateInput() == false{
            self.showAlert(title: "Login", message: "Please enter valid email address")
                     return
        }
        self.showSpinner(onView: self.view)
        NetworkManager().login(username: userNameField.text!, password: passwordField.text!) {
            (response) -> () in
           
            self.removeSpinner(completion: {
                if response != nil  {
                    
                    if (response as? String == "Error"){
                        self.showAlert(title: "Login", message: "Please enter valid username/password")
                        
                    }
                    else{
                        UserDefaults.standard.set(self.userNameField.text!, forKey: "username")
                        RPMUserSettings.sharedInstance.username = self.userNameField.text!
                        let pId = RPMUserSettings.sharedInstance.patientId ?? ""
                        
                        NetworkManager().getpatientconsent(patienId: pId, completion: {
                            (status:String) in
                            if status == "pending" || status == "failure"{
                                MeasurementIdentifier.sharedInteractor.isfromdashboardforTerms = false
                                self.performSegue(withIdentifier: "Terms", sender: nil)
                            }else{
//                                if MeasurementIdentifier.sharedInteractor.customerId == "4e11d2bc-e5ea-499e-836e-cfa403e88ce7"{
                                    self.performSegue(withIdentifier: "LogInToDashboard", sender: nil)
//                                }else{
//                                    self.performSegue(withIdentifier: "directDashboard", sender: nil)
//                                }
                            }
                        })
                    }
                }
                else{
                    self.showAlert(title: "Login", message: "Please enter valid username/password")
                }
            })
         
        }
        

    }
    
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    
    
    @IBAction func onForgotPwdClicked(_ sender: UIButton) {
        
            let alert  = UIAlertController(title: "Warning", message: "Please contact the administrator for your new password or send an email to info@healthcarerpm.com", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onForgotPasswordClicked(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "username") != nil{
            NetworkManager().forgotpassword(params: ["Email":UserDefaults.standard.value(forKey: "username") as! String], completion: {
                _ in
            })
              RPMUserSettings.sharedInstance.username = UserDefaults.standard.value(forKey: "username") as! String
            let alert  = UIAlertController(title: "Password Reset", message: "We have sent an email to your registered email address", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Okay",
                                           style: UIAlertAction.Style.default,
                                  handler: {
                                    (alert: UIAlertAction!) in
                                    self.performSegue(withIdentifier: "ForgotPassword", sender: nil)}
             ))
            self.present(alert, animated: true, completion: nil)
         
        }
        else if self.userNameField.text != ""{
           
            if self.validateInput() == false{
                let alert  = UIAlertController(title: "Password Reset", message: "Please enter a valid email address", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                 RPMUserSettings.sharedInstance.username = self.userNameField.text!
                NetworkManager().forgotpassword(params: ["Email": self.userNameField.text!], completion: {
                    _ in
                })
                let alert  = UIAlertController(title: "Password Reset", message: "We have sent an email to your registered email address", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(
                    alert: UIAlertAction!) in
                    self.performSegue(withIdentifier: "ForgotPassword", sender: nil)}))
                self.present(alert, animated: true, completion: nil)

            }
        }
        else if self.userNameField.text == ""
        {
            let alert  = UIAlertController(title: "Password Reset", message: "Please enter an email address", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        
        print("tap working")
    }
    
    //Logout
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    


}

extension LoginViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location > 3{
                 return false
             }
        let nsString:NSString? = textField.text as NSString?
        let updatedString = nsString?.replacingCharacters(in:range, with:string);
        
        textField.text = updatedString
        //Setting the cursor at the right place
        let selectedRange = NSMakeRange(range.location + string.count, 0)
        let from = textField.position(from: textField.beginningOfDocument, offset:selectedRange.location)
        let to = textField.position(from: from!, offset:selectedRange.length)
        textField.selectedTextRange = textField.textRange(from: from!, to: to!)
        
        //Sending an action
        textField.sendActions(for: UIControl.Event.editingChanged)
        
        return false
    }
}


