//
//  AilmentSelectionViewController.swift
//  HealthcareRPM
//
//  Created by Admin on 1/14/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class AilmentSelectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var ailmentSelected = [AilmentSelected]()
    var scrollView = UIScrollView()
    var patientCondtions : [RPMPatientCondition] = []

    let images : [UIImage] = [#imageLiteral(resourceName: "heart"), #imageLiteral(resourceName: "diabetes"), #imageLiteral(resourceName: "copd"),#imageLiteral(resourceName: "pregnancy")]
    let ailments = ["Congestive heart failure", "Diabetes","COPD","Pregnancy"]
    let customCellId = "customCellId"
    
    
    let nextBtn : UIButton = {
        let btn = UIButton()
        let attributedText = NSAttributedString(string: "Next", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 26)!, NSAttributedString.Key.foregroundColor : UIColor.white])
        btn.setAttributedTitle(attributedText, for: .normal)
//        btn.setBackgroundImage(#imageLiteral(resourceName: "backBarButton"), for: .normal)
        btn.contentMode = .scaleAspectFit
        btn.addTarget(self, action: #selector(nextBtnClicked), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let screenTitle: UILabel = {
        let label = UILabel()
        
        let attributedText = NSAttributedString(string: "Select A Condition", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 26)!, NSAttributedString.Key.foregroundColor : UIColor.white])
        label.attributedText = attributedText
        label.backgroundColor = .clear
        label.textAlignment = .left
        label.numberOfLines = 1
//        label.adjustsFontSizeToFitWidth = true
//        label.minimumScaleFactor = 0.2
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleTextView: UILabel = {
        let textview = UILabel()
        
        let attributedText = NSAttributedString(string: "Please select a condition and let us customize your experience", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 20)!, NSAttributedString.Key.foregroundColor : Constants.COMMON_BLUE])
        textview.attributedText = attributedText
//        textview.isEditable = false
//        textview.isSelectable = false
//        textview.textAlignment = .center
        textview.numberOfLines = 0
        textview.minimumScaleFactor = 0.2
        textview.adjustsFontForContentSizeCategory = true
        textview.adjustsFontSizeToFitWidth = true
        textview.translatesAutoresizingMaskIntoConstraints = false
        
        return textview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpMainView()
        self.getPatientConditions()
       
     }
    

    func setUpMainView()
    {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = #imageLiteral(resourceName: "healthSessionBgImg")
        self.view.insertSubview(backgroundImage, at: 0)
        
        self.view.addSubview(screenTitle)
        screenTitle.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 25).isActive = true
        screenTitle.heightAnchor.constraint(equalToConstant: 60).isActive = true
        screenTitle.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.7).isActive = true
//        screenTitle.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        screenTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        self.view.addSubview(nextBtn)
        nextBtn.centerYAnchor.constraint(equalTo: screenTitle.centerYAnchor).isActive = true
        nextBtn.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.2).isActive = true
        nextBtn.heightAnchor.constraint(equalToConstant: 60).isActive = true
        nextBtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        
        let bgView = UIView()
        bgView.backgroundColor = .white
        bgView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(bgView)
        
        bgView.topAnchor.constraint(equalTo: screenTitle.bottomAnchor, constant: 16).isActive = true
        bgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        bgView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        bgView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
        
        bgView.addSubview(titleTextView)
        titleTextView.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 16).isActive = true
        titleTextView.leadingAnchor.constraint(equalTo: bgView.leadingAnchor, constant: 32).isActive = true
        titleTextView.widthAnchor.constraint(equalTo: bgView.widthAnchor, multiplier: 0.9).isActive = true
        titleTextView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 100, width: self.view.frame.width*0.95, height: self.view.frame.height), collectionViewLayout: flowLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        bgView.addSubview(collectionView)
        bgView.bringSubviewToFront(collectionView)
        
        collectionView.backgroundColor = .clear
        collectionView.register(CustomCell.self, forCellWithReuseIdentifier: customCellId)
        
        collectionView.isUserInteractionEnabled = true

    }
    
    @objc func nextBtnClicked()
    {
        if  RPMUserSettings.sharedInstance.patientCondition.count == 0{
            self.showAlert(title: "Info", message: "Please select a condition to proceed")
        }else{
            self.performSegue(withIdentifier: "NavigateToNurse", sender: self)
        }    }
    
    var isImgSelected = false
    var selectedAilments = [String]()
    
    @objc func tappedOnImage(gestureRecognizer: UIGestureRecognizer)
    {
        print("selected")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count

    }
    
    @IBAction func addailmenttolist(_ sender: UIButton) {
        
        if  RPMUserSettings.sharedInstance.patientCondition.count == 0{
            self.showAlert(title: "Info", message: "Please select a condition to proceed")
        }else{
            self.performSegue(withIdentifier: "NavigateToNurse", sender: self)
        }
    }
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let customCell = collectionView.dequeueReusableCell(withReuseIdentifier: customCellId, for: indexPath) as! CustomCell
        
        customCell.backgroundColor = .clear
        customCell.mainImageView.image = images[indexPath.item]
        customCell.instructionLabel.text = ailments[indexPath.item]
        
        return customCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 0
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2.2, height: collectionViewSize/2.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let cell = collectionView.cellForItem(at: indexPath) as! CustomCell
        guard !self.ailmentSelected.isEmpty else {
            self.ailmentSelected.append(AilmentSelected(isSelected: true, itemId: indexPath.item))
            cell.selectionButton.setImage(#imageLiteral(resourceName: "selected-image"), for: .normal)
            if (indexPath.row < patientCondtions.count){
                RPMUserSettings.sharedInstance.patientCondition.append(patientCondtions[indexPath.row])
            }
            return
        }
        if self.ailmentSelected.contains(where: { $0.itemId == indexPath.item})
        {
            cell.selectionButton.setImage(UIImage(named: "unselected-image"), for: .normal)
            if indexPath.row <  RPMUserSettings.sharedInstance.patientCondition.count{
                RPMUserSettings.sharedInstance.patientCondition.remove(at: indexPath.row)
            }

        }
        else
        {
            self.ailmentSelected.append(AilmentSelected(isSelected: true, itemId: indexPath.item))
            cell.selectionButton.setImage(#imageLiteral(resourceName: "selected-image"), for: .normal)
            if (indexPath.row < patientCondtions.count){
                RPMUserSettings.sharedInstance.patientCondition.append(patientCondtions[indexPath.row])
            }
        }
        
        
    }
    
    func getPatientConditions(){
        let rpmParameters: [String: Any] = ["CustomerId":"09349ed1-a358-456b-a6fb-3257df3d7ffb"]
        NetworkManager().getPatientConditions(requestData: rpmParameters){(response) -> () in
            
            if let value = response as? [String: Any]{
                
                let details : [[String:Any]] = value["PageItems"] as! [[String : Any]]
                
                for condition in details{
                    let patientCon : RPMPatientCondition = RPMPatientCondition(patientCondition: condition)
                    self.patientCondtions.append(patientCon)
                    
                }
                
            }
            
            
        }
    }
    
}


//MARK: - UICollectionViewCell
class CustomCell: UICollectionViewCell {
    var isImgSelected = false

    let mainImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "copd")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
//    let selectionButton: UIButton = {
//        let button = UIButton()
//        button.setBackgroundImage(UIImage(named: "unselected-image"), for: .normal)
//        button.translatesAutoresizingMaskIntoConstraints = false
//        return button
//    }()
    
    let instructionLabel: UILabel = {
        let label = UILabel()
        label.text = "Pregnancy"
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.2030155957, green: 0.6176377535, blue: 0.9852994084, alpha: 1)
        label.font = UIFont(name: "Bogle-Bold", size: 15)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        label.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    let selectionButton = UIButton()
   
    
    func setUpView()
    {
        addSubview(mainImageView)
        mainImageView.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        mainImageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        mainImageView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.9).isActive = true
        mainImageView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.8).isActive = true
        
        selectionButton.setBackgroundImage(UIImage(named: "unselected-image"), for: .normal)
        selectionButton.translatesAutoresizingMaskIntoConstraints = false
        mainImageView.addSubview(selectionButton)
        selectionButton.topAnchor.constraint(equalTo: mainImageView.topAnchor, constant: 16).isActive =  true
        selectionButton.leadingAnchor.constraint(equalTo: mainImageView.leadingAnchor, constant: 16).isActive = true
        selectionButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
        selectionButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        selectionButton.addTarget(self, action: #selector(selectImage(sender:)), for: .touchUpInside)
        
        mainImageView.addSubview(instructionLabel)
        instructionLabel.centerYAnchor.constraint(equalTo: mainImageView.centerYAnchor).isActive = true
        instructionLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        instructionLabel.leadingAnchor.constraint(equalTo: mainImageView.leadingAnchor).isActive = true
        instructionLabel.widthAnchor.constraint(equalTo: mainImageView.widthAnchor).isActive = true
    }
    
    
    
    @objc func selectImage(sender button: UIButton)
    {
        print("selected")
        
        isImgSelected = !isImgSelected
//        if isImgSelected
//        {
//            let intVal = button.tag
//            selectedAilments.append(ailments[intVal])
//        }
//        else
//        {
//            let intVal = button.tag
//            selectedAilments.remove(at: intVal)
//        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class AilmentSelected
{
    var isSelected: Bool?
    var itemId: Int?
    
    init(isSelected:Bool, itemId:Int) {
        self.isSelected = isSelected
        self.itemId = itemId
    }
    
}
