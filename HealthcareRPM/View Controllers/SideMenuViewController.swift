
import UIKit

class SideMenuViewController :UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let coreDataUtils = CoreDataUtils()
    
    var mainViewController: UIViewController!
    
    @IBOutlet weak var sideMenuContentTableView: UITableView!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
    var sideMenuContentTextArray = [String]()
    var sideMenuContentImageArray = [UIImage]()
    
    var placeHolderText = "Write your feedback here.........."
    var activeTextView:UITextView!
    
    var menuOptions = ["Home","Report","Change Password","Feedback","Terms & Conditions","Sign Out"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sideMenuContentImageArray = [#imageLiteral(resourceName: "SideMenuHome") ,#imageLiteral(resourceName: "pdfFile") ,#imageLiteral(resourceName: "ChangePassword"),#imageLiteral(resourceName: "Feedback"),#imageLiteral(resourceName: "LegalInformation"),#imageLiteral(resourceName: "SignOut")]
        userProfileImage.layer.cornerRadius = userProfileImage.frame.size.width/2
        userProfileImage.clipsToBounds = true
        emailLabel.text = RPMUserSettings.sharedInstance.username
        setUserInfo()
        self.view.addBackgroundImage()

     //   addBackgroundImage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUserInfo()
        
   //     addBackgroundImage()
        
    }
    
    
     func setUserInfo() {
            let patientDetails = coreDataUtils.getUserDetails();
            
            self.patientLabel.text = "\(patientDetails.firstName!) \(patientDetails.lastName!)"
        }
        
        //Update BackGround ImageView
        func addBackgroundImage(){
            self.backgroundImage.removeFromSuperview()
            backgroundImage.image = UIImage(named: "sidemenubackground")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
            
        }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.sideMenuContents.text = self.menuOptions[indexPath.row]
        cell.sideMenuContentImage.image = self.sideMenuContentImageArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "homeSegue", sender: self)
            break
        case 1:
            self.performSegue(withIdentifier: "pdfReportSegue", sender: self)
            break
        case 2:
            self.performSegue(withIdentifier: "changePasswordSegue", sender: self)
            break
        case 3:
            self.performSegue(withIdentifier: "feedbackSeque", sender: self)
            break
        case 4:
            self.performSegue(withIdentifier: "Terms", sender: self)
            MeasurementIdentifier.sharedInteractor.isfromdashboardforTerms = true
                    break

         case 5:
            DispatchQueue.main.async {
                 let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: .alert)
                 alert.addAction(UIAlertAction(title: "Logout",style:.default,handler: {
                     //delete all data before logout
                     (alert: UIAlertAction!) in UserDefaults.standard.set("no", forKey: "LoggedIn")
                         UserDefaults.standard.set("no", forKey: "LoggedIn")
                         UserDefaults.standard.synchronize()
                         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                         let loginVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                         self.present(loginVC, animated: true, completion: nil)
                         }))
                 alert.addAction(UIAlertAction(title: "Cancel",style:.cancel,handler: nil))
                            self.present(alert, animated: true, completion: nil)

             }
             break

        default:
            print("Not In use")
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "changePasswordSegue" {
               let navigationContoller = segue.destination as! UINavigationController
               let _ = navigationContoller.topViewController as! ChangePasswordViewController
           }
           else if segue.identifier == "logOutSegue" {//“Are you sure you want to sign out?”
               self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil);
           }
       }
    
    
   
    
    
}



////
////  SideMenuViewController.swift
////  HealthcareRPM
////
////  Created by Thejus Jose on 06/04/20.
////  Copyright © 2020 Isham. All rights reserved.
////
//import UIKit
////import AlamofireImage
////import Localize_Swift
//
//class SideMenuViewController: UIViewController {
//
//
//    @IBOutlet weak var sideMenuContentTableView: UITableView!
//    @IBOutlet weak var patientLabel: UILabel!
//    @IBOutlet weak var emailLabel: UILabel!
//    @IBOutlet weak var phoneLabel: UILabel!
//    @IBOutlet weak var userProfileImage: UIImageView!
//    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
//    var sideMenuContentTextArray = [String]()
//    var sideMenuContentImageArray = [UIImage]()
//
//    var placeHolderText = "Write your feedback here.........."
//    var activeTextView:UITextView!
//
//    var mainViewController: UIViewController!
//
//    var parameter = [String:Any]()
//    var statesArray = [Any]()
//    var userDefault:Int?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//     //  self.view.addBackgroundImage()
//        //userProfile
//       // self.userDefault =  UserDefaults.standard.value(forKey: "PatientType") as? Int
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let mainViewController = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
//        self.mainViewController = UINavigationController(rootViewController: mainViewController)
//
//        self.sideMenuContentImageArray = [UIImage(named: "SideMenuHome")!,UIImage(named: "ChangePassword")!,UIImage(named: "LegalInformation")!,UIImage(named: "Feedback")!,UIImage(named: "SignOut")!]
//        userProfileImage.layer.cornerRadius = userProfileImage.frame.size.width/2
//        userProfileImage.clipsToBounds = true
//
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SideMenuVC.openCameraGallery))
//        self.userProfileImage.isUserInteractionEnabled = true
//        self.userProfileImage.addGestureRecognizer(tapGestureRecognizer)
//
//        self.setSideMenuTextToLocalizeLangauge()
//
//    }
//
//
//    @objc func openCameraGallery() {
//        let actionSheetController: UIAlertController = UIAlertController(title: SELECT_IMAGE.localized(), message: CHOOSE_IMAGE_FROM_GALLERY_OR_CAMERA.localized(), preferredStyle: .actionSheet)
//
//        // create an action
//        let actionGallery: UIAlertAction = UIAlertAction(title: GALLERY.localized(), style: .default) { action -> Void in
//            let picker  = UIImagePickerController()
//            picker.delegate = self
//            picker.navigationBar.backgroundColor = UIColor(red: 97.0/255, green: 177.0/255, blue: 242.0/255, alpha: 0.3)
//            self.present(picker, animated: true, completion: nil)
//
//        }
//
//        let actionCameraRoll: UIAlertAction = UIAlertAction(title: CAMERA.localized(), style: .default) { action -> Void in
//            let imagePicker =  UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = .camera
//            self.present(imagePicker, animated: true, completion: nil)
//        }
//
//        let removeImage: UIAlertAction = UIAlertAction(title: REMOVE.localized(), style: .destructive) { action -> Void in
//
//              let userTypeDefault = UserDefaults.standard.value(forKey: "PatientType") as! Int
//              if userTypeDefault == 1 {
//                 self.parameter = ["UserId":UserInfoCache.Instance.getPatientID(),"UserEmail":UserInfoCache.Instance.getPatientEmail(),"UserType":1]
//                self.deleteUserProfileImgae(self.parameter, DELETE_USER_PROFILE_IMAGE)
//               }
//              else{
//                self.parameter = ["UserId":UserInfoCache.Instance.getPatientID(),"UserEmail":UserInfoCache.Instance.getPatientEmail(),"UserType":2]
//                self.deleteUserProfileImgae(self.parameter, DELETE_USER_PROFILE_IMAGE)
//              }
//        }
//
//        let actionNothing = UIAlertAction(title: CANCEL.localized(), style: UIAlertAction.Style.default) { (action) in
//
//        }
//        actionSheetController.addAction(actionGallery)
//        actionSheetController.addAction(actionCameraRoll)
//
//        if  self.userProfileImage.image == UIImage(named: "userProfile"){
//            print("userProfile")
//        }else{
//            actionSheetController.addAction(removeImage)
//        }
//
//        actionSheetController.addAction(actionNothing)
//
//         FlurryManager.shared.createFlurryCrashlytic()
//
//        present(actionSheetController, animated: true, completion: nil)
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        FlurryManager.shared.setLogPageView()
//       self.userDefault =  UserDefaults.standard.value(forKey: "PatientType") as? Int
//
//        setUserInfo()
//        self.userProfileImage.image = UIImage(named: "userProfile")
//        addBackgroundImage()
//        if imageCache.image(withIdentifier: "UserProfileImage") != nil {
//            DispatchQueue.global(qos: .background).async {
//              DispatchQueue.main.async {
//                self.userProfileImage.image = imageCache.image(withIdentifier: "UserProfileImage")
//             }
//         }
//       }
//        else{
//             self.userProfileImage.image = UIImage(named: "userProfile")
//        }
//
//    NotificationCenter.default.addObserver(self, selector: #selector(setSideMenuTextToLocalizeLangauge), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
//    }
//
//
//
//    @objc func setSideMenuTextToLocalizeLangauge() {
//      self.sideMenuContentTextArray = [HOME.localized(),CHANGE_PASSCODE.localized(),LEGAL_INFORMATION.localized(),FEEDBACK.localized(),SIGN_OUT.localized()]
//        self.sideMenuContentTableView.reloadData()
//    }
//
//    //Update BackGround ImageView
//    func addBackgroundImage(){
//        self.backgroundImage.removeFromSuperview()
//        if self.userDefault == 1{
//            backgroundImage.image = UIImage(named: "background")
//            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
//            self.view.insertSubview(backgroundImage, at: 0)
//        }else{
//            backgroundImage.image = UIImage(named: "friendsbg")
//            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
//            self.view.insertSubview(backgroundImage, at: 0)
//        }
//    }
//
//    //Change Status bar color
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
//
//    func setUserInfo() {
//        self.patientLabel.text = "\(UserInfoCache.Instance.getFirstName()) \(UserInfoCache.Instance.getLastName())"
//        self.emailLabel.text = UserInfoCache.Instance.getPatientEmail()
//        self.phoneLabel.text = UserInfoCache.Instance.getPatientPhonenNumber()
//    }//patientUpdateSegue
//
//    @IBAction func profileUpdateButtonTapped(_ sender: Any) {
//       self.performSegue(withIdentifier: "editProfileSegue", sender: nil)
//        //CommonProvider.Instance.AlertMessage(vc: self, "Under Construction")
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "changePasswordSegue" {
//            let navigationContoller = segue.destination as! UINavigationController
//            let _ = navigationContoller.topViewController as! ChangePasswordViewController
//        }
//        else if segue.identifier == "feedbackSegue" {
//            let navigationContoller = segue.destination as! UINavigationController
//            let _ = navigationContoller.topViewController as! FeedBackViewController
//
//        }
//        else if segue.identifier == "logOutSegue" {//“Are you sure you want to sign out?”
//            self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil);
//        }
//    }
//
//}
//
//
//extension SideMenuViewController : UITableViewDelegate,UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.sideMenuContentImageArray.count
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 52.0
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell: SideMenuCell = self.sideMenuContentTableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
//        cell.sideMenuContents.text = self.sideMenuContentTextArray[indexPath.row]
//        cell.sideMenuContentImage.image = self.sideMenuContentImageArray[indexPath.row]
//        return cell
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        switch indexPath.row {
//        case 0:
//            self.performSegue(withIdentifier: "homeSegue", sender: nil)
//            break
//        case 1:
//            self.performSegue(withIdentifier: "changePasswordSegue", sender: nil)
//            break
//        case 3:
//            //self.performSegue(withIdentifier: "LoadLegalInformationSegue", sender: nil)
//            self.performSegue(withIdentifier: "feedbackSegue", sender: nil)
//            break
//        case 4:
//            //DispatchQueue.global(qos: .background).async {
//                DispatchQueue.main.async {
//                    CommonProvider.Instance.TowAleternativeAlertMessagePeroformAction(vc: self, "Are you sure want to sign out?", "Alert!", handler: { (action) in
//                        let userTypeDefault = UserDefaults.standard.value(forKey: "PatientType") as! Int
//                        if userTypeDefault == 2 {
//                        let userPasswordSaveInDefualt = UserDefaults.standard
//                        userPasswordSaveInDefualt.set(0, forKey: "selectedIndex")
//                        userPasswordSaveInDefualt.synchronize()
//                        FriendInfoCache.Instance.removePatientFriendInformationFromCache()
//                    }
//                    self.performSegue(withIdentifier: "logOutSegue", sender: nil)
//                    }, Cancelhandler: { (cancel) in
//                }, "Yes", "No")
//
//                }
//            //}
//            break
//        default:
//            print("Not In use")
//            break
//        }
//    }
//}
//
//
//
//
//
//
//
