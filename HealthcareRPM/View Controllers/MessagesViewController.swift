//
//  MessagesViewController.swift
//  HealthcareRPM
//
//  Created by Barath Vijayaraghavan on 9/30/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var messagestableview: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        NetworkManager().getpatientmessages(patientId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
            _ in
            
        })
        self.messagestableview.reloadData()
        self.messagestableview.separatorColor = UIColor.clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messagestableview.register(MessagesCell.self, forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if MeasurementIdentifier.sharedInteractor.messages != nil{
        return MeasurementIdentifier.sharedInteractor.messages!.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MessagesCell.init(style: .default, reuseIdentifier: "cell")
        let font = UIFont.init(name: "Bogle-Regular", size: 24)
        let message = MeasurementIdentifier.sharedInteractor.messages![indexPath.row]
        cell.label1?.text = "\(indexPath.row + 1)" + ") " + message.value!
        cell.textLabel?.font = font
        let date = getDateascontinuesStrings(datefromfunction: message.creationDate!)//message.creationDate?.components(separatedBy: "T")
        cell.label2?.text = date
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 
        return 60.0
    }
    
    func getDateascontinuesStrings(datefromfunction:String) -> (String)
    {
        let dateFormatter = DateFormatter()
        var date:Date? = nil
        dateFormatter.locale = Locale.current
        // save locale temporarily
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" //Z has been removed since if we download user data again we are not receiving SSSZ and app crashes
        date = dateFormatter.date(from:datefromfunction)
        /*
         Temporaray fix to handle both mobile and server values
         */
        if date == nil{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            //date = dateFormatter.date(from: datefromfunction)
            date = dateFormatter.date(from:datefromfunction)
            
        }
        
        let dateFormatterWithDay = DateFormatter()
        dateFormatterWithDay.dateFormat = "EEEE MMM d, yyyy HH:mm"
        let dateString = dateFormatterWithDay.string(from: date!)
        return dateString
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
