//
//  DashboardViewController.swift
//  OnboardingExample
//
//  Created by Isham on 20/03/2019.
//  Copyright © 2019 Anitaa. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import SWRevealViewController


class DashboardViewController : UIViewController{
    
  
    @IBOutlet weak var lineChartVC: LineChart!
    
   
    @IBOutlet weak var graphViewContainer: RoundRectView!
    
    @IBOutlet weak var mainMeasurementTitleLabel: UILabel!
    
    @IBOutlet weak var mainMeasurementRecordLabel: UILabel!
    
    @IBOutlet weak var firstMeasurementTitleLabel: UILabel!
    
    @IBOutlet weak var firstMeasurementRecordLabel: UILabel!
    
    @IBOutlet weak var secondMeasurementTitleLabel: UILabel!
    
    @IBOutlet weak var secondMeasurementRecordLabel: UILabel!
    
    @IBOutlet weak var thirdMeasurementTitleLabel: UILabel!
    
    @IBOutlet weak var thirdMeasurementRecordLabel: UILabel!
    
    @IBOutlet weak var fourthMeasurementTitleLabel: UILabel!
    @IBOutlet weak var fourthMeasurementRecordLabel: UILabel!
    
    @IBOutlet weak var fifthMeasurementTitleLabel: UILabel!
    
    @IBOutlet weak var fifthMeasurementRecordLabel: UILabel!
    
    @IBOutlet weak var firstlabel: UILabel!
    
    @IBOutlet weak var secondLabel: UILabel!
    
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthLabel: UILabel!
    
    @IBOutlet weak var fifthLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var mssageButton: UIButton!
    
    @IBOutlet weak var startMySession: BasicButton!
    
    
    
    //Prashant Changes
     var swContainer : SWRevealViewController?
     var viewModel: SessionModel!
    var repeatingTimerControl : Bool = true
       
       let coreDataUtils = CoreDataUtils()
       var viewType : Int = 3
       var tapGesture = UITapGestureRecognizer()
       let list = ["Blood pressure","Blood glucose","Pulse","Temperature","Weight"]
       var sessionIndex = 0
    
    
    
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
   
    
   
    
    override func viewDidLoad() {
        //UI
         super.viewDidLoad()
        
    
        self.setUpUI()
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(DashboardViewController.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        lineChartVC.addGestureRecognizer(tapGesture)
        lineChartVC.isUserInteractionEnabled = true
          let patientDetails = coreDataUtils.getUserDetails();
        
        if(patientDetails.firstName != nil){
                   //.uppercased() removed
                   self.navigationController?.navigationBar.topItem?.title = ("Hello, " + patientDetails.firstName! + "!")
               }
               else if RPMUserSettings.sharedInstance.firstname != "" || RPMUserSettings.sharedInstance.lastname != ""{
                   //.uppercased() removed
                   self.navigationController?.navigationBar.topItem?.title = ("Hello, " + RPMUserSettings.sharedInstance.firstname + "!")
               }
               else{
                   //.uppercased() removed
                   self.navigationController?.navigationBar.topItem?.title = "Hello!"
               }
        self.navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        MeasurementIdentifier.sharedInteractor.repeatingTimer.eventHandler = {
                   if self.repeatingTimerControl{
                       
                       print("Timer Fired: \(self.repeatingTimerControl)")
                       //get patient sessions from server
                       NetworkManager().getPatientSessions(patientId: patientDetails.patientId!,completion:{ result in
                           
                           self.loadTableViewModel()
                       })
                       
                       NetworkManager().postAssessmentResponse(patientId: patientDetails.patientId!, completion: { _ in })
                       if(false){   //I know this makes no sense, but it works. Go figure...
                           MeasurementIdentifier.sharedInteractor.repeatingTimer.suspend()
                       }
                       
                       NetworkManager().getpatientLastMessagesreadtime(userId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
                           _ in
                           
                       })
                       
                       NetworkManager().getpatientmessages(patientId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
                           _ in
                           let newmessages = MeasurementIdentifier.sharedInteractor.messages!.count - MeasurementIdentifier.sharedInteractor.initialmessagecounter
                           //                    self.scheduledSessionsLabel.text = String(MeasurementIdentifier.sharedInteractor.initialmessagecounter )
                         //  self.calculateActualNewMessages()
                           //    self.setTabBadge(itemCount:String(newmessages))
                           
                       })
                       
                       
                   }
               }
               
               MeasurementIdentifier.sharedInteractor.repeatingTimer.resume()
               
               NetworkManager().getPatientSessions(patientId: patientDetails.patientId!,completion:{ result in
                   
                   self.loadTableViewModel()
                   
               })
               
               DispatchQueue.main.async {
                   NetworkManager().postAssessmentResponse(patientId: patientDetails.patientId!, completion: { _ in })
                   
               }
          self.setLineChart()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        loadTableViewModel()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
           
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 44.0/255.0, green: 171.0/255.0, blue: 227.0/255.0, alpha: 1.0)
               self.navigationController?.navigationBar.tintColor = .white
               self.navigationController?.navigationBar.titleTextAttributes = [
                   NSAttributedString.Key.foregroundColor : UIColor.white
               ]
        
           let itemCount = CoreDataUtils().getActiveAssessmentsCount();
           if(itemCount > 0){
               
               //    setTabBadge(itemCount:itemCount)
           }
           NetworkManager().getpatientmessages(patientId: MeasurementIdentifier.sharedInteractor.patientId!, completion: {
               _ in
               let newmessages = MeasurementIdentifier.sharedInteractor.messages!.count - MeasurementIdentifier.sharedInteractor.initialmessagecounter
               //                    self.scheduledSessionsLabel.text = String(MeasurementIdentifier.sharedInteractor.initialmessagecounter )
             //  self.calculateActualNewMessages()
               //    self.setTabBadge(itemCount:String(newmessages))
               
           })
       // self.mssageButton.badge(text: String(MeasurementIdentifier.sharedInteractor.messages!.count))
       
                 self.setGraphValueForDashbord()
                  self.setMeasurementCount()
                   self.loadTableViewModel()
           }
           
           
           
      
           
       
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
         //  self.setViewType()
        // self.performSegue(withIdentifier: "ConnectedCareSegue", sender: nil)
    }
    func loadTableViewModel()
       {
           let coreDataUtils = CoreDataUtils()
           self.viewModel = SessionModel(assessments: coreDataUtils.getPendingAssessments())
           checkForSessionAvailability()
       }
    func setUpUI()
       {

           self.startMySession.setTitle("No Session Scheduled", for: .disabled)
           self.startMySession.titleLabel?.adjustsFontForContentSizeCategory = true
           self.startMySession.titleLabel?.adjustsFontSizeToFitWidth = true
           self.startMySession.titleLabel?.lineBreakMode = .byWordWrapping
           self.startMySession.titleLabel?.numberOfLines = 1
           self.startMySession.titleLabel?.minimumScaleFactor = 0.2
           
           self.startMySession.layer.cornerRadius = 10
           self.startMySession.isEnabled = false
    //       self.startMySession.alpha = 0.25
           self.startMySession.clipsToBounds = true
           
           //setting only logo in navigation
//           let imageView = UIImageView(frame: CGRect(x:UIScreen.main.bounds.size.width - 60, y:12, width:50, height:20))
//           imageView.contentMode = .scaleAspectFit
//           imageView.image = LogoResource.sharedInteractor.splashLogo
//           navigationController?.navigationBar.addSubview(imageView)
           //option to set the logo that covers entire bar
           //navigationController?.navigationBar.setBackgroundImage(logo, for: .default)
          
           

           

       }
    
     func checkForSessionAvailability(){
           
           
           let now = Date();
           var nextSessionStartTime = Date.distantFuture;
           var nextSessionFlag = false;
           let n = self.viewModel.numberOfAssessments
           if n > 0{
               for i in 0...n-1 {
                   
                   if (self.viewModel.startDateTime(at: i) <= now && now <= self.viewModel.endDateTime(at: i)  && ( self.viewModel.isSessionCompleted(at: i) == false && self.viewModel.isSessionSubmitted(at: i) == false) ){
                       self.startMySession.isEnabled = true
                       self.startMySession.setTitle("Start My Health Session Now", for: .normal)
                       self.startMySession.alpha = 1
                       sessionIndex = i
                       startMySession.tag = sessionIndex
                       let headline = self.viewModel.text(at: i)
                       let assessmentId = self.viewModel.assessmentId(at: i)
                       let sessionId = self.viewModel.sessionId(at: i)
                       startMySession.accessibilityIdentifier = assessmentId
                       startMySession.accessibilityLabel = sessionId
                       startMySession.accessibilityHint = headline
                       //                    self.startMySession
                       break
                   }else{
                       if(self.viewModel.startDateTime(at: i) > now && nextSessionStartTime > self.viewModel.startDateTime(at: i) &&  self.viewModel.isSessionCompleted(at: i) == false && self.viewModel.isSessionSubmitted(at: i) == false){
                           nextSessionStartTime = self.viewModel.startDateTime(at: i)
                           nextSessionFlag = true
                       }
                       self.startMySession.isEnabled = false
                       self.startMySession.alpha = 0.25
                   }
               }//end of for
               if(nextSessionFlag)
               {//set next session start time
                   let dateTimeFormatter = DateFormatter()
                   dateTimeFormatter.dateFormat="HH:mm,dd-MMM"
                   self.startMySession.setTitle("Next Session At:\(dateTimeFormatter.string(from: nextSessionStartTime))", for: .disabled)
               }
               
           }//end of if
           else{
               //set empty session for button
               self.startMySession.isEnabled = false
               self.startMySession.alpha = 0.25
               self.startMySession.setTitle("No Session Scheduled", for: .disabled)
           }
       }

    func setMeasurement(measurementTitleLabel: UILabel, measurementRecordLabel: UILabel){
        let titleLabelText = measurementTitleLabel.text
        let recordLabelText = measurementRecordLabel.text
       // measurementTitleLabel.text = mainMeasurementTitleLabel.text
       // measurementRecordLabel.text = mainMeasurementRecordLabel.text
        self.mainMeasurementTitleLabel.text = titleLabelText
        self.mainMeasurementRecordLabel.text = recordLabelText
        setViewType()
        self.setGraphValueForDashbord()
    }
    
    func setViewType(){
        
        switch mainMeasurementTitleLabel.text?.uppercased() {
       case "BLOOD PRESSURE" :
           viewType = 0
            break
        case "BLOOD GLUCOSE" :
            viewType = 1
            break
        case "SPO2" :
            viewType = 2
             break
        case "TEMPERATURE" :
           viewType = 3
            break
        case "WEIGHT" :
            viewType = 4
            break
        default :
            print("???")
            break
        }
        
    }
    
    @IBAction func startSessionButton_Tapped(_ sender: UIButton) {
        
        let tag = sessionIndex
               let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
               let nextViewController = storyBoard.instantiateViewController(withIdentifier: "healthSessionViewController") as! HealthSessionViewController
               nextViewController.viewType = tag
               let coreDataUtils = CoreDataUtils()
               let nodes = coreDataUtils.getUntakenChildNodesOfAssessmentParent(assessmentId: self.viewModel.assessmentId(at: tag), sessionId:self.viewModel.sessionId(at: tag) , parentId: sender.accessibilityIdentifier ?? "")
               if(nodes.count > 0){
                   //update  8-Oct-19
                   //            self.repeatingTimerControl = false
                   let finishedRootNodesCount = CoreDataUtils().getTakenChildNodesOfAssessmentParent(assessmentId:  sender.accessibilityIdentifier ?? "", sessionId:sender.accessibilityLabel ?? "", parentId: sender.accessibilityIdentifier ?? "").count
                   
                   nextViewController.viewModel = HealthSessionViewController.ViewModel(assessmentId: self.viewModel.assessmentId(at: tag),sessionId:self.viewModel.sessionId(at: tag), currentParentId: sender.accessibilityIdentifier ?? "", currentNode: nodes[0], questionName: sender.accessibilityHint!,completedQuestionNumber: finishedRootNodesCount, totalQuestions: (finishedRootNodesCount+nodes.count))
                   self.navigationController?.show(nextViewController, sender: self)
               }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "MeasurementSegue" {
               let vc = segue.destination as! MeasureRecordsViewController
                vc.viewType = viewType
              
           }else if segue.identifier == "ConnectedCareSegue" {
               let vc = segue.destination as! ConnectedCareViewController
               vc.viewType = viewType
               
           }
       }
    @IBAction func educationMaterialButton_Tapped(_ sender: Any) {
    }
    @IBAction func messageToNurseButton_Tapped(_ sender: Any) {
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//             let nextViewController = storyBoard.instantiateViewController(withIdentifier: "messageDetail") as! ChatViewController
//             self.navigationController?.show(nextViewController, sender: self)
        self.performSegue(withIdentifier: "ChatMessageSegue", sender: nil)
    }
    
    @IBAction func firstMeasurementButton_Tapped(_ sender: Any) {
        self.graphViewContainer.backgroundColor = UIColor(red: 18/255.0, green: 94/255.0, blue: 162/255.0, alpha: 1)
        self.firstlabel.backgroundColor =  UIColor(red: 18/255.0, green: 94/255.0, blue: 162/255.0, alpha: 1)
        self.secondLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
         self.thirdLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.fourthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.fifthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.setMeasurement(measurementTitleLabel: self.firstMeasurementTitleLabel, measurementRecordLabel: firstMeasurementRecordLabel)
    }
    
    @IBAction func secondMeasurementButton_Tapped(_ sender: Any) {
        self.graphViewContainer.backgroundColor = UIColor(red: 128/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
        self.firstlabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.secondLabel.backgroundColor = UIColor(red: 128/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
         self.thirdLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.fourthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.fifthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.setMeasurement(measurementTitleLabel: self.secondMeasurementTitleLabel, measurementRecordLabel: secondMeasurementRecordLabel)
    }
    
    @IBAction func thirdMeasurementButton_Tapped(_ sender: Any) {
        self.graphViewContainer.backgroundColor = UIColor(red: 255/255.0, green: 194/255.0, blue: 69/255.0, alpha: 1)
               
               self.firstlabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
               self.secondLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
                self.thirdLabel.backgroundColor = UIColor(red: 255/255.0, green: 194/255.0, blue: 69/255.0, alpha: 1)
               self.fourthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
               self.fifthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
               self.setMeasurement(measurementTitleLabel: self.thirdMeasurementTitleLabel, measurementRecordLabel: thirdMeasurementRecordLabel)
    }
    
    @IBAction func fourthMeasurementButton_Tapped(_ sender: Any) {
        self.graphViewContainer.backgroundColor = UIColor(red: 0/255.0, green: 186/255.0, blue: 155/255.0, alpha: 1)
        self.firstlabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
              self.secondLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
               self.thirdLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
              self.fourthLabel.backgroundColor = UIColor(red: 0/255.0, green: 186/255.0, blue: 155/255.0, alpha: 1)
              self.fifthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        self.setMeasurement(measurementTitleLabel: self.fourthMeasurementTitleLabel, measurementRecordLabel: fourthMeasurementRecordLabel)
    }
    
    @IBAction func fifthMeasurementButton_Tapped(_ sender: Any) {
        self.graphViewContainer.backgroundColor = UIColor(red: 121/255.0, green: 76/255.0, blue: 116/255.0, alpha: 1)
        self.firstlabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
              self.secondLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
               self.thirdLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
              self.fourthLabel.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
              self.fifthLabel.backgroundColor = UIColor(red: 121/255.0, green: 76/255.0, blue: 116/255.0, alpha: 1)
        
         self.setMeasurement(measurementTitleLabel: self.fifthMeasurementTitleLabel, measurementRecordLabel: fifthMeasurementRecordLabel)
    }
    
    @IBAction func manualMeasurementButton_Tapped(_ sender: UIButton) {
        self.setViewType()
        self.performSegue(withIdentifier: "MeasurementSegue", sender: nil)
    }
    
    
    @IBAction func viewDeatailsButton_Tapped(_ sender: UIButton) {
        self.setViewType()
        self.performSegue(withIdentifier: "ConnectedCareSegue", sender: nil)
    }
    
    
   
    
}
//Set Line Chart
extension DashboardViewController: LineChartDelegate {
    func didSelectYDataPoint(_ y: CGFloat, xValues: [Any]) {
        
    }
    
    func setLineChart(){
        
    //    let data2: [CGFloat] = [1, 3, 15,5, 20]
        let xLabels: [String] = ["","","","",""]
        //let yLabels:[CGFloat] = [0,10,20,30,40,50]
        
        
        self.lineChartVC.dots.color = UIColor(displayP3Red: 255/255, green: 0/255, blue: 144/255, alpha: 1)
        self.lineChartVC.singleColoredLine = true
        self.lineChartVC.x.axis.visible = false
        self.lineChartVC.y.axis.visible = false
        self.lineChartVC.x.grid.visible = false
        self.lineChartVC.y.grid.visible = false
        self.lineChartVC.x.labels.visible = false
        self.lineChartVC.y.labels.visible = false
        self.lineChartVC.x.labels.values = xLabels
       // self.lineChartVC.addLine(data2)
        self.lineChartVC.delegate = self
        
    }
    
    func didSelectDataPoint(_ x: CGFloat, yValues: [CGFloat]) {
        
    }
    func setGraphValueForDashbord(){
        
        
        var tableName = ""
        if(self.list[viewType] == "Blood pressure"){
            tableName = "BloodPressure"
        }else if (self.list[viewType] == "Blood glucose"){
            tableName = "BloodGlucose"
        }else{
            tableName = self.list[viewType]
        }
        let chartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: tableName)
        
        lineChartVC.clearAll()
        
        
        if(chartData != nil && chartData!.measurementArray.count > 0){
            mainMeasurementRecordLabel.text = "(\(String(chartData!.measurementArray.count)) Records)"
            if(viewType == 0 || viewType == 2){
              lineChartVC.singleColoredLine = false
                lineChartVC.x.labels.values = Array(0...chartData!.measurementArray.count).map  { String($0) }
                lineChartVC.addLine(chartData!.measurementArray)
                lineChartVC.addLine(chartData!.secondaryMeasurementArray)
            }else{
                lineChartVC.singleColoredLine = true
                lineChartVC.addLine(chartData!.measurementArray)
            }
            //lineChartVC.isHidden = false
        }else{
            mainMeasurementRecordLabel.text = "(0 Records)"
            //lineChartVC.isHidden = true
        }
       lineChartVC.setNeedsDisplay()
        
    }
    
    func setMeasurementCount(){
        self.firstMeasurementTitleLabel.text = "Blood Pressure"
         self.secondMeasurementTitleLabel.text = "Blood Glucose"
         self.thirdMeasurementTitleLabel.text = "Temperature"
         self.fourthMeasurementTitleLabel.text = "Weight"
          self.fifthMeasurementTitleLabel.text = "Spo2"
        
        let bloodPressurechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "BloodPressure")
         self.firstMeasurementRecordLabel.text =   "(\(String(bloodPressurechartData!.measurementArray.count)) Records)"
        let bloodGlucosechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "BloodGlucose")
        self.secondMeasurementRecordLabel.text =   "(\(String(bloodGlucosechartData!.measurementArray.count)) Records)"
        let temperaturechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "Temperature")
        self.thirdMeasurementRecordLabel.text =   "(\(String(temperaturechartData!.measurementArray.count)) Records)"
        let weightchartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "Weight")
        self.fourthMeasurementRecordLabel.text =   "(\(String(weightchartData!.measurementArray.count)) Records)"
        let pulsechartData : DashboardData? = coreDataUtils.getLatestMeasurement(tableName: "Pulse")
        self.fifthMeasurementRecordLabel.text =   "(\(String(pulsechartData!.measurementArray.count)) Records)"
        
        
    }
    
}
class SessionModel {
    
    private var assessments: [Assessment]
    private lazy var dateFormatter: DateFormatter = {
        let fmtr = DateFormatter()
        fmtr.dateFormat = "EEEE, MMM d"
        return fmtr
    }()
    
    var numberOfAssessments: Int {
        return assessments.count
    }
    
    private func assessment(at index: Int) -> Assessment {
        return assessments[index]
    }
    
    func text(at index: Int) -> String {
        return assessment(at: index).name ?? ""
    }
    
    func sessionId(at index: Int) -> String {
        return assessment(at: index).sessionId
    }
    
    func assessmentId(at index: Int) -> String {
        return assessment(at: index).assessmentId
    }
    
    func isSessionCompleted(at index:Int) -> Bool{
        return assessment(at: index).isCompleted
    }
    
    func isSessionSubmitted(at index:Int) -> Bool{
        return assessment(at: index).isSubmitted
    }
    
    func date(at index: Int) -> String{
        guard let startDate = self.assessment(at: index).startTime else{
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MMM/yyyy"
        let result = dateFormatter.string(from: startDate)
        return result
    }
    
    
    func startTime(at index: Int) -> String{
        guard let startDate = self.assessment(at: index).startTime else{
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let result = dateFormatter.string(from: startDate)
        return result
        
        
    }
    
    func startDateTime(at index: Int) -> Date{
        guard let startDate = self.assessment(at: index).startTime else{
            return Date()
        }
        return startDate
    }
    
    func endTime(at index: Int) -> String{
        guard let endDate = self.assessment(at: index).endTime else{
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let result = dateFormatter.string(from: endDate)
        return result
    }
    
    func endDateTime(at index: Int) -> Date{
        guard let endDate = self.assessment(at: index).endTime else{
            return Date()
        }
        return endDate
    }
    
    //        func dueDateText(at index: Int) -> String {
    //            let date = node(at: index).dueDate
    //            return dateFormatter.string(from: date)
    //        }
    //
    //        func editViewModel(at index: Int) -> EditToDoItemViewController.ViewModel {
    //            let toDo = self.node(at: index)
    //            let editViewModel = EditToDoItemViewController.ViewModel(node: node)
    //            return editViewModel
    //        }
    //
    //        func addViewModel() -> EditToDoItemViewController.ViewModel {
    //            let node = Node()
    //            nodes.append(node)
    //            let addViewModel = EditToDoItemViewController.ViewModel(node: node)
    //            return addViewModel
    //        }
    //
    @objc private func removeNode(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let assessment = userInfo[Notification.Name.deleteNodeNotification] as? Assessment,
            let index = assessments.index(of: assessment) else {
                return
        }
        assessments.remove(at: index)
    }
    
    // MARK: Life Cycle
    init(assessments: [Assessment] = []) {
        self.assessments = assessments
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeNode(_:)), name: .deleteNodeNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
