//
//  HealthSessionViewController.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 03/03/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import CoreLocation
import Eureka
import ViewRow
import CoreData
import AVKit
import AVFoundation

let brighterBlue = UIColor.init(red: 0.000, green: 0.467, blue: 0.694, alpha: 1.0)

class HealthSessionViewController: FormViewController,MeasureForAssessmentProtocol,WeightScaleMeasurementCaptureProtocol,BluetoothReadingViewControllerDelegate,NoninInputViewControllerDelegate{
    
    var measurementTaken:MeasurementData?
    //    var measurementView:AlertRow<String>?
    var measurementView:LabelRow?
//    var measurementInfoLabel:UILabel?
    var viewModel: ViewModel!
   
    let measurementList = ["BloodPressure", "BloodGlucose","Pulse","Temperature","Weight"]
    var freeTextValue = ""
    var numberValue = 0
    var dateValue : Date?
    
    var datecontrolLabel:UILabel!
    var totalSessionCount:Float?
    
    var measurementInfoLabel: UILabel = {
        let label = UILabel()
        label.textColor = Constants.COMMON_BLUE
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = UIColor(red: 235/255, green: 244/255, blue: 253/255, alpha: 1)
        label.minimumScaleFactor = 0.2
        label.font = UIFont(name: "Bogle-Bold", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var measurementValueLabel: UILabel = {
        let label = UILabel()
        label.textColor = Constants.COMMON_BLUE
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = UIColor(red: 235/255, green: 244/255, blue: 253/255, alpha: 1)
        label.text = "  --   "
        label.minimumScaleFactor = 0.2
        label.font = UIFont(name: "Bogle-Bold", size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d yyyy, h:mm a"
        return formatter
    }()
    
    // MARK: - Life Cycle
    convenience init(viewModel: ViewModel) {
        self.init()
        self.viewModel = viewModel
        initialize()
    }
    
    var viewType : Int = 0
    
    private func initialize() {
        view.backgroundColor = .white
    }
    
    func setResultOfMeasurement(valueSent: MeasurementData) {
        self.measurementTaken = valueSent
        //        self.measurementValueLabel.text = "\(valueSent.value!)"
        if valueSent.value != nil{
            self.measurementInfoLabel.text = "\(valueSent.value!)"
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Healthcare RPM"
    
        self.view.addBackgroundImage()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "healthSessionBgImg")!)
        
        self.createForm()
    }
    
    var isEntered = false
    var progressView = UIProgressView(progressViewStyle: .bar)
    
    //setting up the background view
    func setUpTheView()
    {
        isEntered = true
        self.totalSessionCount = self.viewModel.totalSessionsNo
        
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.text = self.viewModel.questionNameString
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont(name:"Bogle-Bold", size: 25)
        label.minimumScaleFactor = 0.3
        label.adjustsFontSizeToFitWidth = true
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 1
        
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        
        label.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 40).isActive = true
        label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
        backgroundView.translatesAutoresizingMaskIntoConstraints = false

        self.view.addSubview(backgroundView)
        backgroundView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
        backgroundView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        backgroundView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.65).isActive = true
        backgroundView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.9).isActive = true
        backgroundView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10).isActive = true
        progressView.translatesAutoresizingMaskIntoConstraints = false

        backgroundView.addSubview(self.progressView)
        backgroundView.bringSubviewToFront(self.progressView)
        progressView.leadingAnchor.constraint(equalTo: backgroundView.leadingAnchor, constant: 20).isActive = true
        progressView.trailingAnchor.constraint(equalTo: backgroundView.trailingAnchor, constant: -20).isActive = true
        progressView.heightAnchor.constraint(equalToConstant: 15.0).isActive = true
        progressView.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 40).isActive = true
        
        self.progressView.center = backgroundView.center
        self.progressView.progressTintColor = Constants.COMMON_BLUE
        self.progressView.backgroundColor = .white
        self.progressView.progressTintColor = Constants.COMMON_BLUE
        self.progressView.progressViewStyle = .bar
        
        self.progressView.layer.cornerRadius = 10
        self.progressView.clipsToBounds = true
        self.progressView.layer.sublayers![1].cornerRadius = 10
        self.progressView.subviews[1].clipsToBounds = true
        
        let imageView = UIImageView()
        let image = UIImage(named: "healthSessionBlueIcon")
        imageView.image = image
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)
        self.view.bringSubviewToFront(imageView)

        imageView.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 70.0).isActive = true
        imageView.centerYAnchor.constraint(equalTo: backgroundView.topAnchor).isActive = true
    }
    
    
    func createForm(){
        
        guard viewModel != nil else {
            if !isEntered
            {
                setUpTheView()
            }
            
            self.view.addSubview(self.tableView)
            self.tableView.backgroundColor = UIColor.clear
            
            
            progressView.setProgress((10 / 10), animated: false)
            
            let ssName:LabelRow =  LabelRow().cellSetup {
                cell, row in

                let bgView = UIView()
                bgView.translatesAutoresizingMaskIntoConstraints = false
                cell.addSubview(bgView)
                
                bgView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
                bgView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                bgView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                bgView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                
                let imageView = UIImageView()
                let image = UIImage(named: "thankyou")
                imageView.image = image
                imageView.contentMode = .scaleAspectFit
                
                imageView.translatesAutoresizingMaskIntoConstraints = false
                
                bgView.addSubview(imageView)
                
                imageView.topAnchor.constraint(equalTo: bgView.topAnchor).isActive = true
                imageView.bottomAnchor.constraint(equalTo: bgView.bottomAnchor).isActive = true
                imageView.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
                
                let textView = UITextView()
                let attributedText = NSMutableAttributedString(string: "Your Health Session is completed! Thank You.", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 18)!,NSAttributedString.Key.foregroundColor: Constants.COMMON_BLUE])
                textView.attributedText = attributedText
                textView.textAlignment = .center
                textView.backgroundColor = .white
                
                textView.translatesAutoresizingMaskIntoConstraints = false
                
                cell.addSubview(textView)
                
                textView.topAnchor.constraint(equalTo: bgView.bottomAnchor).isActive = true
                textView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                textView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                textView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                
                cell.height = ({
                    
                    let textViewHeight = textView.textInputView.frame.height + 100
                    return textViewHeight
                })
                
            }
            
            form +++ ssName
            form +++ Section() <<< ButtonRow() {
                $0.cellSetup({ (cell, row) in
                    let button = self.setUpNextButton()
                    
                    cell.addSubview(button)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                    button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                    button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                    cell.backgroundColor = .clear
                    
                    cell.height = ({return 50})
                })
                }.onCellSelection {  cell, row in
                    //save the current node and show the next node
                    self.form.removeAll()
                    self.checkForRemainingSessions()
            }
            
            return
        }
        
        
        guard !viewModel!.currentNodeId.isEmpty, !viewModel!.currentNodeType.isEmpty else{
            return
        }
        if !isEntered
        {
            setUpTheView()
        }
        
        self.view.addSubview(self.tableView)
        self.tableView.backgroundColor = UIColor.clear
        
        progressView.setProgress((viewModel.completedSessionsNo / (self.totalSessionCount ?? viewModel.totalSessionsNo)), animated: false)
        
        switch(viewModel.currentNodeType.lowercased()) {
        case "radio":
            
            let ssRadio: SelectableSection<ImageCheckRow<Node>> = SelectableSection<ImageCheckRow<Node>>() { section in
                section.header = HeaderFooterView(title: self.viewModel.text ?? "")
            }
            for option in self.viewModel.nodeOptions! {
                ssRadio <<< ImageCheckRow<Node>(option.text ?? ""){ lrow in
                    lrow.title = option.text ?? ""
                    lrow.selectableValue = option
                    lrow.value = nil
                    lrow.cellSetup({ (cell, row) in
                        cell.height = ({return 50})
                        cell.backgroundColor = .white
                    })
                    }.cellUpdate {
                        cell, row in
                        cell.textLabel?.textColor = Constants.COMMON_BLUE
                        cell.backgroundColor = UIColor.white
                }
            }
            
            //create form
            form +++ ssRadio
            form +++ Section()
                
                <<< ButtonRow() {
                    
                    $0.cellSetup({ (cell, row) in

                        let button = self.setUpNextButton()
                        
                        cell.addSubview(button)
                        
                        button.translatesAutoresizingMaskIntoConstraints = false
                        
                        button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                        button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                        button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                        button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                        cell.backgroundColor = .clear
                        
                        cell.height = ({return 30})
                    })
                    }
                    .onCellSelection {  cell, row in
                        if ssRadio.selectedRow() != nil{
                            //save the current node and show the next node
                            self.form.removeAll()
                            if let selectedNode:Node = ssRadio.selectedRow()!.baseValue as? Node{
                                //save the value of the current node
                                self.viewModel.saveNodeValueAnswer(valueGiven: selectedNode.text ?? "", answerIdGiven: selectedNode.id)
                                //note : parent is the option element nd not the radio element
                                //other cases parentid = self.viewModel.currentNode.id
                                self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: selectedNode.id)
                                if( self.viewModel != nil ){
                                    self.createForm()
                                }
                                else{
                                    self.onSessionCompletionAlert()
                                }
                            }
                            
                        }
                        else{
                            //alert user to make a selection
                            Toast.show(message: "Please make a selection", controller: self)
                        }
                        
            }
            
        case "check":
            
            let ssCheck = SelectableSection<ImageCheckRow<Node>>(self.viewModel.text ?? "", selectionType: .multipleSelection)
            for option in self.viewModel.nodeOptions! {
                ssCheck <<< ImageCheckRow<Node>(option.text){ lrow in
                    lrow.title = option.text
                    lrow.selectableValue = option
                    lrow.value = nil
                    }.cellSetup { cell, _ in
                        cell.trueImage = UIImage(named: "selectedRectangle")!
                        cell.falseImage = UIImage(named: "unselectedRectangle")!
                        cell.accessoryType = .checkmark
                }
            }
            
            form +++ ssCheck
                
                //            form +++ Section()
                <<< ButtonRow() {
                    $0.cellSetup({ (cell, row) in
                        
                        let button = self.setUpNextButton()
                        
                        cell.addSubview(button)
                        
                        button.translatesAutoresizingMaskIntoConstraints = false
                        
                        button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                        button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                        button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                        button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                        cell.backgroundColor = .clear
                        
                        cell.height = ({return 50})
                    })
                    }
                    .onCellSelection {  cell, row in
                        if(ssCheck.selectedRows().count>0){
                            //                    //save the current node and show the next node
                            self.form.removeAll()
                            let selectedNodes:[Node] = ssCheck.selectedRows().map({$0.baseValue}) as! [Node]
                            //                    //save the value of the current node
                            var valString = [String]()
                            for selection in selectedNodes{
                                valString.append(selection.value ?? "")
                            }
                            self.viewModel.saveNodeValueAnswer(valueGiven: valString.description, answerIdGiven: selectedNodes[0].id)
                            //                    //note : parent is the option element nd not the radio element
                            //                    //other cases parentid = self.viewModel.currentNode.id
                            self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID:selectedNodes[0].id)
                            if( self.viewModel != nil ){
                                self.createForm()
                            }
                            else{
                                self.onSessionCompletionAlert()
                            }
                        }
                        else{
                            Toast.show(message: "Please make selection(s)", controller: self)
                        }
                        
            }
            
        case "measurement":
            
            let btnTag = self.viewModel.measuremenType
            
            
            self.viewType = self.measurementList.firstIndex(of: btnTag!) ?? 0
            
            if (btnTag?.contains("Heart"))! {
                self.viewType = 2
            }
            if (btnTag?.lowercased().elementsEqual("spo2"))!{
                self.viewType = 2
            }
            
            form +++ Section(self.viewModel.text ?? "")
                
                <<< ButtonRow() {
                    $0.cellSetup({ (cell, row) in
                       
                        self.measurementInfoLabel.text = "  --  "
                        cell.addSubview(self.measurementInfoLabel)
                        
                        self.measurementInfoLabel.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                        self.measurementInfoLabel.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                        self.measurementInfoLabel.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                        self.measurementInfoLabel.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                        
                        
                        let button = self.setUpMeasurementButtons(titleColor: UIColor.white, borderColor: UIColor.white, bgColor: Constants.COMMON_BLUE, title: "Manual Entry")
                        cell.addSubview(button)
                        button.titleLabel?.numberOfLines = 0
                        button.titleLabel?.adjustsFontSizeToFitWidth = true
                        button.titleLabel?.minimumScaleFactor = 0.5
                        button.titleLabel?.lineBreakMode = .byWordWrapping
                        button.topAnchor.constraint(equalTo: cell.topAnchor, constant: 10).isActive = true
                        button.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: -10).isActive = true
                        button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.4).isActive = true
                        button.trailingAnchor.constraint(equalTo: cell.trailingAnchor, constant: -10).isActive = true
                        
                        cell.backgroundColor = UIColor(red: 235/255, green: 244/255, blue: 253/255, alpha: 1)
                        cell.selectionStyle = .none

                       
                        cell.height = ({return 80})
                        
                        
                    })
                    }.onCellHighlightChanged{(cell,row) in
                        cell.selectionStyle = .none
                    }
                    .onCellSelection {  cell, row in
                        let AndBPstoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let measureVC = AndBPstoryboard.instantiateViewController(withIdentifier: "measureData") as! MeasureRecordsViewController
                        cell.selectionStyle = .none
                        measureVC.viewType = self.viewType
                        measureVC.assessmentDelegate = self
                        
                        self.show(measureVC,sender: self)
                }
                
                <<< ButtonRow() {
                    $0.cellSetup({ (cell, row) in
                        
                        
                        cell.addSubview(self.measurementValueLabel)
                        
                        self.measurementValueLabel.topAnchor.constraint(equalTo: cell.topAnchor, constant: 10).isActive = true
                        self.measurementValueLabel.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                        self.measurementValueLabel.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.4).isActive = true
                        self.measurementValueLabel.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                        
                        let image = UIImage(named: "device")
                        let imageView = UIImageView()
                        imageView.image = image
                        imageView.contentMode = .scaleAspectFit
                        imageView.translatesAutoresizingMaskIntoConstraints = false
                        cell.addSubview(imageView)
                        
                        imageView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                        imageView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                        imageView.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.1).isActive = true
                        imageView.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                        
                        
                        let button = self.setUpMeasurementButtons(titleColor: Constants.COMMON_BLUE, borderColor: Constants.COMMON_BLUE, bgColor: UIColor.white, title: "Use Device")
//
                        cell.addSubview(button)
                        button.titleLabel?.numberOfLines = 0
                        button.titleLabel?.adjustsFontSizeToFitWidth = true
                        button.titleLabel?.minimumScaleFactor = 0.5
                        button.titleLabel?.lineBreakMode = .byWordWrapping
                        button.topAnchor.constraint(equalTo: cell.topAnchor, constant: 10).isActive = true
                        button.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: -10).isActive = true
                        button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.4).isActive = true
                        button.trailingAnchor.constraint(equalTo: cell.trailingAnchor, constant: -10).isActive = true
                        
                        cell.selectionStyle = .none
                        
                        cell.backgroundColor = UIColor(red: 235/255, green: 244/255, blue: 253/255, alpha: 1)
                        cell.height = ({return 80})
                        
                    })
                    }.onCellHighlightChanged{(cell,row) in
                        cell.selectionStyle = .none
                    }
                    .onCellSelection {  cell, row in
                        cell.selectionStyle = .none
                        if (self.viewType == 0){
                            //BP
                            let AndBPstoryboard = UIStoryboard(name: "BluetoothReading", bundle: nil)
                            let nextVC = AndBPstoryboard.instantiateViewController(withIdentifier: "AnDBPStoryboard") as! BluetoothReadingViewController
                            self.navigationController?.show(nextVC, sender: self)
                            nextVC.delegate = self
                            nextVC.callingController = self
                        }
                            
                        else if (self.viewType == 2){
                            //pulseOx
                            let NoninStoryboard = UIStoryboard(name: "NoninPulseOxStoryboard" , bundle: nil)
                            let nextVC = NoninStoryboard.instantiateViewController(withIdentifier: "NoninInputViewController") as! NoninInputViewController
                            self.navigationController?.show(nextVC, sender: self)
                            nextVC.delegate = self
                            nextVC.callingController = self
                        }
                        else if(self.viewType == 4){
                            //weight
                            let AnDStoryboard = UIStoryboard(name: "andWeightScaleStoryboard" , bundle: nil)
                            let nextVC = AnDStoryboard.instantiateViewController(withIdentifier: "A&DWeightScale") as! AnDWeightScaleViewController
                            self.navigationController?.show(nextVC, sender: self)
                            nextVC.delegate = self
                             nextVC.callingController = self
                        }
            }
            
            form +++ Section()
                <<< ButtonRow() {
                    //$0.title = "Submit"
                    $0.cellSetup({ (cell, row) in
                        
                        let button = self.setUpNextButton()
                        
                        cell.addSubview(button)
                        
                        button.translatesAutoresizingMaskIntoConstraints = false
                        
                        button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                        button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                        button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                        button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                        cell.backgroundColor = .clear
                        
                        cell.height = ({return 50})
                    })
                    }.onCellSelection {  cell, row in
                        if(self.measurementTaken != nil){
                            if(!self.measurementTaken!.value!.isEmpty){
                                //save the current node and show the next node
                                self.form.removeAll()
                                //to be changed
                                
//                                self.viewModel.saveNodeValueAnswer(valueGiven: self.measurementTaken?.value! ?? "", answerIdGiven: UUID().uuidString)
                                self.viewModel.saveMeasurementNodeValueAnswer(valueGiven: self.measurementTaken?.value! ?? "", answerIdGiven: UUID().uuidString,measurementTypeId: self.measurementTaken?.measurementTypeId! ?? "",measurementValueTypeId:  self.measurementTaken?.measurementValueType ?? "",measurementValue: self.measurementTaken?.value ?? "" )

                                self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                                if( self.viewModel != nil ){
                                    self.measurementTaken = nil
                                    self.createForm()
                                }
                                else{
                                    self.onSessionCompletionAlert()
                                }
                                print("")
                            }
                        }
                        else{
                            Toast.show(message: "Please take/enter the measurement", controller: self)
                        }
                        
            }
        case "alert":
            self.form.removeAll()
            self.viewModel.saveNodeValueAnswer(valueGiven: "", answerIdGiven: "")
            self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
            if( self.viewModel != nil ){
                self.createForm()
            }
            else{
                self.onSessionCompletionAlert()
            }
            print("")
            
        case "freetext":
            let ssName:LabelRow =  LabelRow().cellSetup {
                cell, row in
                
                let textView = UITextView()
                let attributedText = NSMutableAttributedString(string: self.viewModel!.text!, attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 18)!,NSAttributedString.Key.foregroundColor: UIColor.white])
                textView.attributedText = attributedText
                textView.textAlignment = .center
                textView.backgroundColor = Constants.COMMON_BLUE
                
                textView.translatesAutoresizingMaskIntoConstraints = false
                
                cell.addSubview(textView)
                
                textView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                textView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                textView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                textView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                
                cell.height = ({
                    
                    let textViewHeight = textView.textInputView.frame.height
                    return textViewHeight
                    
                })
                
            }
            form +++ ssName
                
                <<< TextRow().cellSetup{
                    cell, row in
                    
                    cell.textField.translatesAutoresizingMaskIntoConstraints = false
                    cell.textField.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                    cell.textField.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    cell.textField.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                    cell.textField.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                    cell.textField.backgroundColor = UIColor.init(red: 234/255, green: 242/255, blue: 252/255, alpha: 1)
                    cell.textField.textColor = Constants.COMMON_BLUE
                    row.placeholder = "Please enter text here"
                    cell.height = ({
                        return 100
                    })
                    row.onChange({ (text) in
                        self.freeTextValue = text.value ?? ""
                    })
            }
            
            form +++ Section() <<< ButtonRow() {
                $0.cellSetup({ (cell, row) in
                    
                    let button = self.setUpNextButton()
                    
                    cell.addSubview(button)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                    button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                    button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                    cell.backgroundColor = .clear
                    
                    cell.height = ({return 50})
                })
                }.onCellSelection {  cell, row in
                    if(self.freeTextValue != ""){
                        //save the current node and show the next node
                        self.form.removeAll()
                        //to be changed
                        self.viewModel.saveNodeValueAnswer(valueGiven: self.freeTextValue, answerIdGiven: UUID().uuidString)
                        
                        
                        self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                        if( self.viewModel != nil ){
                            self.createForm()
                        }
                        else{
                            self.onSessionCompletionAlert()
                        }
                        print("")
                    } else{
                        Toast.show(message: "Please type your response", controller: self)
                    }
            }
            
        case "number":
            let ssName:LabelRow =  LabelRow().cellSetup {
                cell, row in
                
                let textView = UITextView()
                let attributedText = NSMutableAttributedString(string: self.viewModel!.text!, attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 22)!,NSAttributedString.Key.foregroundColor: UIColor.white])
                textView.attributedText = attributedText
                textView.textAlignment = .center
                textView.backgroundColor = Constants.COMMON_BLUE
                
                textView.translatesAutoresizingMaskIntoConstraints = false
                
                cell.addSubview(textView)
                
                textView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                textView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                textView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                textView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
        
                cell.height = ({
                    
                    let textViewHeight = textView.textInputView.frame.height
                    return textViewHeight
                    
                })
            }
            form +++ ssName
                
                <<< IntRow().cellSetup{
                    cell, row in
                    row.placeholder = "Please enter number here"
                    
                    row.onChange({ (number) in
                        self.numberValue = number.value ?? 0
                    })
                    cell.height = ({return 60})
            }
            
            form +++ Section() <<< ButtonRow() {
                // $0.title = "Submit"
                $0.cellSetup({ (cell, row) in

                    let button = self.setUpNextButton()
                    
                    cell.addSubview(button)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                    button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                    button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                    cell.backgroundColor = .clear
                    
                    cell.height = ({return 50})
                })
                }.onCellSelection {  cell, row in
                    
                    //save the current node and show the next node
                    self.form.removeAll()
                    //to be changed
                    self.viewModel.saveNodeValueAnswer(valueGiven: String(self.numberValue), answerIdGiven: UUID().uuidString)
                    
                    self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                    if( self.viewModel != nil ){
                        self.createForm()
                    }
                    else{
                        self.onSessionCompletionAlert()
                    }
            }
            
        case "date":
            
            let ssName:LabelRow =  LabelRow().cellSetup {
                cell, row in
                
                let textView = UITextView()
                let attributedText = NSMutableAttributedString(string: self.viewModel!.text!, attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 18)!,NSAttributedString.Key.foregroundColor: UIColor.white])
                textView.attributedText = attributedText
                textView.textAlignment = .center
                textView.backgroundColor = Constants.COMMON_BLUE
                
                textView.translatesAutoresizingMaskIntoConstraints = false
                
                cell.addSubview(textView)
                
                textView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                textView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                textView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                textView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                
                cell.height = ({
                    
                    let textViewHeight = textView.textInputView.frame.height
                    return textViewHeight
                    
                })
            }
            form +++ ssName
                
                <<< DateRow().cellSetup{
                    cell, row in
                    cell.backgroundColor = UIColor.white
//                    row.title = "Please click here to select a date"
                    row.value = self.dateValue
                    self.datecontrolLabel = UILabel.init(frame: CGRect.init(x: 10, y: -20, width: 250, height: 120))
                    self.datecontrolLabel.text = self.viewModel.text
                    self.datecontrolLabel.textColor = Constants.COMMON_BLUE
                    self.datecontrolLabel.textAlignment = .center
                    self.datecontrolLabel.numberOfLines = 0
                    self.datecontrolLabel.adjustsFontSizeToFitWidth = true
                    self.datecontrolLabel.lineBreakMode = .byWordWrapping
                    self.datecontrolLabel.text = "Please click here to select a date"
                    self.datecontrolLabel.font = UIFont(name:"Bogle-Bold", size: 18)
                    cell.addSubview(self.datecontrolLabel)
                    cell.height = ({return 100})
                    row.onChange({ (date) in
                        self.dateValue = row.value

                    })
                    
                    }.onCellSelection({ (cell, row) in
                    
                        self.datecontrolLabel.text = ""
                        
                    })
            
            form +++ Section() <<< ButtonRow() {
                $0.cellSetup({ (cell, row) in

                    let button = self.setUpNextButton()
                    
                    cell.addSubview(button)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                    button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                    button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                    cell.backgroundColor = .clear
                    
                    cell.height = ({return 50})
                })
                }.onCellSelection {  cell, row in
                    //save the current node and show the next node
                    if self.dateValue == nil{
                        self.datecontrolLabel.text = "Please click here to select a date"
                        Toast.show(message: "Please select a date", controller: self)
                    }else{
                        self.form.removeAll()
                        //to be changed
                        let formatter = DateFormatter()
                        // initially set the format based on your datepicker date / server String
                        formatter.dateFormat = "MM-dd-yyyy"
                        // convert your string to date
                        let valueDateString = formatter.string(from: self.dateValue ?? Date())
                        
                        self.viewModel.saveNodeValueAnswer(valueGiven: valueDateString, answerIdGiven: UUID().uuidString)
                        
                        self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                        if( self.viewModel != nil ){
                            self.createForm()
                        }
                        else{
                            self.onSessionCompletionAlert()
                        }
                    }
                    print("")
                    
            }
            
        case "media":
        
        
        let ssName:LabelRow =  LabelRow().cellSetup {
            cell, row in
            cell.backgroundColor = UIColor.clear

        }
        form +++ ssName
        
        <<< ViewRow<UIImageView>()
        .cellSetup { (cell, row) in
            //  Construct the view for the cell
            cell.view = UIImageView()

            cell.viewRightMargin = 0.0
            cell.viewLeftMargin = 0.0
            cell.viewTopMargin = 0.0
            cell.viewBottomMargin = 0.0

            cell.height = { return CGFloat(200) }
            
            cell.contentView.addSubview(cell.view!)
            let imgURL = URL(string: self.viewModel.text!)!
            //                print("image URL=="imgURL);
            let session = URLSession(configuration: .default)
                            
            let downloadPicTask = session.dataTask(with: imgURL) { (data, response, error) in
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                    
                } else {
                    if let res = response as? HTTPURLResponse {
                        print("Downloaded cat picture with response code \(res.statusCode)")
                        if let imageData = data {
                            DispatchQueue.main.async {
                                let image = UIImage(data: imageData)
                                cell.view!.image = image
                                
                                cell.view!.setNeedsLayout()
                                cell.view!.setNeedsDisplay()
                                cell.setNeedsLayout()
                                cell.setNeedsDisplay()
                                print("is it displayed")
                            }
                        } else {
                            print("Couldn't get image: Image is nil")
                            
                        }
                        
                    } else {
                        print("Couldn't get response code for some reason")
                    }
                }
            }
            downloadPicTask.resume()
        }
        
        
        
        
        form +++ Section() <<< ButtonRow() {
            $0.cellSetup({ (cell, row) in
                
                let button = self.setUpNextButton()
                
                cell.addSubview(button)
                
                button.translatesAutoresizingMaskIntoConstraints = false
                
                button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                cell.backgroundColor = .clear
                
                cell.height = ({return 50})
            })
            }.onCellSelection {  cell, row in
                //save the current node and show the next node
                self.form.removeAll()
                //to be changed
                self.viewModel.saveNodeValueAnswer(valueGiven: "", answerIdGiven: UUID().uuidString)
                
                self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                if( self.viewModel != nil ){
                    self.createForm()
                }
                else{
                    self.onSessionCompletionAlert()
                    
                    //self.navigationController?.popViewController(animated: true)
                }
                print("")
                
        }
            
            case "video":
            
            
            let ssName:LabelRow =  LabelRow().cellSetup {
                cell, row in
                cell.backgroundColor = UIColor.clear
                //media played
                print("Video----\(self.viewModel.text!)")
                let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
                let player = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
                cell.addSubview(playerViewController.view)
                cell.height = ({return 400})
            }
            form +++ ssName
            
            
            form +++ Section() <<< ButtonRow() {
                $0.cellSetup({ (cell, row) in
                    
                    let button = self.setUpNextButton()
                    
                    cell.addSubview(button)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                    button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                    button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                    cell.backgroundColor = .clear
                    
                    cell.height = ({return 50})
                })
                }.onCellSelection {  cell, row in
                    //save the current node and show the next node
                    self.form.removeAll()
                    //to be changed
                    self.viewModel.saveNodeValueAnswer(valueGiven: "", answerIdGiven: UUID().uuidString)
                    
                    self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                    if( self.viewModel != nil ){
                        self.createForm()
                    }
                    else{
                        self.onSessionCompletionAlert()
                        
                        //self.navigationController?.popViewController(animated: true)
                    }
                    print("")
                    
            }
        case "instructions","option":
            
            let ssName:LabelRow =  LabelRow().cellSetup {
                cell, row in
                
                if self.viewModel.currentNodeType.lowercased() == "instructions"
                {
                    let bgView = UIView()
                    
                    bgView.translatesAutoresizingMaskIntoConstraints = false
                    cell.addSubview(bgView)
                    
                    bgView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
                    bgView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                    bgView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                    bgView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                    
                    let imageView = UIImageView()
                    let image = UIImage(named: "important")
                    imageView.image = image
                    imageView.contentMode = .scaleAspectFit
                    
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    
                    bgView.addSubview(imageView)
                    
                    imageView.topAnchor.constraint(equalTo: bgView.topAnchor).isActive = true
                    imageView.bottomAnchor.constraint(equalTo: bgView.bottomAnchor).isActive = true
                    imageView.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
                    
                    let textView = UITextView()
                    let attributedText = NSMutableAttributedString(string: self.viewModel!.text!, attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 18)!,NSAttributedString.Key.foregroundColor: Constants.COMMON_BLUE])
                    textView.attributedText = attributedText
                    textView.textAlignment = .center
                    textView.backgroundColor = .white
                    
                    textView.translatesAutoresizingMaskIntoConstraints = false
                    
                    cell.addSubview(textView)
                    
                    textView.topAnchor.constraint(equalTo: bgView.bottomAnchor).isActive = true
                    textView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    textView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                    textView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                    
                    cell.height = ({
                        
                        let textViewHeight = textView.textInputView.frame.height + 100
                        return textViewHeight
                    })
                }
                else
                {
                    
                    let textView = UITextView()
                    let attributedText = NSMutableAttributedString(string: self.viewModel!.text!, attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 18)!,NSAttributedString.Key.foregroundColor: Constants.COMMON_BLUE])
                    textView.attributedText = attributedText
                    textView.textAlignment = .center
                    textView.backgroundColor = .white
                    
                    textView.translatesAutoresizingMaskIntoConstraints = false
                    
                    cell.addSubview(textView)
                    
                    textView.topAnchor.constraint(equalTo: cell.topAnchor).isActive = true
                    textView.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true
                    textView.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
                    textView.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
                    
                    cell.height = ({
                        
                        let textViewHeight = textView.textInputView.frame.height
                        return textViewHeight
                    })
                }
                
            }
            
            form +++ ssName
            form +++ Section() <<< ButtonRow() {
                $0.cellSetup({ (cell, row) in
                    
                    let button = self.setUpNextButton()
                    
                    cell.addSubview(button)
                    
                    button.translatesAutoresizingMaskIntoConstraints = false
                    
                    button.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
                    button.topAnchor.constraint(equalTo: cell.topAnchor).isActive = false
                    button.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = false
                    button.widthAnchor.constraint(equalTo: cell.widthAnchor, multiplier: 0.5).isActive = true
                    cell.backgroundColor = .clear

                    cell.height = ({return 50})
                })
                }.onCellSelection {  cell, row in
                    //save the current node and show the next node
                    self.form.removeAll()
                    //to be changed
                    self.viewModel.saveNodeValueAnswer(valueGiven: "", answerIdGiven: UUID().uuidString)
                    
                    self.viewModel = self.viewModel.checkForNextQuestionAvailability(parentID: self.viewModel.currentNodeId)
                    if( self.viewModel != nil ){
                        self.createForm()
                    }
                    else{
                        self.onSessionCompletionAlert()
                    }
                    print("")
                    
            }
            
        default:
            print("DEFAULT \(viewModel.currentNodeType)")
            
        }
        
        
    }
    
    func setUpMeasurementButtons(titleColor:UIColor, borderColor:UIColor, bgColor: UIColor, title:String) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.layer.borderColor = borderColor.cgColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 20
        button.layer.masksToBounds = true
        button.clipsToBounds = true
        button.titleLabel?.font = UIFont.init(name: "Bogle-Bold", size: 18)
        button.backgroundColor = bgColor
        button.isUserInteractionEnabled = false
        button.setTitleColor(titleColor, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
    
    func setUpNextButton() -> UIButton
    {
        let button = UIButton()
        let attributedText = NSMutableAttributedString(string: "Next", attributes: [NSAttributedString.Key.font: UIFont(name: "Bogle-Bold", size: 18)!,NSAttributedString.Key.foregroundColor: UIColor.white])
        button.setAttributedTitle(attributedText, for: .normal)
        
        button.layer.cornerRadius = 20
        button.layer.masksToBounds = true
        button.clipsToBounds = true
        
        button.backgroundColor = Constants.COMMON_BLUE
        
        button.isUserInteractionEnabled = false
        return button
    }
    
    func checkForRemainingSessions()
    {
        // let pendingSessions = self.viewModel.numberOfAssessments ?? 0
        self.navigationController?.popViewController(animated: true)
    }
    
    var isSessionComplete = false
    func onSessionCompletionAlert()
    {
        isSessionComplete = true
        createForm()
    }
    
    
    func unwindFromMeasurementVC(_ sender:UIStoryboardSegue){
        if sender.source is MeasureRecordsViewController {
            if let senderVC = sender.source as? MeasureRecordsViewController{
                measurementTaken  = senderVC.measurement
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "getMeasureData" {
            let nextViewController = segue.destination as! MeasureRecordsViewController
            nextViewController.assessmentDelegate = self
            let btnTag = (sender as! AlertRow<String>).tag
            let viewIndex = measurementList.firstIndex(of: btnTag!)
            guard viewIndex != nil else{
                return
            }
            
            nextViewController.viewType = viewIndex!
        }
    }
    
    @objc func multipleSelectorDone(_ item:UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    //custom table header for eureka forms
    func tableView(_: UITableView, willDisplayHeaderView view: UIView, forSection: Int) {
        
        
        if let view = view as? UITableViewHeaderFooterView {
            
            view.textLabel?.font = UIFont(name: "Bogle-Bold", size: 15)
            view.textLabel?.textColor = UIColor.white
            view.backgroundView!.backgroundColor = Constants.COMMON_BLUE
            view.backgroundView!.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 60)
            view.layer.borderWidth = 2.0
            view.textLabel?.textAlignment = .center
            view.textLabel?.numberOfLines = 0
            view.textLabel?.adjustsFontSizeToFitWidth = true
            view.textLabel?.lineBreakMode = .byWordWrapping
            view.textLabel?.minimumScaleFactor = 0.3
            view.layer.borderColor = Constants.COMMON_BLUE.cgColor
        }
        
    }
    
    func bluetoothDevicedidcapturemeasurment(_ measurementType: String!, valuedict valuesdictionary: [AnyHashable : Any]) {
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "BloodPressure";
        request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeasString()
        
        let systolicVal = valuesdictionary["systolic"] as! Int
        let diaSystolicVal = valuesdictionary["diastolic"] as! Int
        let heartRateValue = valuesdictionary["pulse"] as! Int
        measurement.measurementValueType = "2/3"
        measurement.value =  String(systolicVal) + "/" + String(diaSystolicVal) + "/" + String(heartRateValue)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
        }
        
        measurement.value = "Blood Pressure: " +  String(systolicVal) + "/" + String(diaSystolicVal)  + " mmHg " + " Heart Rate: " + String(heartRateValue)
        
        self.setResultOfMeasurement(valueSent: measurement)
        
    }
    
    func noninInputController(_ controller: NoninInputViewController!, didCompleteWithMeasurement measurements: [AnyHashable : Any]!) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Pulse";
        
        let table = "HeartRate"
        request.predicate = NSPredicate(format: "name = %@", table)// measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        
        measurement.measurementDateTime = getCurrentDateTimeasString()
        
        let pulse = measurements["pulse"] as! Int
        let spo2 = measurements["spo2"] as! Int
        measurement.value =  String(pulse) + "/" + String(spo2)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        
        measurement.value = String(pulse)
        
        NetworkManager().uploadDataPulse(measurementData: measurement ){ (response) -> () in
        }
        
        measurement.value = String(spo2)
        measurement.isManual = false
        measurement.measurementTypeId = MeasurementIdentifier.sharedInteractor.spo2ID
        
        NetworkManager().uploadDataSPO2(measurementData: measurement, completion: { (response) -> () in
        })
        
         measurement.value = "Heart Rate: " +  String(pulse)  + " bpm " + "SPO2: " +  String(spo2) + "%"
        
        self.setResultOfMeasurement(valueSent: measurement)
               
    }
    
    
    func weightScaleDidCaptureMeasurement(_ value:Double, dateCaptured date: Date) {
        
        var measurement: MeasurementData! = MeasurementData()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        
        measurement.measurementName = "Weight";
        request.predicate = NSPredicate(format: "name = %@",  measurement.measurementName!)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = false
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
            }
        }catch {
            print("Failed")
        }
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        measurement.measurementDateTime = getCurrentDateTimeasString()
        
        let weight = value
        measurement.value =  String(weight)
        CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
        }
        if measurement.measurementUnit != nil{
        measurement.value = "Weight: " +  String(weight)  + "\(measurement.measurementUnit!)"
        }else{
             measurement.value = "Weight: " +  String(weight)
        }
        
        self.setResultOfMeasurement(valueSent: measurement)
        
    }
    
    func getCurrentDateTimeasString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        return str
    }
    
    
}

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}
