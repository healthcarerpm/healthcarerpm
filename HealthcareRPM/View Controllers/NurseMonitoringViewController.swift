//
//  NurseMonitoringViewController.swift
//  HealthcareRPM
//
//  Created by Admin on 1/27/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class NurseMonitoringViewController: ViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionView: UITextView!
    
    @IBOutlet weak var subLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var checkBox: UIButton!
    
    @IBOutlet weak var tAndCLabel: UILabel!
    
    @IBOutlet weak var checkOutBtn: UIButton!
    var selectedPatientCondition : [RPMPatientCondition] = []
    var conditionacceptance = false
    
    var keyServiceArray = ["Be monitored with doctor approved and proven care plans which are daily health questions to manage your health.","Remote interaction with a qualified nurse via messages.","Educational materials","Daily health measurements and trending of it which allows your nurse, doctor or healthcare provider to take informed and better decisions.","Peace of mind"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = #imageLiteral(resourceName: "healthSessionBgImg")
        self.view.insertSubview(backgroundImage, at: 0)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 300
        
        
        self.descriptionView.text = "Nurse monitoring will provide you with the comfort and care needed during the journey of your pregnancy."
        
        let lineText = NSMutableAttributedString(string: "Terms and Conditions", attributes:
             [.underlineStyle: NSUnderlineStyle.single.rawValue])
        
           self.tAndCLabel.attributedText = lineText
          let tap = UITapGestureRecognizer(target: self, action: #selector(handleTermTapped))
          tAndCLabel.addGestureRecognizer(tap)
          tAndCLabel.isUserInteractionEnabled = true
//        self.tAndCLabel.text = "I have reviewd Terms & Conditions"
        self.titleLabel.text = "Why Nurse Monitoring?"
        
        self.subLabel.text = "We provide key services such as:"
        
        self.tableView.register(UINib(nibName: "NurseTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "nurseCell")
        self.tableView.separatorStyle = .none
        

    }
    
    @IBAction func clickcheckBx(_ sender: UIButton) {
        if sender.isSelected == true
        {
            self.checkBox.setImage(#imageLiteral(resourceName: "unchecked_blue"), for: UIControl.State.normal)
            conditionacceptance = false
        }
        else
        {
            self.checkBox.setImage(#imageLiteral(resourceName: "checked_blue"), for: UIControl.State.normal)
            conditionacceptance = true

        }
        sender.isSelected = !sender.isSelected
    }
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }

    
    @IBAction func checkOutAction(_ sender: UIButton) {
        if conditionacceptance == false{
            self.showAlert(title: "Info", message: "Please accept the Terms and Conditions")
        }
        else{
        self.selectedPatientCondition =  RPMUserSettings.sharedInstance.patientCondition
        
        for conditions in self.selectedPatientCondition{
        NetworkManager().addpatientcondition(conditions.iD, completion: {

            _ in

        })
        }

        logintoapp()
        }
    }
    
    
    @objc func handleTermTapped(gesture: UITapGestureRecognizer) {

            self.performSegue(withIdentifier: "Policy", sender: nil)
           
    }
    
    func logintoapp()
    {
        self.showSpinner(onView: self.view)
        NetworkManager().login(username: RPMUserSettings.sharedInstance.username, password: RPMUserSettings.sharedInstance.password) { (response) -> () in
            self.removeSpinner(completion: {
                if response != nil {
                    
                    let pId = RPMUserSettings.sharedInstance.patientId ?? ""
                    let termsAndCondtns = UserDefaults.standard.string(forKey: pId) ?? ""
                    if termsAndCondtns.elementsEqual(pId) == false {
                         self.performSegue(withIdentifier: "LogInToDashboard1", sender: nil)
                        //self.performSegue(withIdentifier: "Policy", sender: nil)
                    }
                    else{
                        self.performSegue(withIdentifier: "LogInToDashboard1", sender: nil)
                    }
                    
                }
               
            })
    }
    }

}

//MARK: - UITableviewDelgate
extension NurseMonitoringViewController: UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

//MARK: - UITableviewDataSource

extension NurseMonitoringViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.keyServiceArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "nurseCell", for: indexPath) as! NurseTableViewCell
//        cell.pointsLbl.text = keyServiceArray[indexPath.row]
        cell.titleLbl?.text = keyServiceArray[indexPath.row]
        cell.titleLbl.numberOfLines = 0
//        cell.titleLbl.adjustsFontForContentSizeCategory = true
        cell.titleLbl.adjustsFontSizeToFitWidth = true
        cell.titleLbl.lineBreakMode = .byWordWrapping
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

class NurseMonitorTableViewCell: UITableViewCell
{
    
    @IBOutlet weak var pointsLbl: UILabel!
    
    override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
      }
      
      override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
          
          super.init(style:style, reuseIdentifier:reuseIdentifier)
//          self.initialize()
      }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
           
           // Configure the view for the selected state
    }
}


import UIKit

class SegueFromLeft: UIStoryboardSegue
{
    override func perform()
    {
        let src = self.source
        let dst = self.destination

        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: -src.view.frame.size.width, y: 0)

        UIView.animate(withDuration: 0.25,
            delay: 0.0,
            options: UIView.AnimationOptions.curveEaseInOut,
            animations: {
                dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
            },
            completion: { finished in
                src.present(dst, animated: false, completion: nil)
            }
        )
    }
}
