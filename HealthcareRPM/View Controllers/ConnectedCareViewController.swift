//
//  ConnectedCareViewController.swift
//  HealthcareRPM
//
//  Created by Admin on 2/28/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import UIKit

class ConnectedCareViewController: UIViewController {

        var viewType : Int = 0
        override func viewDidLoad() {
            super.viewDidLoad()
//            tabBarController?.tabBar.isHidden = true
//            self.view.addBackgroundImage()
            
//            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(ConnectedCareVC.handlebackButton))
            
        }
        
        @objc func handlebackButton() {
            self.navigationController?.popViewController(animated: true)
        }
        
        @IBAction func recordManually(_ sender: Any) {
             self.performSegue(withIdentifier: "recordmanually", sender: nil)
        }
        
        @IBAction func graphView(_ sender: Any) {
             self.performSegue(withIdentifier: "graphViewSegue", sender: nil)
           
        }
        
        @IBAction func tableViewClicked(_ sender: Any) {
             self.performSegue(withIdentifier: "tableViewSegue", sender: nil)
            
        }
        
    
    @IBAction func useDevice(_ sender: UIButton) {
        
        if (viewType == 0){
            //BP
            let AndBPstoryboard = UIStoryboard(name: "BluetoothReading", bundle: nil)
            let nextVC = AndBPstoryboard.instantiateViewController(withIdentifier: "AnDBPStoryboard") as! BluetoothReadingViewController
            self.navigationController?.show(nextVC, sender: self)
//            nextVC.delegate = self
        }
            
        else if (viewType == 2){
            //pulseOx
            let NoninStoryboard = UIStoryboard(name: "NoninPulseOxStoryboard" , bundle: nil)
            let nextVC = NoninStoryboard.instantiateViewController(withIdentifier: "NoninInputViewController") as! NoninInputViewController
            self.navigationController?.show(nextVC, sender: self)
//            nextVC.delegate = self
        }
        else if(viewType == 4){
            //weight
            let AnDStoryboard = UIStoryboard(name: "andWeightScaleStoryboard" , bundle: nil)
            let nextVC = AnDStoryboard.instantiateViewController(withIdentifier: "A&DWeightScale") as! AnDWeightScaleViewController
            self.navigationController?.show(nextVC, sender: self)
//            nextVC.delegate = self
        }
    }
    
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "recordmanually" {
                let vc = segue.destination as! MeasureRecordsViewController
                vc.viewType = viewType
        
        
            }
            else if segue.identifier == "graphViewSegue" {
                let vc = segue.destination as! GraphViewController
                vc.viewType = viewType
                
                
            }
            else if segue.identifier == "tableViewSegue" {
                let vc = segue.destination as! TableRecordViewController
                vc.viewType = viewType
                
                
            }
    }
}
