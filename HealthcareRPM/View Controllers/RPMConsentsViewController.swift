//
//  RPMConsentsViewController.swift
//  HealthcareRPM
//
//  Created by EHC_Bosch on 08/09/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit

class RPMConsentsViewController: RPMViewController,UITableViewDelegate, UITableViewDataSource {
    
    
    
    var consentInformation = [RPMConsentModel]()
    var selectedConsentInformation = [RPMConsentModel]()
    @IBOutlet var consentTableView : UITableView?
    @IBOutlet var nextButton : UIButton?
    
    let CONSENT_TYPE_TEXT = "text"
    let CONSENT_TYPE_SWITCH = "switch"
    let CONSENT_TYPE_CHECK = "checkbox"
    let CONSENT_TYPE_SIGNATURE = "signature"
    var isSignature : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        consentTableView?.delegate = self
        consentTableView?.dataSource = self
        //consentTableView?.separatorColor = .clear
        
        loadConsentsData()
        
    }
    
    func loadConsentsData(){
    
        consentInformation.removeAll()
        if let infomation = RPMDataManager.sharedInstance.getConsentsArray(){
            consentInformation = infomation
            consentTableView?.reloadData()
        }
        
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consentInformation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let consent : RPMConsentModel = consentInformation[indexPath.row]
        var selectedConsent : RPMConsentModel?
        if(selectedConsentInformation.count > indexPath.row){
         selectedConsent = selectedConsentInformation[indexPath.row]
        }
        
        
        if(consent.consentType == CONSENT_TYPE_TEXT || consent.consentType == CONSENT_TYPE_SWITCH){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConsentCell")
                as! RPMConsentsTableViewCell
            var switchStatus = false
            if let switchOn = selectedConsent?.isSwichedOn{
                switchStatus = switchOn
            }
            print("the switch status:",switchStatus)
        
            cell.populateConsentCellWith(consentDescription: consent.consentDescription!, switchState: switchStatus)
            //cell.consentSwitch?.addTarget(self, action: Selector(("switchValueDidChange:")), for: .valueChanged)
            cell.consentSwitch?.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
            
            if(consent.consentType == CONSENT_TYPE_TEXT){
                cell.consentSwitch?.isHidden = true
                cell.lblDescription.textContainer.lineBreakMode = .byWordWrapping
                cell.lblDescription.textContainer.maximumNumberOfLines = 6
            }
            else{
             //  let font = UIFont(name: "ArialMT", size:18)!
              //  cell.lblDescription?.font = font
                 cell.consentSwitch?.isHidden = false
            }
            cell.selectionStyle = .none
             return cell
        }
        else if(consent.consentType == CONSENT_TYPE_SIGNATURE){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignatureCell")
                as! RPMSignatureTableViewCell
            cell.sigImage.layer.borderColor = UIColor.gray.cgColor
            cell.sigImage.layer.borderWidth = 1
           
            //cell.populateConsentCellWith(consentDescription: consent.consentDescription!, switchState: consent.isSwichedOn!)
             return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConsentSelectionCell")
                as! RPMConsentSelectionTableViewCell
            //cell.populateConsentCellWith(consentDescription: consent.consentDescription!, switchState: consent.isSwichedOn!)
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let consent : RPMConsentModel = consentInformation[indexPath.row]
        if(consent.consentType == CONSENT_TYPE_SIGNATURE){
            return (self.consentTableView?.frame.height)!/3
        }
        else if (consent.consentType == CONSENT_TYPE_TEXT || consent.consentType == CONSENT_TYPE_SWITCH){
            return 100
        }
        else{
            return 60
        }
        
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        let consent : RPMConsentModel = consentInformation[indexPath.row]
//        if(consent.consentType == CONSENT_TYPE_SIGNATURE){
//            return (self.consentTableView?.frame.height)!/3
//        }
//        else if (consent.consentType == CONSENT_TYPE_TEXT || consent.consentType == CONSENT_TYPE_SWITCH){
//            return 90
//        }
//        else{
//            return 60
//        }
//    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let consent : RPMConsentModel = consentInformation[indexPath.row]
       
        if(consent.consentType == CONSENT_TYPE_SIGNATURE){
            let cell:RPMSignatureTableViewCell = tableView.cellForRow(at: indexPath) as! RPMSignatureTableViewCell
            addSignature(signatureCell:cell)
        }
       
    }
    
    func  addSignature(signatureCell:RPMSignatureTableViewCell){
        //self.sigView.clear()
        isSignature = true
        let window : UIWindow? = UIApplication.shared.keyWindow
        let signaturePopover = Bundle.main.loadNibNamed("HPSSignaturePopup", owner: self, options: nil)?.first as? RPMSignaturePopup
        signaturePopover?.frame = window!.frame
        signaturePopover?.delegate = signatureCell
        self.view.addSubview(signaturePopover!)
        
    }
    
    @objc func switchValueDidChange(_ sender : UISwitch!)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.consentTableView)
        if let indexPath = self.consentTableView?.indexPathForRow(at:buttonPosition){
            if (sender.isOn == true){
                let consent : RPMConsentModel = consentInformation[indexPath.row]
                consent.isSwichedOn = true
               selectedConsentInformation.append(consent)
            }
            else{
                if(selectedConsentInformation.count > indexPath.row){
                    selectedConsentInformation.remove(at: indexPath.row)
                     print("off")
                }
                
               
            }
        }
        
    }
    
    @IBAction func showDashBoard(_sender : UIButton){
        if (selectedConsentInformation.count > 10 && isSignature == true){
          
            NetworkManager().postconsentforpatient(patientId: RPMUserSettings.sharedInstance.patientId!, completion: {
                _ in
                
                  self.performSegue(withIdentifier: "dashboard", sender: nil)
            })
        }
        else if(selectedConsentInformation.count < 10){
            let alert = UIAlertController(title: "Info", message: "Please provide all consent's required to use the application.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil);

        }
        else if(isSignature == false){
            let alert = UIAlertController(title: "Info", message: "Please sign on consent's", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil);
        }
        
    }


}
