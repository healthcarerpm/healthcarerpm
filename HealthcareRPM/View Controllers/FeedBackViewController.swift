//
//  FeedBackViewController.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 06/04/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import SWRevealViewController
import MessageUI
//
//  FeedBackViewController.swift
//  MyFetalLife
//
//  Created by Om Bhagwan on 18/06/18.
//  Copyright © 2018 Om Bhagwan. All rights reserved.
//

import UIKit
import Localize_Swift
class FeedBackViewController: UIViewController , MFMailComposeViewControllerDelegate{


    let ALL_FIELD_MENDATORY =  "Problem: Please fill out all of the required fields."
    
    let FEEDBACK_SAVE_URL = ""
    
    let GET_CATEGORY_FEEDBACK_URL = ""
    
    let SUBMIT = "SUBMIT"

    //MARK:- Feedback

    let CATEGORY = "Category"
    var keyboardHeight : CGFloat?
     

    let coreDataUtils = CoreDataUtils()

    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var sideMenu: UIBarButtonItem!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet weak var bannerView:GADBannerView!
    
    @IBOutlet var bannerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var submitButton: BasicButton!
    
    @IBOutlet var scrollView: UIScrollView!
    
    var feedbackCategories = [Any]()
    var myPickerView : UIPickerView!
    var toolBar = UIToolbar()
    var feedbacKID:Int?
    var parameter = [String:Any]()
    
    var placeHolderText = "Comments.......".localized()
    var activeTextView:UITextView!
    
    var feedbackIndex:Int = 0
    var tempIndex:Int = 0
    var feedbackValue:String?
    
    let feedbackArray = ["App shows inaccurate details","App does not open a page","App needs more improvement"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //   self.view.addBackgroundImage()
        self.categoryTextField.layer.borderWidth = 1
        self.categoryTextField.layer.cornerRadius = 6
        self.categoryTextField.layer.borderColor = UIColor.gray.cgColor
        self.categoryTextField.setLeftPaddingPoints(6.0)
        
        self.feedbackTextView.layer.borderWidth = 1
        self.feedbackTextView.layer.cornerRadius = 6
        self.feedbackTextView.layer.borderColor = UIColor.gray.cgColor
        self.feedbackTextView.doneAccessory = true
        
        if self.revealViewController() != nil {
            sideMenu.target = self.revealViewController()
            sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.setPickerViewForFeedbackCategories()
        
        self.setFeedbackTextToLocalizeLangauge()
        
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 150
    }
    

    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardHeight = keyBoardHeight()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.3//keyboardSize.height/2
            }
        }
    }

        @objc func keyboardWillHide(notification: NSNotification) {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getFeedBackCategories()
         NotificationCenter.default.addObserver(self, selector: #selector(setFeedbackTextToLocalizeLangauge), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    @objc func setFeedbackTextToLocalizeLangauge() {
       self.title = "Feedback".localized()
        self.categoryTextField.attributedPlaceholder = UIUtilFunctions.Instance.setDisablePlaceholderAndColorLightGray(CATEGORY)
        self.submitButton.setTitle(SUBMIT, for: UIControl.State.normal)
         self.feedbackTextView.text = placeHolderText
    }
    //Change Status bar color
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //PickerViewSetUP
    fileprivate func setPickerViewForFeedbackCategories(){
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.clear
        
        // ToolBar
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(FeedBackViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(FeedBackViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
    }
    //MARK:- UIPicker_Gender_Action
    @objc func doneClick() {
        self.feedbackIndex = self.tempIndex
        self.categoryTextField.text = self.feedbackValue
        self.categoryTextField.resignFirstResponder()
    }
    @objc func cancelClick() {
        self.categoryTextField.resignFirstResponder()
    }

    fileprivate func getFeedBackCategories() {

        self.feedbackCategories.append(FeedbackCategories(["1":"General"]) )
    }
    
    
    @IBAction func submitButton_Tapped(_ sender: Any) {
        if feedbackTextView.text == "" || categoryTextField?.text == ""
        {
                     let alert = UIAlertController(title: "Feedback", message: "Please fill both the fields", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok",style:.cancel,handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.categoryTextField.text = ""
                    self.feedbackTextView.text = placeHolderText

        }else{
        sendEmail()
        }
    }
    
    func isAllFieldValid() -> Bool {
        if (self.categoryTextField.text?.isEmpty)! || self.feedbackTextView.text == placeHolderText || self.feedbackTextView.text == "" {
            return false
        }
        else {
            return true
        }
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            let patientDetails = coreDataUtils.getUserDetails()
            mail.setToRecipients(["info@HealthcareRPM.com"])
            mail.setMessageBody("<p>Feedback given by \(patientDetails.firstName!)    \(patientDetails.lastName!). \(patientDetails.firstName!)  \(patientDetails.lastName!) writes \(String(describing: feedbackTextView.text!))</p>", isHTML: true)
            mail.setSubject("Feedback for HealthcareRPM Business app ")
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
        self.categoryTextField.text = ""
        self.feedbackTextView.text = ""
        let alert = UIAlertController(title: "Feedback", message: "Feedback submitted.\nThanks for your suggestions.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok",style:.cancel,handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.categoryTextField.text = ""
        self.feedbackTextView.text = placeHolderText
        
    }

    
}

//MARK:- UIPickerDelegate and UIPickerDataSource Method
extension FeedBackViewController : UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.feedbackArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let data : String = self.feedbackArray[row]
        return data
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
            let data : String = self.feedbackArray[row]
            self.tempIndex = row
           self.feedbackValue = data
          
        
    }
}

extension FeedBackViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if self.feedbackCategories.count ==  0 {
            print("Value not Avail")
            self.categoryTextField.isUserInteractionEnabled = false
        }
        else{
             self.categoryTextField.isUserInteractionEnabled = true
            self.categoryTextField.inputView = myPickerView
            self.categoryTextField.inputAccessoryView = toolBar
            self.myPickerView.selectRow(self.feedbackIndex, inComponent: 0, animated: true)
            self.pickerView(self.myPickerView, didSelectRow: self.feedbackIndex, inComponent: 0)
        }
     }
}

extension FeedBackViewController : UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.activeTextView = textView
        self.activeTextView.textColor = .black
        
        if(self.activeTextView.text == placeHolderText) {
            self.activeTextView.text = ""
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.activeTextView = textView
        if(activeTextView.text == "") {
            let colour = UIColor(red: 205/255.0, green: 207/255.0, blue: 219/255.0, alpha: 1.0)
            self.activeTextView.text = placeHolderText
            self.activeTextView.textColor = colour
        }
    }
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    func setCornerRadiusOfTextField() {
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 6
        self.layer.borderColor = UIColor.gray.cgColor
    }
}















