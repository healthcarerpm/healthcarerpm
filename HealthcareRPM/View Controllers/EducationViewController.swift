//
//  EducationViewController.swift
//  HealthcareRPM
//
//  Created by Himanshu on 8/14/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit
import WebKit
import SWRevealViewController

class EducationViewController: UIViewController,LogoutModalDelegate {
    
     let coreDataUtils = CoreDataUtils()
    
      func handleTouch() {
      let alert = UIAlertController(title: "Logout", message: "Are you sure you wan to logout?", preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "Logout",
                                       style:.default,
                                       
                                       handler: {(alert: UIAlertAction!) in                                          UserDefaults.standard.set("no", forKey: "LoggedIn")
                                        self.deleteAllData()
                                         UserDefaults.standard.synchronize()
                                         
                                         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                                         
                                         let loginVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginViewController
                                         
                                         self.present(loginVC, animated: true, completion: nil)
                                         
         }))
         
         alert.addAction(UIAlertAction(title: "cancel",
                                       
                                       style:.cancel,
                                       
                                       handler: nil))

         
         self.present(alert, animated: true, completion: nil)
     }
   
    @objc func deleteAllData() {
            
            do {
                //invalidate the timer on logout
                MeasurementIdentifier.sharedInteractor.repeatingTimer.suspend()
                try self.coreDataUtils.deleteAllDatabase()
                
            } catch  {
                print("caught exception")
            }
            
        }
    
func moveToTC() {
      let tcview =  self.storyboard?.instantiateViewController(withIdentifier: "TermsConditionsViewController") as! TermsConditionsViewController
      tcview.fromDashboard = true
      self.present(tcview, animated: true, completion: nil)
  }

    let webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: "http://brainmeshintegrumportal.azurewebsites.net/Education") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
        if self.revealViewController() != nil {
             self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
         }

   //      self.navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "more"), style: .plain, target: self, action: #selector(moreBarBtnAction(sender:)))
        // Do any additional setup after loading the view.
        
        //setting only logo in navigation
        let imageView = UIImageView(frame: CGRect(x:UIScreen.main.bounds.size.width - 60, y:12, width:50, height:20))
        imageView.contentMode = .scaleAspectFit
        imageView.image = LogoResource.sharedInteractor.splashLogo
        navigationController?.navigationBar.addSubview(imageView)
        
    }
    
    override func loadView() {
        self.view = webView
    }
      var isPopoverPresented = false
    @objc func moreBarBtnAction(sender: UIBarButtonItem)
       {
           isPopoverPresented = !isPopoverPresented
           if !isPopoverPresented
           {
               if self.children.count > 0
               {
                   let viewControllers:[UIViewController] = self.children
                   for viewContoller in viewControllers{
                       viewContoller.willMove(toParent: nil)
                       viewContoller.view.removeFromSuperview()
                       viewContoller.removeFromParent()
                   }
               }
           }
           else{
               let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "LogoutModalViewController") as! LogoutModalViewController

               popoverContent.view.frame = CGRect(x: self.view.frame.midX, y: 100, width: UIScreen.main.bounds.width/2, height: 100)
               popoverContent.delegate = self
               self.addChild(popoverContent)
               self.view.addSubview(popoverContent.view)
               popoverContent.didMove(toParent: popoverContent)
           }
       }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false

        self.navigationItem.rightBarButtonItem?.isEnabled = false

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
