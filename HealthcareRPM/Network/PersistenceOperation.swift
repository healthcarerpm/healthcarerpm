//
//  PersistenceOperation.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 17/04/19.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation
import CoreData

class PersistenceOperation : Operation{
    
    init(_ aClosure: @escaping (_ context: NSManagedObjectContext)->Void) {
        
        closure = aClosure
        super.init()
    }
    
    override func main() {
        
        guard !isCancelled else {
            return
        }
        
        //        let context = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        //        context.parentContext = NSManagedObjectContext.MR_rootSavingContext()
        
//        let context = NSManagedObjectContext.mr_()
        
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let context = appDelegate!.persistentContainer.viewContext
        
        context.performAndWait {
            
            self.closure(context)
        }
    }
    
    fileprivate let closure: ((NSManagedObjectContext)->Void)
}

