//
//  AsyncronousOperation.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 17/04/19.
//  Copyright © 2019 Isham. All rights reserved.
//

class AsyncronousOperation: Operation {
    
    init(closure aClosure: @escaping (@escaping ()->Void)-> Void) {
        self.closure = aClosure
    }
    
    //  var closure: (@escaping ()->Void) ->Void;
    
    fileprivate func isInProgress() -> Bool {
        
        var result: Bool!
        
        statusIsolationQueue.sync {
            
            [unowned self] in
            result = self.inProgress
        }
        
        return result
    }
    
    fileprivate func setInProgress(_ value: Bool) {
        
        willChangeValue(forKey: "isExecuting")
        
        self.statusIsolationQueue.sync(flags: .barrier, execute: {
            
            [unowned self] in
            self.inProgress = value
        })
        
        didChangeValue(forKey: "isExecuting")
    }
    
    fileprivate func isDone() -> Bool {
        
        var result: Bool!
        
        statusIsolationQueue.sync {
            
            [unowned self] in
            result = self.done
        }
        
        return result
    }
    
    fileprivate func setDone(_ value: Bool) {
        
        willChangeValue(forKey: "isFinished")
        
        self.statusIsolationQueue.sync(flags: .barrier, execute: {
            
            [unowned self] in
            self.done = value
        })
        
        didChangeValue(forKey: "isFinished")
    }
    
    fileprivate let closure: (_ completion:(@escaping ()->Void)) -> Void
    
    fileprivate var inProgress = false
    fileprivate var done = false
    
    fileprivate let statusIsolationQueue = DispatchQueue(label: "AsyncronousOperation Status Isolation queue", attributes: DispatchQueue.Attributes.concurrent);
}

//MARK:- Implementation

extension AsyncronousOperation {
    
    override func start() {
        
        print("operation start: %@", name)
        setInProgress(true)
        
        OperationQueue.main.addOperation {
            
            self.closure {
                
                print("operation finish: %@", self.name)
                self.setInProgress(false)
                self.setDone(true)
            }
        }
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        return self.isInProgress()
    }
    
    override var isFinished: Bool {
        return self.isDone()
    }
}

