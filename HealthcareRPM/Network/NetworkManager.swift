//

//  File.swift

//  HealthcareRPM

//

//  Created by Isham on 17/01/2019.

//  Copyright © 2019 Isham. All rights reserved.

//



import Foundation

import Alamofire

import CoreData

import Repeat

import SwiftyJSON


class NetworkManager {
    
    fileprivate let isolationQueue: OperationQueue = {
        
        let operationQueue = OperationQueue()
        
        operationQueue.maxConcurrentOperationCount = 5
        
        operationQueue.name = "networkOperationQueue"
        
        return operationQueue
        
    }()
    
    static let sharedInstance = NetworkManager()
    
    let BASE_URL = "https://healthcarerpmapi.azurewebsites.net/"
//    let BASE_URL = "https://brainmeshintegrumapi.azurewebsites.net/"
    
    var patientId = ""
    
    let networkReachability = NetworkReachabilityManager()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    func startObservingforNetworkReachability() {
        
        networkReachability?.listener = { status in
            if self.networkReachability?.isReachable ?? false {
                
                switch status {
                    
                case .reachable(.ethernetOrWiFi):
                    print("The network is reachable over the WiFi connection")
                    self.checkForOfflineSyncNeeded()
                    
                case .reachable(.wwan):
                    print("The network is reachable over the WWAN connection")
                    self.checkForOfflineSyncNeeded()
                    
                    
                case .notReachable:
                    print("The network is not reachable")
                    
                case .unknown :
                    print("It is unknown whether the network is reachable")
                    
                }
            }
            
            
        }
        self.networkReachability?.startListening()
    }
    
    
    func getpatientmessages(patientId:String, completion: @escaping (Any?) -> Void)
    {
        guard let url = URL(string: BASE_URL+"/api/patient/GetPatientChats") else {
            completion(nil)
            return
        }
        
        let requestData = ["PatientId" : patientId
        ]
        
        Alamofire.request(url,
                   method: .get,
                   parameters: requestData)
            .validate()
            .responseJSON { response in
                switch response.result{
                    
                case .success(let successResponse):
                    let value = successResponse as? NSArray
                    if value!.count > 0{
                        MeasurementIdentifier.sharedInteractor.messages = []
                        
                        for msg in value!{
                            let message = msg as! [String:Any]
                            let messag = Message.init(fromId: message["FromId"] as? String, toID: message["ToId"] as? String, value: message["Message"] as? String, creationDate: message["CreateDate"] as? String, frompatient: ((message["bFromPatient"] != nil)), messageId: message["Id"] as? String, fromuserName: message["FromUserName"] as? String)
                            MeasurementIdentifier.sharedInteractor.messages?.append(messag)
                        }
                        
                        completion(value)
                    }
                    else
                    {
                        completion(nil)
                    }
                    
                case .failure(let error):
                    completion(error)
                    
                    print("error", error)
                    
                }
                
        }
    }
    
    
    func checkForOfflineSyncNeeded(){
        
        CoreDataUtils().getAllOfflineSavedData()
        if (appDelegate.offlineSyncNeeded == true){
            
            //  fetch core data records with value isUploadedToServer == False. and call API to send data
            
            
            
        }else{
            return
        }
        
        
    }
    
    
    // With Alamofire
    
    func login(username: String, password: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/Token") else {
            
            completion(nil)
            
            return
            
        }
        
        let requestData = ["grant_type" : "password",
                           
                           "username" : username,
                           
                           "password" : password]
        
        
        
        Alamofire.request(url,
                   
                   method: .post,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON { response in
                
                switch response.result{
                case .success(let result):
                    let value = result as? [String: Any];
                    if let accessToken = value? ["access_token"]{
                        let accessTokenExpiry = value? [".expires"]!
                        UserDefaults.standard.set(accessToken, forKey: Constants.ACCESS_TOKEN_DEFAULT)
                        UserDefaults.standard.set(accessTokenExpiry, forKey: Constants.ACCESS_TOKEN_TOEKN_EXPIRY)
                        UserDefaults.standard.set(true, forKey: Constants.USER_DEF_LOGIN_STATUS)
                        UserDefaults.standard.synchronize()
                        self.getPatientDetails(username: username, password: password, completion: completion)
                    }
                    else{
                        
                        completion(nil)
                        
                        
                    }
                    
                    
                case .failure(let error):
                    completion("Error")
                    
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    func loginForToken(username: String, password: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/Token") else {
            
            completion(nil)
            
            return
            
        }
        
        let requestData = ["grant_type" : "password",
                           
                           "username" : username,
                           
                           "password" : password]
        
        
        
        Alamofire.request(url,
                   
                   method: .post,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON { response in
                
                switch response.result{
                case .success(let result):
                    let value = result as? [String: Any];
                    let accessToken = value? ["access_token"]!
                    let accessTokenExpiry = value? [".expires"]!
                    UserDefaults.standard.set(accessToken, forKey: Constants.ACCESS_TOKEN_DEFAULT)
                    UserDefaults.standard.set(accessTokenExpiry, forKey: Constants.ACCESS_TOKEN_TOEKN_EXPIRY)
                    UserDefaults.standard.set(true, forKey: Constants.USER_DEF_LOGIN_STATUS)
                    UserDefaults.standard.synchronize()
                    completion(result)
                    
                case .failure(let error):
                    completion(error)
                    
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    func addpatientcondition(_ condition:String,completion: @escaping (Any?) -> Void)
        
    {
        
        guard let url = URL(string: BASE_URL+"/api/patient/addPatientCondition") else {
            
            
            
            completion(nil)
            
            
            
            return
            
            
            
        }
        
        
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        
        let headers: HTTPHeaders = [
            
            "api_key": token,
            
            "Accept": "application/json"
            
        ]
        
        
        
        let requestData = ["Id": UUID().uuidString,
                           
                           "ConditionId": condition,
                           
                           "IsActive": true,
                           
                           "Sequence": 0,
                           
                           "PatientId":  RPMUserSettings.sharedInstance.patientIdNumber ,
                           
                           "CreateDate": getCurrentDateTimeString(),
                           
                           "CreatedBy": "00000000-0000-0000-0000-000000000000",
                           
                           "LastAction": getCurrentDateTimeString(),
                           
                           "LastActionBy": "00000000-0000-0000-0000-000000000000"] as [String:Any]
        
        
        
        Alamofire.request(url,
                   
                   method: .post,
                   
                   parameters: requestData, headers: headers)
            
            .validate()
            
            .responseJSON { response in
                
                switch response.result{
                    
                case .success(let result):
                    
                    print("success \(result)")
                    
                    
                    
                case .failure(let error):
                    
                    print("error \(error)")
                    
                }
                
        }
        
        
        
        
        
    }
    
    func getPatientConditions(requestData: [String:Any],completion: @escaping (Any?) -> Void){
        guard let url = URL(string: BASE_URL+"/api/conditions/list") else {
            
            completion(nil)
            
            return
            
        }
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: requestData, headers: headers)
            
            .validate()
            
            .responseJSON { response in
                
                switch response.result{
                case .success(let result):
                    let value = result as? [String: Any];
                    completion(value)
                    //self.getPatientDetails(username: username, password: password, completion: completion)
                    
                case .failure(let error):
                    completion(error)
                    
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    func register(requestData: [String:Any],completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/Patient/Create") else {
            
            completion(nil)
            
            return
            
        }
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   
                   method: .post,
                   
                   parameters: requestData, headers: headers)
            
            .validate()
            
            .responseJSON { response in
                
                switch response.result{
                case .success(let result):
                    let value = result as? [String: Any];
                    completion(value)
                    //self.getPatientDetails(username: username, password: password, completion: completion)
                    
                case .failure(let error):
                    completion(error)
                    
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    /**
        Get assessment data of a range on dates
     */
    func getAssessmentRangeData(patientId:String,fromDate:Date,toDate:Date, start:Int,count:Int, completion: @escaping (JSONTraverser?) -> Void) {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        let fromDateString = dateFormatter.string(from: fromDate)
        let fromDateString = dateFormatter.string(from: fromDate)//"2019-01-09T16:47:23.872Z"
        let toDateString = dateFormatter.string(from: toDate)
        
        guard let url = URL(string: BASE_URL+"api/assessment/GetAssessmentResponseRange") else {
            
            completion(nil)
            
            return
            
        }
        
        let requestData = ["patientId" : patientId,
                           "fromDate" : fromDateString,
                           "toDate" : toDateString]
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    do {
                        completion(JSONTraverser(data: response.data))
                        
                    } catch {
                        
                        print(error)
                        completion(nil)
                    }
                    
                    
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
        }
                
    }
    
    
    
    func getPatientDetails(username: String, password: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/Account/GetUser") else {
            completion(nil)
            return
        }
        
        let requestData = ["userName" : username,
                           "password" : password]
        
        Alamofire.request(url,
                   method: .get,
                   parameters: requestData)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    let value = result as? [String: Any];
                    CoreDataUtils().saveUserdetails(value: value!)
                    UserDefaults.standard.set(true, forKey: Constants.USER_DEF_LOGIN_STATUS)
                    let patientId = value? ["Id"]!
                    self.patientId = patientId as! String
                    RPMUserSettings.sharedInstance.patientId = self.patientId
                    MeasurementIdentifier.sharedInteractor.patientId = self.patientId
                    UserDefaults.standard.set(patientId, forKey: Constants.USER_DEF_PATIENT_ID)
                    self.UploadDeviceToken(patientId as! String)
                    let customerId = value!["CustomerId"] as! String
                    UserDefaults.standard.set(customerId, forKey: Constants.USER_DEF_CUSTOMER_ID)
                    if let loggedstatus = UserDefaults.standard.value(forKey: "LoggedIn") as? String
                    {
                        if loggedstatus == "no"
                        {
                            UserDefaults.standard.set("yes", forKey: "LoggedIn")
                            UserDefaults.standard.synchronize()
                            self.getmeasurementsByPatientId(userId: self.patientId, completion: {_ in
                            })
                        }
                    }
                    else
                    {
                        UserDefaults.standard.set("yes", forKey: "LoggedIn")
                        UserDefaults.standard.synchronize()
                        self.getmeasurementsByPatientId(userId: self.patientId, completion: {_ in
                        })
                        
                    }
                    self.getCustomerTypeandDetails(userId: customerId, completion: completion)
                    
                case .failure(let error):
                    completion(error)
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    func getCurrentDateTimeasString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        
        return str
    }
    
    func postpatientMessage(_ userId:String,message:String)
    {
        guard let url = URL(string: BASE_URL + "/api/patient/PostPatientMessage") else {
            return
        }
        
        let requestData = [
            "Id":UUID().uuidString,
            "Message" : message,
            "FromId" : MeasurementIdentifier.sharedInteractor.patientId!,
            "ToId" : "00000000-0000-0000-0000-000000000000", //new requirement UTC time
            "bFromPatient":true,
            "CreateDate": getCurrentDateTimeasString(),
            "fromUserName": CoreDataUtils().getUserDetails().firstName!
            ] as [String : Any]
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    print("success \(result)")
                    
                case .failure(let error):
                    //                    completion(error)
                    
                    print("error \(error)")
                }
        }
    }
    
    
    func UploadDeviceToken(_ userId:String)
        
    {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard let url = URL(string: BASE_URL + String(format: "api/registerdevice/%@?sDeviceId=%@", userId,appDelegate.returndevicetoken())) else{
            return
        }
        
        Alamofire.request(url,
                   
                   method: .post,
                   
                   parameters: nil, headers: nil)
            
            .validate()
            
            .responseJSON { response in
                
                switch response.result{
                case .success(let result):
                    print("success \(result)")
                    
                case .failure(let error):
                    //                    completion(nil)
                    
                    print("error \(error)")
                }
        }
    }
    
    
    
    func getCustomerTypeandDetails(userId: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format:"api/customer/%@/details",userId)) else {
            
            completion(nil)
            
            return
            
        }
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: nil)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                        
                        guard let jsonArray = json as? [String: Any] else {
                            
                            return
                            
                        }
                        
                        print(jsonArray)
                        
                        CoreDataUtils().saveCustomerType(dictionary: jsonArray)
                        
                        self.getMeasurementTypes(userId: self.patientId , completion: completion)
                        
                        
                        
                    } catch {
                        
                        print(error)
                        
                    }
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
                
                
                
        }
        
    }
    
    
    
    
    
    func getMeasurementTypes(userId: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/measurement/types") else {
            
            completion(nil)
            
            return
            
        }
        
        let requestData = ["Id" : userId]
        
        
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    let decoder = JSONDecoder()
                    
                    do {
                        let array = try decoder.decode([Measurement].self, from: response.data!)
                        debugPrint(array)
                        
                        CoreDataUtils().saveMeasurementType(array: array)
                        
                    } catch {
                        
                        print(error)
                        
                    }
                    completion(result)
                case .failure(let error):
                    completion(nil)
                    
                    print("error \(error)")
                }
                
                
                
                
                
        }
        
    }
    
    
    
    func uploadData(measurementData: MeasurementData, completion: @escaping (String?) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format: "api/measurement/%@", measurementData.patientId!)) else {
            completion(nil)
            return
        }
        
        let requestData = [
            "Id":measurementData.patientId,
            "MeasurementTypeId" : measurementData.measurementTypeId,
            "Value" : measurementData.value,
            "MeasurementValueTypeId" : measurementData.measurementValueType,
            "MeasurementDateTime" : measurementData.measurementDateTime!, //new requirement UTC time
            "IsManual":measurementData.isManual!.description,
            "LinkedMeasurementId":measurementData.linkedId ?? UUID().uuidString,
            "Details": measurementData.details!
        ]
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    completion(nil)
                    
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
        }
    }
    
    
    
    
    func changepassword(params: [String:Any], completion: @escaping (String) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format: "api/Account/ChangePasswordWithLogin")) else {
            completion("Error")
            return
        }
        
        let requestData = params
        
        //        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        
        let headers: HTTPHeaders = [
            //            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    
                    let value = result as? [String: Any];
                    
                    if value?["Succeeded"] as! Int == 1{
                        completion("Success")
                    }else{
                        completion("Failure")
                    }
                    
                case .failure(let error):
                    completion("Failure")
                    print("error \(error)")
                }
                
        }
    }
    
    
    func changepasswordwithoutlogin(params: [String:Any], completion: @escaping (String) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format: "api/Account/ChangePassword")) else {
            completion("Error")
            return
        }
        
        let requestData = params
        
        let headers: HTTPHeaders = [
            
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    completion("Success")
                    
                    print("success \(result)")
                    
                case .failure(let error):
                    completion("Failure")
                    print("error \(error)")
                }
                
        }
    }
    
    func forgotpassword(params: [String:Any], completion: @escaping (String?) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format: "api/Account/ForgotPassword")) else {
            completion(nil)
            return
        }
        
        let requestData = params
        
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    completion(nil)
                    
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
        }
    }
    
    func uploadDataPulse(measurementData: MeasurementData, completion: @escaping (String?) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format: "api/measurement/%@", measurementData.patientId!)) else {
            completion(nil)
            return
        }
        
        let requestData = [
            "Id":measurementData.patientId,
            "MeasurementTypeId" : measurementData.measurementTypeId,
            "Value" : measurementData.value,
            "MeasurementValueTypeId" :"1",// measurementData.measurementValueType,
            "MeasurementDateTime" : measurementData.measurementDateTime,
            "IsManual":measurementData.isManual!.description,
            "LinkedMeasurementId":measurementData.linkedId ?? UUID().uuidString
        ]
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    completion(nil)
                    
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
        }
    }
    
    
    func uploadDataSPO2(measurementData: MeasurementData, completion: @escaping (String?) -> Void) {
        
        guard let url = URL(string: BASE_URL + String(format: "api/measurement/%@", measurementData.patientId!)) else {
            completion(nil)
            return
        }
        let requestData = [
            "Id":measurementData.patientId,
            "MeasurementTypeId" : measurementData.measurementTypeId,
            "Value" : measurementData.value,
            "MeasurementValueTypeId" : "1",// measurementData.measurementValueType,
            "MeasurementDateTime" : measurementData.measurementDateTime,
            "IsManual":measurementData.isManual!.description,
            "LinkedMeasurementId":measurementData.linkedId ?? UUID().uuidString
        ]
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData, headers: headers)
            .validate()
            .responseJSON { response in
                switch response.result{
                case .success(let result):
                    completion(nil)
                    
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
        }
    }
    
    func getPatientSessions(patientId: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/assessmentcalendar/getpatientupdatedassessments") else {
            
            completion(nil)
            
            return
            
        }
        
        let requestData = ["patientId" : patientId,"startDate":getAddedDateTimeStringStartdate(daysToAdd: -1, monthsToAdd: 0, yearsToAdd: 0),"endDate":getAddedDateTimeString(daysToAdd: 3), "removeCompleted": String(true)] as [String:Any]
        
        
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    do {
                        
                        JSONTraverserHelper.doJSONTraversalSessionRequest(data:response.data!)
                        
                    } catch {
                        
                        print(error)
                        
                    }
                    
                    completion(result)
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    
    func getAssessmentNodes(assessmentId: String, sessionId: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/AssessmentDesigner/getassessmentdata") else {
            
            completion(nil)
            
            return
            
        }
        
        let requestData = ["AssessmentId" : assessmentId]
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    do {
                        print("assessment array", response)
                        JSONTraverserHelper.doJSONTraversalAssessmentNodeRequest(data:response.data!,sessionId: sessionId)
                        
                    } catch {
                        
                        print(error)
                        
                    }
                    
                    completion(result)
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
                
                
        }
        
    }
    
    
    /**
     
     Sends Assessment Response of the patient to the server.
     - Parameters:
     - patientId : Patient whose session is send
     - sessionId : The session being send
     - assessmentId: The assessment of the session being send.
     - Throws: .
     - Returns: Emtyp completion.
     */
    
    func postAssessmentResponse(patientId:String, completion: @escaping (Any?) -> Void) {
        let isolatedClosure = {
            (operationCompletion: ()->Void) in
            assert(Thread.isMainThread, "Must be the main thread")
            
//            guard let url = URL(string: self.BASE_URL+"api/assessment/\(patientId)/postassessmentresponse") else {
//                completion(nil)
//                return
//            }
            
            guard let url = URL(string: self.BASE_URL+"api/assessment/postassessmentresponses") else {
                completion(nil)
                return
            }

            print("about to get CompletedAssessments")
            
            CoreDataUtils().getCompletedAssessments(){
                (completedAssessments) in
                for completedAssessement in completedAssessments{
                    let dateTimeFormatter = DateFormatter()
                    dateTimeFormatter.dateFormat="MM/dd/yyyy HH:mm:ss"
                    //            dateTimeFormatter.timeZone = TimeZone(identifier: "UTC")
                    let answeredDateTimeString:String  = dateTimeFormatter.string(from: completedAssessement.answeredDateTime ?? Date())
                    var questionsJSON = [Question]()
                    
                    var idx = 0
                    
                    let nodes = CoreDataUtils().getTakenCompletedChildNodesOfAssessmentParent(assessmentId: completedAssessement.assessmentId , sessionId: completedAssessement.sessionId , parentId: completedAssessement.assessmentId )
                    
                    for takenNode in nodes{
                        let takenNodeJSON = (self.formAssessmentRequestJSON(takenNode:takenNode))
                        questionsJSON.append(takenNodeJSON)
                        idx += 1
                    }
                    
                    let requestDataJSON = AssessmentResponse(PatientId:patientId,AssessmentId:completedAssessement.assessmentId,Name:completedAssessement.name! ,AnsweredDateTime: answeredDateTimeString, Questions:questionsJSON, Id: completedAssessement.sessionId)
                    
                    print("requestDataJSON = \(requestDataJSON)")
                    
                    
                    
                    let jsonData = try! JSONEncoder().encode(requestDataJSON)
                    let jsonString = String(data: jsonData, encoding: .utf8)!
                    print("jsonString:\(jsonString)")
                    
                    let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
                    let headers: HTTPHeaders = [
                        "Accept": "application/json",
                        "Content-Type":"application/json",
                    ]
                    let finalJSONString = jsonString.replacingOccurrences(of:"\\", with: "")
                    print(finalJSONString)
                    do {
                        var requestDictionary = try requestDataJSON.asDictionary()
                        requestDictionary["patientid"] = RPMUserSettings.sharedInstance.patientId!
                        Alamofire.request(url,
                                   method: HTTPMethod.post ,
                                   parameters: requestDictionary,
                                   encoding: JSONEncoding.default,
                                   headers: headers
                                   )
                            .responseJSON { response in
                                switch response.result{
                                case .success(let result):
                                    CoreDataUtils().markSessionAsSubmitted(sessionId: completedAssessement.sessionId, assessmentId: completedAssessement.assessmentId)
                                    print("success \(result)")
                                    
                                case .failure(let error):
                                    completion(nil)
                                    print("error \(error)")
                                }
                                
                        }
                        
                    }
                    catch {
                        print(error)
                    }
                    
                    
                }
                
            }
        }
        
        let operation = AsyncronousOperation(closure: isolatedClosure as (@escaping()->Void)-> Void)
        operation.name = "Posting Completed Assessment Responses to Server"
        isolationQueue.addOperation(operation)
        
    }
    
    
    /**
     Co.
     - Parameters:
     - object :
     - Throws: .
     - Returns: JSON in String format.
     */
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        
        return String(data: data, encoding: String.Encoding.utf8)
        
    }
    
    
    
    
    /**
     Recursive function that forms the questions JSON strcuture to be send to server
     - Throws: .
     - Returns: Question.
     */
    
    func formAssessmentRequestJSON(takenNode:Node)->Question{
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="MM/dd/yyyy HH:mm:ss"
        
        let takenMeasurementDateTime:String  = dateTimeFormatter.string(from: takenNode.measurementDateTime ?? Date())
        
        let takenId = takenNode.id
        let takenText = takenNode.text ?? ""
        let takenAnswerId = takenNode.answerId ?? ""
        let takenValue = takenNode.value ?? ""
        let takenNodeType = takenNode.nodeType.rawValue
        var childrenJSON = [Question]()
        //bypass option nodes in case of radio and checkbox
        //        if(takenNodeType == Node.NodeType.radio.rawValue || takenNodeType == Node.NodeType.check.rawValue )
        
        let childrenNode = CoreDataUtils().getTakenCompletedChildNodesOfAssessmentParent(assessmentId: takenNode.assessmentId ?? "", sessionId: takenNode.sessionId ?? "", parentId: takenNode.id ?? "")
        
        var idx = 0
        for childNode in childrenNode{
            var childNodeJSON = self.formAssessmentRequestJSON(takenNode:childNode)
            childrenJSON.append(childNodeJSON)
            idx += 1
        }
        let nodeJSON = Question(Id:takenId,Text:takenText,AnswerId:takenAnswerId,Value:takenValue,type:takenNodeType,MeasurmentDateTime:takenMeasurementDateTime, Children:childrenJSON, MeasurementTypeId:takenNode.measurementTypeId ?? "", MeasurementValueTypeId: takenNode.measurementValueTypeId ?? "", MeasurementValue: takenNode.measurementValue ?? "")
        return nodeJSON
    }
    
    /**
     
     Returns the current date time as a string.
     
     - Throws: .
     
     - Returns: Current date time as a string.
     
     */
    
    func getCurrentDateTimeString() -> String{
        
        let currentDateTime = Date()
        
        let dateTimeFormatter = DateFormatter()
        
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'"
        
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        
        return str + "00:01:01.767Z"
        
    }
    
    func getpatientconsent(patienId:String, completion: @escaping (String) -> Void)
    {
        
        guard let url = URL(string: BASE_URL + String(format:"/api/patient/getconsents?sPatientId=%@",patienId)) else {
            
            completion("failure")
            
            return
            
        }
        
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: nil)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                    
                case .success(let result):
                    do {
                        
                        if let res = result as? NSArray{
                            if res.count > 0{
                                completion("consents given")
                            }else{
                                completion("pending")
                            }
                        }else{
                            completion("pending")
                        }
                        
                        print("success \(result)")
                        
                    } catch {
                        
                        completion("failure")
                        print(error)
                        
                    }
                    
                    
                    
                    
                case .failure(let error):
                    completion("failure")
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    func postconsentforpatient(patientId: String, completion: @escaping (Any?) -> Void) {
        
        guard let url = URL(string: BASE_URL+"api/patient/postconsents") else {
            completion(nil)
            return
        }
        
        let requestData = ["patientId" : patientId,"Id":UUID().uuidString, "ConsentDate":  "2020-02-21T21:31:26.317Z","ConsentType": "Patient", "ConsentVersion": "1.0"] as [String:Any]
        
        
        Alamofire.request(url,
                   method: .post,
                   parameters: requestData)
            .validate()
            .responseJSON{ response in
                
                switch response.result{
                    
                case .success(let result):
                    completion(result)
                    print("success \(result)")
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
                
                
        }
        
    }
    
    
    
    
    func getAddedDateTimeString(daysToAdd:Int = 0 ,monthsToAdd:Int = 0 ,yearsToAdd:Int = 0 ) -> String{
        
        let currentDateTime = Date()
        
        var dateComponent = DateComponents()
        
        dateComponent.month = monthsToAdd
        
        dateComponent.day = daysToAdd
        
        dateComponent.year = yearsToAdd
        
        let futureDateTime = Calendar.current.date(byAdding: dateComponent, to: currentDateTime)
        
        let dateTimeFormatter = DateFormatter()
        
        dateTimeFormatter.dateFormat="yyyy-MM-dd"
        
        let str:String  = dateTimeFormatter.string(from: futureDateTime!)
        
        return str //+ "23:59:09.767Z"
        
    }
    
    
    
    func getAddedDateTimeStringStartdate(daysToAdd:Int = 0 ,monthsToAdd:Int = 0 ,yearsToAdd:Int = 0 ) -> String{
        
        let currentDateTime = Date()
        
        var dateComponent = DateComponents()
        
        dateComponent.month = monthsToAdd
        
        dateComponent.day = daysToAdd
        
        dateComponent.year = yearsToAdd
        
        let futureDateTime = Calendar.current.date(byAdding: dateComponent, to: currentDateTime)
        
        let dateTimeFormatter = DateFormatter()
        
        dateTimeFormatter.dateFormat="yyyy-MM-dd"
        
        //             dateTimeFormatter.dateFormat="yyyy-dd-MM'T'"
        
        let str:String  = dateTimeFormatter.string(from: futureDateTime!)
        
        return str //+ "00:01:01.767Z"
        
    }
    
    func getpatientLastMessagesreadtime(userId:String , completion: @escaping (Any?) -> Void)
    {
        guard let url = URL(string: BASE_URL + String(format:"api/patient/GetPatientLastMessagesReadTime",userId)) else {
            completion(nil)
            return
        }
        
        let requestData = ["patientId" : userId]
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: requestData)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    do {
                        
                        let value = result as? [String: Any];
                        if value?["Result"] != nil{
                            let lastreadtime = value?["Result"]! as? String
                            MeasurementIdentifier.sharedInteractor.datetimelastRead = lastreadtime
                            
                        }
                        
                    } catch {
                        
                        print(error)
                        
                    }
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
        }
    }
    
    func postmessagesLastReadtime(userId:String, completion: @escaping (Any?) -> Void)
    {
        guard let url = URL(string: BASE_URL + "api/patient/PatientLastMessagesReadTime") else {
            completion(nil)
            return
        }
        
        let parameters = ["patientId" : userId,
                          "AddtionalInformation" : "read by user"] as! [String:Any]
        
        let token = UserDefaults.standard.string(forKey: Constants.ACCESS_TOKEN_DEFAULT)!
        
        let headers: HTTPHeaders = [
            "api_key": token,
            "Accept": "application/json"
        ]
        
        Alamofire.request(url,
                   method: .post,
                   parameters: parameters,
                   encoding:JSONEncoding.default,
                   headers: headers)
            .responseJSON
            { response in
                
                switch response.result{
                case .success(let result):
                    do {
                        
                        print("Posted")
                        
                    } catch {
                        
                        print(error)
                        
                    }
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
        }
        
    }
    
    
    func getmeasurementsByPatientId(userId: String , completion: @escaping (Any?) -> Void)
    {
        guard let url = URL(string: BASE_URL + String(format:"api/%@/measurementsbypatient",userId)) else {
            
            completion(nil)
            
            return
            
        }
        
        Alamofire.request(url,
                   
                   method: .get,
                   
                   parameters: nil)
            
            .validate()
            
            .responseJSON{ response in
                
                switch response.result{
                case .success(let result):
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                        
                        guard let jsonArray = json as? [String: Any] else {
                            
                            return
                            
                        }
                        
                        print(jsonArray)
                        
                        if jsonArray != nil{
                            let measurements = jsonArray["PageItems"] as! NSArray
                            if measurements.count > 0
                            {
                                for measurment in measurements{
                                    print(measurment)
                                    CoreDataUtils().savemeasurements(dictionary: measurment as! [String : Any])
                                }
                            }
                        }
                        
                        CoreDataUtils().appendMeasurements()
                        CoreDataUtils().appendMeasurementsPulse()
                        
                        
                    } catch {
                        
                        print(error)
                        
                    }
                    
                case .failure(let error):
                    completion(nil)
                    print("error \(error)")
                }
        }
        
    }
    

}


extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}

extension Data {
    init?<S: StringProtocol>(hexString: S) {
        guard hexString.count % 2 == 0 else { return nil }  // Must be even number of letters
        
        var bytes: [UInt8] = []
        
        var index = hexString.startIndex
        while index != hexString.endIndex {
            let secondIndex = hexString.index(after: index)
            let hexValue = hexString[index...secondIndex]
            guard let byte = UInt8(hexValue, radix: 16) else { return nil } // Unexpected character
            bytes.append(byte)
            index = hexString.index(after: secondIndex)
        }
        self.init(bytes)
    }
}





