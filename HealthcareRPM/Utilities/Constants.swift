//
//  Constants.swift
//  HealthcareRPM
//
//  Created by Isham on 19/01/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import Foundation

struct Constants {
    static let ACCESS_TOKEN_DEFAULT = "accessToken"
    static let ACCESS_TOKEN_TOEKN_EXPIRY = "accessTokenExpiry"
    static let USER_DEF_LOGIN_STATUS = "loginStatus"
    static let USER_DEF_PATIENT_ID = "patientID"
    static let USER_DEF_CUSTOMER_ID = "customerID"
    static let USER_DEF_SPLASH_LOGO_PATH = "splashLogoPath"
    static let BAR_COLOR = UIColor.init(red: 65/225, green: 105/225, blue: 225/225, alpha: 1.0)
    static let COMMON_BLUE = UIColor.init(red: 26/225, green: 107/225, blue: 235/225, alpha: 1.0)
}
