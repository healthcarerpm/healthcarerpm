//
//  LogoResource.swift
//  HealthcareRPM
//
//  Created by Thejus Jose on 10/05/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation

class LogoResource:NSObject{
    
    var splashLogo:UIImage?
    
    static let sharedInteractor: LogoResource = {
        let sharedLogo = LogoResource()
        return sharedLogo
    }()
    
    fileprivate override init() {
        super.init()
    }
    
    
}
