//
//  ViewController.swift
//  Sample
//
//  Created by Isham on 27/12/2018.
//  Copyright © 2018 Isham. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let loginStatus = UserDefaults.standard.bool(forKey: Constants.USER_DEF_LOGIN_STATUS)
        
        if loginStatus {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "dashboardNav") as! UINavigationController
            UIApplication.shared.keyWindow?.rootViewController = viewController;
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onLoginButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "login", sender: nil)
    }
    
}

