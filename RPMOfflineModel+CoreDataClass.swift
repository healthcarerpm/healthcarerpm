//
//  RPMOfflineModel+CoreDataClass.swift
//  
//
//  Created by EHC_Bosch on 20/09/19.
//
//

import Foundation
import CoreData

@objc(RPMOfflineModel)
public class RPMOfflineModel: NSManagedObject {

    @NSManaged public var offlineMeasurementData: String?
    @NSManaged public var tableName: String?
}
